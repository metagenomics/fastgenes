import axios from 'axios';
import EggNogCard from '@/components/eggnogcard/eggnogcard.vue';
import KeggCard from '@/components/keggcard/keggcard.vue';
import TaxonomyCard from '@/components/taxonomycard/taxonomycard.vue';
import SimpleExpand from '@/components/listing/simpleexpand/simpleexpand.vue';
import SimpleListing from '@/components/listing/simplelisting/simplelisting.vue';

export default {
  components: { KeggCard, TaxonomyCard, EggNogCard, SimpleExpand, SimpleListing },
  name: 'gene-detail',
  data() {
    return {
      geneId: '',
      geneDetail: [],
      geneExpandDetails: [],
      sequence: '',
      keggIds: [],
      taxonomyId: '',
      eggnogIds: [],
      notFound: false,
    };
  },
  computed: {
    sourceUrls() {
      return {
        'igc': 'https://db.cngb.org/microbiome/genecatalog/genecatalog_human/',
        'virgo': 'https://virgo.igs.umaryland.edu/',
        'undef': '',
      }
    },
    labelUrls() {
      return {
        'igc': 'Visit IGC website',
        'virgo': 'Visit Virgo website',
        'undef': '',
      }
    },
  },
  mounted() {
    this.geneId = this.$route.params.id;
    this.getGeneDetail();
  },
  methods: {
    getGeneDetail() {
      axios.get(`/api/genes/${this.geneId}`, {
        headers: {
          Accept: 'application/json',
        },
      })
        .then((response) => {
          this.geneDetail = [
            {
              title: 'ID',
              content: response.data.gene_id,
            },
            {
              title: 'Length (bp)',
              content: response.data.length,
            },
          ];

          if (response.data.sequence) {
            this.sequence = '>' + response.data.gene_id + '\n' + response.data.sequence;
          }
          // if (response.data.functions.length > 0) {
          //   response.data.functions.forEach(funcAnnot => {
          //     if (funcAnnot.source == 'kegg') {
          //       this.keggIds.push(funcAnnot.function_id);
          //     }
          //     else if (funcAnnot.source == 'eggnog') {
          //       this.eggnogIds.push(funcAnnot.function_id);
          //     }
          //   })

          // }
          else {
            this.keggId = '';
          }
          if (response.data.taxonomy) {
            this.taxonomyId = response.data.taxonomy.tax_id;
          } else {
            this.taxonomyId = '';
          }
          this.buildGeneExpandDetails(response);
        })
        .catch((error) => {
          console.log(error);
          this.notFound = true;
        });
    },
    buildGeneExpandDetails(response) {
      this.geneExpandDetails = [
        this.buildCatalogs(response),
      ];
    },
    buildCatalogs(response) {
      var catalogs = {
        title: 'Catalogs',
        icon: 'fas fa-book',
        content: [],
      };
      if(response.data.catalogs) {
        Object.entries(response.data.catalogs).forEach(([key, value]) => {
          catalogs.content.push(
            {
              id: value.name,
            },
          );
        });
      }
      return catalogs;
    },
    copyFasta(){
      var copyText = document.getElementById("fastaseq");
      copyText.select();
      document.execCommand("copy");
    }
  },
};