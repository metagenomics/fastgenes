import axios from 'axios';

export default {
  name: 'genes',
  data() {
    return {
      genes: [],
      count: 0,
      page: 1,
      // pagination
      pagination: {
        rowsPerPage: 20,
      },
      pageNumber: 1,
      maxNumberPages: 50000,
      tooManyPages: false,
      // Filters
      showFilters: [false],
      // - Gene info
      geneSource: 'all',
      searchGeneName: null,
      filterGeneLength: false,
      geneLengthFilterRange: [0, 2000],
      // - Taxonomy
      taxLevel: null,
      taxItems: [],
      taxChoice: null,
      // - Function
      filterWithFunction: null,
      functionID: null,
      // Information about request
      loadTable: true,
      requestDone: false,
      // Download FASTA loading
      downloadFastaReady: true,
      downloadCSVReady: true,
      fab: false,
      // Error handlers
      listGenesError: false,
      fastaError: false,
      csvError: false,
    };
  },
  computed: {
    headers() {
      return [
        {
          text: 'Gene ID', align: 'left', sortable: false, value: 'name',
        },
        // {
        //   text: 'Gene name', align: 'left', sortable: false, value: 'name',
        // },
        { text: 'Length', value: 'length', sortable: false },
        { text: 'Taxonomy', value: 'taxonomy', sortable: false },
        { text: 'KEGG', value: 'kegg', sortable: false },
        { text: 'EggNOG', value: 'eggnog', sortable: false },
        { text: 'Catalog', value: 'catalog', sortable: false },
      ];
    },
    taxLevels() {
      const texts = [
        'Kingdom', 'Superkingdom', 'Phylum', 'Class', 'Order', 'Family', 'Genus', 'Species'
      ];
      return this.formatList(texts);
    },
    geneSources() {
      const texts =  [
        'IGC', 'Virgo',
      ];
      return this.formatList(texts);
    },
    rowsPerPageItems() {
      return [this.size];
    },
    qParams() {
      const qParams = {
        size: this.pagination.rowsPerPage,
        page: this.pageNumber
      };
      if (this.geneSource && this.geneSource != 'all') {
        qParams.source = this.geneSource
      }
      if (this.filterGeneLength) {
        qParams.length__gt = this.geneLengthFilterRange[0];
        qParams.length__lt = this.geneLengthFilterRange[1];
      }
      if (this.searchGeneName) {
        qParams.name = this.searchGeneName;
      }
      if (this.functionID){
        qParams.function = this.functionID;
      }
      else if (this.filterWithFunction){
        qParams.has_functions = this.filterWithFunction;
      };
      if (this.taxChoice) {
        qParams.tax_id = this.taxItems[this.taxChoice];
      }
      return qParams;
    },
    maxGeneLength() {
      return 5000;
    },
    minGeneLength() {
      return 0;
    },
    taxChoices() {
      return Object.keys(this.taxItems).sort();
    },
    pageTotalNumber() {
      var numberPages = Math.floor(this.count/this.pagination.rowsPerPage);
      if (numberPages > this.maxNumberPages) {
        this.tooManyPages = true;
        return this.maxNumberPages;
      }
      if ((this.count % this.pagination.rowsPerPage) > 0) {
        return numberPages + 1;
      }
      return numberPages;
    },
    loadingFastaDownload() {
      return !this.downloadFastaReady;
    },
    loadingCSVDownload() {
      return !this.downloadCSVReady;
    },
    loadingDownloads() {
      if (this.loadingFastaDownload) {
        return true;
      }
      else if (this.loadingCSVDownload) {
        return true;
      }
      return false;
    },
    showDownloads() {
      if (this.count != 0 && this.count <= 100000) {
        return true;
      }
      return false;
    },
    downloadRight() {
      return !this.showFilters[0];
    },
    downloadLeft() {
      return this.showFilters[0];
    },
    downloadOpen() {
      if (this.showFilters[0]) {
        return 'right';
      }
      return 'left';
    },
    fileName() {
      if (this.searchGeneName) {
        return this.searchGeneName;
      }
      var basename = "metagenedb";
      if (this.geneSource && this.geneSource != 'all') {
        basename = `${basename}_${this.geneSource}`;
      }
      if (this.filterGeneLength) {
        basename = `${basename}_${this.geneLengthFilterRange[0]}bp-${this.geneLengthFilterRange[1]}bp`;
      }
      if (this.functionID){
        basename = `${basename}_function-${this.functionID}`;
      }
      if (this.taxChoice) {
        var taxName = this.taxChoice.replace(/ /g,"_");
        basename = `${basename}_${this.taxLevel}-${taxName}`;
      }
      return basename;
    },
    downloadAlerts() {
      return [
        {
          value: this.loadingFastaDownload,
          text: "Downloading sequences (.fasta)...",
          type: "info",
          progressBar: true,
        },
        {
          value: this.loadingCSVDownload,
          text: "Downloading metadata (.csv)...",
          type: "info",
          progressBar: true,
        },
        {
          value: this.fastaError,
          text: "Could not download sequences (.fasta).",
          type: "error",
        },
        {
          value: this.csvError,
          text: "Could not download metadata (.csv).",
          type: "error"
        },
      ]
    }
  },
  mounted() {
    this.getGenes();
  },
  methods: {
    formatList(texts) {
      var taxLevelsFormatted = [];
      texts.forEach((element) => {
        var taxObject = {};
        taxObject['text'] = element;
        taxObject['value'] = element.toLowerCase();
        taxLevelsFormatted.push(taxObject);
      })
      return taxLevelsFormatted;
    },
    resetGeneListFlags() {
      this.requestDone = false;
      this.listGenesError = false;
    },
    getGenes() {
      this.resetGeneListFlags();
      axios.get('/api/genes', {
        params: this.qParams,
        headers: {
          Accept: 'application/json',
        },
      })
        .then((response) => {
          const ncbiTaxUrl = 'https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?id=';
          const keggUrl = 'https://www.genome.jp/dbget-bin/www_bget?ko:';
          const genes = response.data.items;
          genes.forEach((item, index) => {
            if (item.taxonomy) {
              item.tax_url = ncbiTaxUrl + item.taxonomy.tax_id;
              item.taxonomy = `${item.taxonomy.name} (${item.taxonomy.rank})`;
            }
            if (item.functions) {
              const { functions } = item;
              item.keggs = [];
              item.eggnogs = [];
              functions.forEach((fun, ind) => {
                if (fun.source === 'kegg') {
                  item.keggs.push({
                    kegg_url: keggUrl + fun.function_id,
                    kegg_id: fun.function_id
                  })
                } else if (fun.source === 'eggnog') {
                  var eggnogItem = {
                    eggnog_id: fun.function_id
                  };
                  if (fun.function_id.startsWith("COG")) {
                    eggnogItem.eggnog_url = `https://ftp.ncbi.nih.gov/pub/COG/COG2014/static/byCOG/${fun.function_id}.html`
                  }
                  item.eggnogs.push(eggnogItem)
                }
              });
            }
          });
          this.genes = genes;
          this.count = response.data.count;
          this.requestDone = true;
          this.loadTable = false;
        })
        .catch((error) => {
          console.error(error);
          this.listGenesError = true;
          this.requestDone = true;
        });
    },
    forceFileDownload(response, fileName){
      const url = window.URL.createObjectURL(new Blob([response.data]))
      const link = document.createElement('a')
      link.href = url
      link.setAttribute('download', fileName) //or any other extension
      document.body.appendChild(link)
      link.click()
    },
    resetFastaFlags() {
      this.downloadFastaReady = false;
      this.fastaError = false;
    },
    downloadFasta() {
      this.resetFastaFlags();
      var qParams = {};
      Object.assign(qParams, this.qParams);
      qParams.fasta = "true";
      delete qParams['size']
      delete qParams['page']
      axios.get('/api/genes', {
        params: qParams,
        headers: {
          Accept: 'application/json',
        },
      })
        .then((response) => {
          this.downloadFastaReady = true;
          this.forceFileDownload(response, `${this.fileName}.fasta`);
        })
        .catch((error) => {
          console.error(error);
          this.fastaError = true;
          this.downloadFastaReady = true;
        });
    },
    resetCsvFlags() {
      this.downloadCSVReady = false;
      this.csvError = false;
    },
    downloadMetadataCSV() {
      this.resetCsvFlags();
      var qParams = {};
      Object.assign(qParams, this.qParams);
      qParams.csv = "true";
      delete qParams['size']
      delete qParams['page']
      axios.get('/api/genes', {
        params: qParams,
        headers: {
          Accept: 'application/json',
        },
      })
        .then((response) => {
          this.downloadCSVReady = true;
          this.forceFileDownload(response, `${this.fileName}.csv`);
        })
        .catch((error) => {
          console.error(error);
          this.csvError = true;
          this.downloadCSVReady = true;
        });
    },
    emptyGeneInformationFilter() {
      this.geneSource = 'all';
      this.filterGeneLength = false;
      this.geneLengthFilterRange = [0, 2000];
      this.getTaxoPresenceOptional();
    },
    emptyTaxonomyFilter() {
      this.taxLevel = null;
      this.taxChoice = null;
    },
    emptyFunctionFilter() {
      this.functionID = null;
      this.filterWithFunction = null;
    },
    emptyFilters() {
      this.emptyGeneInformationFilter();
      this.emptyTaxonomyFilter();
      this.emptyFunctionFilter();
    },
    getTaxoPresence() {
      var endpoint = `/api/statistics/genestatistics-${this.geneSource}-present-taxonomy-${this.taxLevel}`;
      axios.get(endpoint.toLowerCase(), {
        headers: {
          Accept: 'application/json',
        },
      })
        .then((response) => {
          this.taxItems = response.data.body.tax_ids;
        })
        .catch((error) => {
          console.error(error);
        });
    },
    getTaxoPresenceOptional() {
      if (!this.taxLevel){
        return null;
      }
      this.getTaxoPresence();
    },
    runNewSearch() {
      this.loadTable = true;
      this.tooManyPages = false;
      this.pageNumber = 1;
      this.getGenes();
    },
  },
};