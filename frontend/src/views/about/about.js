import Logos from '@/components/logos/logos.vue';
import SimpleListing from '@/components/listing/simplelisting/simplelisting.vue';

export default {
  name: 'about',
  computed: {
    techStack() {
      return [
        {
          icon: "fas fa-database",
          title: "PostgreSQL",
          content: "Database",
          url_all: "https://www.postgresql.org/",
          icon_color: "tertiary",
        },
        {
          icon: "fab fa-python",
          title: "SQLModel",
          content: "Backend - ORM",
          url_all: "https://sqlmodel.tiangolo.com/",
          icon_color: "kegg",
        },
        {
          icon: "fab fa-python",
          title: "FastAPI",
          content: "Backend - API",
          url_all: "https://fastapi.tiangolo.com/",
          icon_color: "kegg",
        },
        {
          icon: "fab fa-vuejs",
          title: "Vue.js",
          content: "Frontend",
          url_all: "https://vuejs.org/",
          icon_color: "green",
        },
        {
          icon: "fab fa-docker",
          title: "Docker Compose",
          content: "Local development environment",
          url_all: "https://docs.docker.com/compose/",
          icon_color: 'tertiary',
        },
        {
          icon: "fab fa-docker",
          title: "Kubernetes",
          content: "Production environment",
          url_all: "https://kubernetes.io/",
          icon_color: 'tertiary',
        },
      ]
    },
    archiImg() {
      return require("/app/public/img/archi.png")
    }
  },
  components: {
    logos: Logos, SimpleListing,
  },
}