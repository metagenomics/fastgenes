import axios from 'axios';
import Histogram from '@/components/graphs/histogram/histogram.vue';
import CountCard from '@/components/countcard/countcard.vue';
import Doughnut from '@/components/graphs/doughnut/doughnut.vue';


export default {
  name: 'stats',
  data() {
    return {
      // Gene source
      geneSource: 'All',
      // Gene length data
      geneLengthRequestDone: false,
      geneLengthLabels: [],
      geneLengthAll: {},
      geneLengthFunctions: {},
      geneLengthKegg: {},
      geneLengthEggnog: {},
      geneLengthTaxonomy: {},
      geneLengthFull: {},
      geneLengthWindowSize: 200,
      stopAt: 4000,
      // Gene counts data
      geneCountsAll: null,
      geneCountsFunctions: null,
      geneCountsKegg: null,
      geneCountsEggnog: null,
      geneCountsTax: null,
      geneCountsFull: null,
      // Taxonomy repartition
      taxoRequestDone: false,
      taxoCounts: {},
      taxLevel: 'phylum',
    };
  },
  computed: {
    selectLevel() {
      return ['kingdom', 'superkingdom', 'phylum', 'class', 'order', 'family', 'genus', 'species'];
    },
    geneSources() {
      return [
        'All', 'IGC', 'Virgo'
      ]
    },
    geneLengthData() {
      return {
        labels: this.geneLengthLabels,
        datasets: [
          this.geneLengthAll,
          this.geneLengthFunctions,
          this.geneLengthEggnog,
          this.geneLengthKegg,
          this.geneLengthTaxonomy,
          this.geneLengthFull,
        ],
      };
    },
    geneCountsElements() {
      return [
        {
          title: "All",
          color: "primary",
          text: this.geneCountsAll,
        },
        {
          title: "Functions",
          color: "function darken-1",
          text: this.geneCountsFunctions,
        },
        {
          title: "KEGG",
          color: "kegg",
          text: this.geneCountsKegg,
        },
        {
          title: "EggNOG",
          color: "eggnog darken-1",
          text: this.geneCountsEggnog,
        },
        {
          title: "Taxonomy",
          color: "taxonomy darken-1",
          text: this.geneCountsTax,
        },
        {
          title: "Full",
          color: "tertiary",
          text: this.geneCountsFull,
        },
      ]
    }
  },
  mounted() {
    this.getAllStats();
  },
  methods: {
    getGeneLengthGeneric(stats_id, label, backgroundColor, geneLengthObj, geneLengthLabelObj) {
      axios.get('/api/statistics/' + stats_id.toLowerCase(), {
        headers: {
          Accept: 'application/json',
        },
      })
        .then((response) => {
          this[geneLengthLabelObj] = response.data.body.labels
          this[geneLengthObj] = {
            data: response.data.body.counts,
            label: label,
            backgroundColor: backgroundColor,
          };
        })
        .catch((error) => {
          this[geneLengthLabelObj] = [];
          this[geneLengthObj] = {};
          console.error(error);
        }).then(() => this.geneLengthRequestDone = true);
    },
    getGeneLength() {
      this.getGeneLengthGeneric(
        `genestatistics-${this.geneSource}-gene-length-${this.geneLengthWindowSize}-${this.stopAt}-all`,
        'All',
        this.$vuetify.theme.primary,
        'geneLengthAll',
        'geneLengthLabels'
      );
    },
    getGeneLengthWithFunctions() {
      this.getGeneLengthGeneric(
        `genestatistics-${this.geneSource}-gene-length-${this.geneLengthWindowSize}-${this.stopAt}-with-functions`,
        'Functions',
        this.$vuetify.theme.function,
        'geneLengthFunctions',
        'geneLengthLabels'
      );
    },
    getGeneLengthWithKEGG() {
      this.getGeneLengthGeneric(
        `genestatistics-${this.geneSource}-gene-length-${this.geneLengthWindowSize}-${this.stopAt}-with-kegg`,
        'KEGG',
        this.$vuetify.theme.kegg,
        'geneLengthKegg',
        'geneLengthLabels'
      );
    },
    getGeneLengthWithEggNOG() {
      this.getGeneLengthGeneric(
        `genestatistics-${this.geneSource}-gene-length-${this.geneLengthWindowSize}-${this.stopAt}-with-eggnog`,
        'EggNOG',
        this.$vuetify.theme.eggnog,
        'geneLengthEggnog',
        'geneLengthLabels'
      );
    },
    getGeneLengthWithTaxonomy() {
      this.getGeneLengthGeneric(
        `genestatistics-${this.geneSource}-gene-length-${this.geneLengthWindowSize}-${this.stopAt}-with-taxonomy`,
        'Taxonomy',
        this.$vuetify.theme.taxonomy,
        'geneLengthTaxonomy',
        'geneLengthLabels'
      );
    },
    getGeneLengthWithFunctionsAndTaxonomy() {
      this.getGeneLengthGeneric(
        `genestatistics-${this.geneSource}-gene-length-${this.geneLengthWindowSize}-${this.stopAt}-with-function-tax`,
        'Full',
        this.$vuetify.theme.tertiary,
        'geneLengthFull',
        'geneLengthLabels'
      );
    },
    getAllGeneLength() {
      this.geneLengthDatasets = {};
      this.getGeneLength();
      this.getGeneLengthWithFunctions();
      this.getGeneLengthWithKEGG();
      this.getGeneLengthWithEggNOG();
      this.getGeneLengthWithTaxonomy();
      this.getGeneLengthWithFunctionsAndTaxonomy();
    },
    getGeneCountsGeneric(stats_id, geneCountsObj) {
      axios.get('/api/statistics/' + stats_id.toLowerCase(), {
        headers: {
          Accept: 'application/json',
        },
      })
        .then((response) => {
          this[geneCountsObj] = response.data.body.count.toString();
        })
        .catch((error) => {
          this[geneCountsObj] = "no data";
          console.error(error);
        });
    },
    getGeneCountsAll() {
      this.getGeneCountsGeneric(
        `genestatistics-${this.geneSource}-count-all`,
        'geneCountsAll'
      );
    },
    getGeneCountsFunctions() {
      this.getGeneCountsGeneric(
        `genestatistics-${this.geneSource}-count-has-function`,
        'geneCountsFunctions',
      );
    },
    getGeneCountsKeggs() {
      this.getGeneCountsGeneric(
        `genestatistics-${this.geneSource}-count-has-function-source-kegg`,
        'geneCountsKegg',
      );
    },
    getGeneCountsEggnogs() {
      this.getGeneCountsGeneric(
        `genestatistics-${this.geneSource}-count-has-function-source-eggnog`,
        'geneCountsEggnog',
      );
    },
    getGeneCountsTaxo() {
      this.getGeneCountsGeneric(
        `genestatistics-${this.geneSource}-count-has-taxonomy`,
        'geneCountsTax',
      );
    },
    getGeneCountsFull() {
      this.getGeneCountsGeneric(
        `genestatistics-${this.geneSource}-count-has-function-has-taxonomy`,
        'geneCountsFull',
      );
    },
    getGeneCounts() {
      // Method to call all different gene counts
      this.getGeneCountsAll();
      this.getGeneCountsFunctions();
      this.getGeneCountsKeggs();
      this.getGeneCountsEggnogs();
      this.getGeneCountsTaxo();
      this.getGeneCountsFull();
    },
    getTaxoCounts() {
      var endpoint = `/api/statistics/genestatistics-${this.geneSource}-taxonomy-repartition-${this.taxLevel}`;
      axios.get(endpoint.toLowerCase(), {
        headers: {
          Accept: 'application/json',
        },
      })
        .then((response) => {
          const chartId = `taxo_distri_${this.taxLevel}`;
          this.taxoCounts = {
            data: response.data.body.counts,
            labels: response.data.body.labels,
            colors: response.data.body.colors,
            level: this.taxLevel,
            chartId,
          };
        })
        .catch((error) => {
          console.error(error);
        }).then(() => this.taxoRequestDone = true);
    },
    getAllStats() {
      this.getAllGeneLength();
      this.getGeneCounts();
      this.getTaxoCounts();
    },
  },
  components: {
    histogram: Histogram,
    countcard: CountCard,
    doughnut: Doughnut,
  },
};