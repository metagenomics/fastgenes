import Vue from 'vue';
import Vuetify from 'vuetify';
import App from './App.vue';
import router from './router';
import store from './store';
import './registerServiceWorker';
import 'vuetify/dist/vuetify.min.css';
import './assets/styles/fonts.css';
import '@fortawesome/fontawesome-free/css/all.css';

var pouet = 2;

Vue.config.productionTip = false;
Vue.use(Vuetify, {
  iconfont: 'fa',
  theme: {
    // Theme colors
    primary: '#263238',
    secondary: '#508991',
    tertiary: '#104f55',
    dropdown: '#d8e2e3',
    // Category colors
    function: '#db5461',
    kegg: '#db9d47',
    eggnog: '#028090',
    taxonomy: '#564138',
    nodata: '#f7ebec',
    // Taxonomy colors
    superkingdom: '#28536b',
    kingdom: '#c2948a',
    phylum: '#7ea8be',
    class: '#e2d4b7',
    order: '#a0c1b9',
    family: '#bcbdc0',
    genus: '#734b5e',
    species: '#8499b1',
  },
});

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app');
