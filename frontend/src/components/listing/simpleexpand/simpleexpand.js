export default {
  name: 'SimpleExpand',
  props: {
    expandData: Array,
    color: String,
  },
  computed: {
    colors() {
      var lighten4 = `${this.color} lighten-4`;
      if (this.color) {
        var lighten5 = `${this.color} lighten-5`;
      }
      else {
        var lighten5 = `${this.defaultColor} lighten-5`;
      }
      return {
        lighten_4: lighten4,
        lighten_5: lighten5,
      }
    },
    defaultColor () {
      return "blue-grey"
    }
  }
};