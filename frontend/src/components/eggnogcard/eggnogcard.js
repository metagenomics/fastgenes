import axios from 'axios';
import SimpleExpand from '@/components/listing/simpleexpand/simpleexpand.vue';
import SimpleListing from '@/components/listing/simplelisting/simplelisting.vue';

export default {
  components: {
    SimpleExpand, SimpleListing,
  },
  name: 'EggNogCard',
  props: {
    eggnogId: String,
    color: String,
  },
  mounted() {
    this.getEggNogDetail();
  },
  data() {
    return {
      eggnogDetails: [],
      eggnogExpandDetails: [],
      eggnogReferences: [],
      requestDone: false,
      eggnogVersion: '',
    };
  },
  computed: {
    funcCatGroups() {
      return {
        'info_storage_processing': 'Information Storage and Processing',
        'cellular_processes_signaling': 'Cellular Processes and Signaling',
        'metabolism': 'Metabolism',
        'poorly_characterized': 'Poorly Characterized',
      }
    },
    displayVersion() {
      if (this.eggnogVersion) {
        return `v.${this.eggnogVersion}`;
      }
      return '';
    },
  },
  methods: {
    getEggNogDetail() {
      if (this.eggnogId == 'no_eggnog') {
        this.requestDone = true;
        return;
      }
      axios.get(`/api/eggnogs/${this.eggnogId}`, {
        headers: {
          Accept: 'application/json',
        },
      })
        .then((response) => {
          this.buildEggNogDetails(response);
          this.buildEggNogExpandDetails(response);
        })
        .catch((error) => {
          console.log(error);
        })
        .finally(() => {
          this.requestDone = true;
        });
    },
    buildEggNogDetails(response) {
      this.eggnogVersion = response.data.version
      var eggnogIdItem = {
        title: 'ID',
        content: response.data.function_id,
      };
      if (this.eggnogId.startsWith('COG')) {
        eggnogIdItem.url = `https://ftp.ncbi.nih.gov/pub/COG/COG2014/static/byCOG/${eggnogIdItem.content}.html`;
        eggnogIdItem.url_label = 'Open in NCBI'
      }
      this.eggnogDetails = [
        eggnogIdItem,
        {
          title: 'Name',
          content: response.data.name,
        },
      ];
    },
    buildEggNogExpandDetails(response) {
      this.eggnogExpandDetails = [
        this.buildFunctionalCategories(response),
      ];
    },
    buildFunctionalCategories(response) {
      const funcCats = {
        title: 'Functional Categories',
        icon: 'fas fa-layer-group',
        content: [],
      };
      Object.entries(response.data.functional_categories).forEach(([key, value]) => {
        const name = `(${value.category_id}) ${value.name}`;
        funcCats.content.push(
          {
            id: name,
            name: this.funcCatGroups[value.group],
          },
        );
      });
      return funcCats;
    },
  },
};