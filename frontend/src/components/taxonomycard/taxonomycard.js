import axios from 'axios';

export default {
  name: 'TaxonomyCard',
  props: {
    taxonomyId: String,
  },
  mounted() {
    this.getTaxonomyDetail();
  },
  data() {
    return {
      taxonomyDetail: [],
      requestDone: false,
    };
  },
  methods: {
    getTaxonomyDetail() {
      axios.get(`/api/taxonomy/${this.taxonomyId}`, {
        headers: {
          Accept: 'application/json',
        },
      })
        .then((response) => {
          const ncbi_tax_url = 'https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?';
          this.taxonomyDetail = [
            {
              title: 'Superkingdom',
              content: response.data.hierarchy.superkingdom,
              color: 'superkingdom',
            },
            {
              title: 'Kingdom',
              content: response.data.hierarchy.kingdom,
              color: 'kingdom',
            },
            {
              title: 'Phylum',
              content: response.data.hierarchy.phylum,
              color: 'phylum',
            },
            {
              title: 'Class',
              content: response.data.hierarchy.class,
              color: 'class',
            },
            {
              title: 'Order',
              content: response.data.hierarchy.order,
              color: 'order',
            },
            {
              title: 'Family',
              content: response.data.hierarchy.family,
              color: 'family',
            },
            {
              title: 'Genus',
              content: response.data.hierarchy.genus,
              color: 'genus',
            },
            {
              title: 'Species',
              content: response.data.hierarchy.species,
              color: 'species',
            },
          ];
          this.taxonomyDetail.forEach((item, index) => {
            if (item.content) {
              item.url_info = ncbi_tax_url + "mode=info&id=" + item.content.tax_id;
              item.url_tree = ncbi_tax_url + "mode=tree&id=" + item.content.tax_id;
            }
          });
        })
        .catch((error) => {
          console.log(error);
        })
        .finally(() => {
          this.requestDone = true;
        });
    },
  },
};