import Chart from 'chart.js';

export default {
  props: {
    doughnutData: {
      type: Object,
      required: true,
    },
    chartId: String,
    requestDone: Boolean,
  },
  data() {
    return {
      myChart: {},
      options: {},
      colors: [],
      hideLegend: false,
      legendPosition: 'right',
      labels: [],
      hideLabels: ['No annotation'],
      noGraph: true,
    };
  },
  computed: {
    hideLabelsLabel() {
      if (Object.entries(this.doughnutData).length == 0) {
        return 'Hide fields';
      }
      return `Hide ${this.doughnutData.level}`;
    },
    displayLegend() {
      return !this.hideLegend;
    },
  },
  methods: {
    createChart() {
      const ctx = document.getElementById(this.chartId);
      this.myChart = new Chart(ctx, {
        type: 'doughnut',
      });
    },
    updateChart() {
      this.labels = this.doughnutData.labels;
      this.updateChartOptions();
      this.updateChartData();
    },
    updateChartOptions() {
      this.options = {
        cutoutPercentage: 60,
        legend: {
          display: this.displayLegend,
          position: this.legendPosition,
          labels: {
            boxWidth: 12,
            padding: 8,
          },
        },
      };
      this.myChart.options = this.options;
      this.myChart.update();
    },
    updateChartData() {
      const dataDict = {};
      const colorDict = {};
      this.labels.forEach((key, i) => dataDict[key] = this.doughnutData.data[i]);
      this.labels.forEach((key, i) => colorDict[key] = this.doughnutData.colors[i]);
      for (let i = 0; i < this.hideLabels.length; i++) {
        delete dataDict[this.hideLabels[i]];
        delete colorDict[this.hideLabels[i]];
      }
      this.myChart.data = {
        labels: Object.keys(dataDict),
        datasets: [
          {
            data: Object.values(dataDict),
            backgroundColor: Object.values(colorDict),
          },
        ],
      };
      this.myChart.update();
    },
  },
  watch: {
    doughnutData(val) {
      if (this.noGraph) {
        this.noGraph = false;
        this.createChart();
      }
      this.updateChart();
    },
  },
};