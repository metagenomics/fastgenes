#!/bin/sh

echo "$CI_REGISTRY_PASSWORD" | docker login -u "$CI_REGISTRY_USER" --password-stdin $CI_REGISTRY

# Build backend
docker build -t "$CI_REGISTRY_IMAGE/backend:${CI_COMMIT_REF_NAME}" backend/
docker tag "$CI_REGISTRY_IMAGE/backend:${CI_COMMIT_REF_NAME}" "$CI_REGISTRY_IMAGE/backend:latest"
docker push "$CI_REGISTRY_IMAGE/backend:${CI_COMMIT_REF_NAME}"
