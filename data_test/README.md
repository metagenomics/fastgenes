# Data test

This directory contains different test files for the creation of a light test database.

## NCBI taxonomy test data

in the `ncbi_taxonomy` is a test `taxdump.tar.gz` made of `nodes.dmp` and `names.dmp` files.

> Do not forget to regenerate the `taxdump.tar.gz` when modifying either `nodes.dmp` or `names.dmp` files. (`tar -czvf taxdump.tar.gz *.dmp`)

## KEGG

No test data for KEGG since the whole KEGG KO database is very quick to create and does not take that much disk space.

## Genes

### Sequences

Sequences are found in the `genes.fa` file.

### Annotations

#### KEGG

Kegg annotations are based on gene_id and kegg_id and are found in `kegg.csv`.