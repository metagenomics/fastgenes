from logging.config import fileConfig

from sqlalchemy import engine_from_config
from sqlalchemy import pool
from sqlmodel import SQLModel

from alembic import context

from app.db import _build_database_url
from app.db import engine

config = context.config
fileConfig(config.config_file_name)

# Model / Schema imports
from app.core.models import (
    KeggOrthology, Eggnog, NcbiTaxonomy,
    KeggOrthologyAnnotation, EggnogAnnotation, NcbiTaxonomyAnnotation,
    Gene, Experiment, Catalog,
    GeneCatalogLink, GeneEggnogAnnotationLink, GeneKeggOrthologyAnnotationLink, GeneNcbiTaxonomyLink
)


target_metadata = SQLModel.metadata

def run_migrations_offline():
    """Run migrations in 'offline' mode.

    This configures the context with just a URL
    and not an Engine, though an Engine is acceptable
    here as well.  By skipping the Engine creation
    we don't even need a DBAPI to be available.

    Calls to context.execute() here emit the given string to the
    script output.

    """
    url = _build_database_url()
    context.configure(
        url=url,
        target_metadata=target_metadata,
        literal_binds=True,
        dialect_opts={"paramstyle": "named"},
    )

    with context.begin_transaction():
        context.run_migrations()


def run_migrations_online():
    """Run migrations in 'online' mode.

    In this scenario we need to create an Engine
    and associate a connection with the context.

    """
    connectable = engine
    print(connectable)

    with connectable.connect() as connection:
        context.configure(
            connection=connection, target_metadata=target_metadata
        )

        with context.begin_transaction():
            context.run_migrations()


if context.is_offline_mode():
    run_migrations_offline()
else:
    run_migrations_online()
