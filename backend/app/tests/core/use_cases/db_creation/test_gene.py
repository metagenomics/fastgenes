import os
import unittest
from unittest.mock import Mock

from sqlalchemy.exc import NoResultFound

from app.core.use_cases.db_creation.gene import (
    CreateOrUpdateGenesUseCase,
)

CREATE_ACTION = "CREATE_ACTION"
UPDATE_ACTION = "UPDATE_ACTION"

EXISTING_CATALOG = "existing_catalog"


class FakeSequence:
    def __init__(self, name, seq):
        self.name = name
        self.seq = seq


class MockGenesRepo:
    def get_all(self, **kwargs):
        return {}

    def create_batch(self, *args, **kwargs):
        return CREATE_ACTION

    def update_batch(self, *args, **kwargs):
        return UPDATE_ACTION

    def get_all_ids(self, *args, **kwargs):
        return {"gene1", "gene2"}


class MockCatalogsRepo:
    def get(self, catalog_name, **kwargs):
        if catalog_name == EXISTING_CATALOG:
            return Mock(id=1)
        raise NoResultFound

    def create(self, *args, **kwargs):
        return Mock(id=2)


class TestCreateOrUpdateGenesUseCase(unittest.TestCase):
    def test_generate_items_no_catalog(self):
        # Given
        fasta_path = os.path.join("fake_path.fa")
        use_case_test = CreateOrUpdateGenesUseCase(
            repo=MockGenesRepo(),
            fasta_path=fasta_path,
            batch_size=2,
        )
        name = "seq"
        sequence = "actg"
        test_sequence = FakeSequence(name=name, seq=sequence)
        print(test_sequence)
        expected_output = {"gene_id": name, "sequence": sequence, "catalogs": None}
        # When
        create_item = use_case_test._generate_create_item(test_sequence)
        update_item = use_case_test._generate_update_item(test_sequence)
        # Then
        self.assertDictEqual(create_item.dict(), expected_output)
        self.assertDictEqual(update_item.dict(), expected_output)

    def test_generate_items_existing_catalog_no_catalog_id(self):
        # Given
        fasta_path = os.path.join("fake_path.fa")
        use_case_test = CreateOrUpdateGenesUseCase(
            repo=MockGenesRepo(),
            fasta_path=fasta_path,
            batch_size=2,
            catalog_name=EXISTING_CATALOG,
            catalog_repo=MockCatalogsRepo(),
        )
        # Then
        with self.assertRaises(UserWarning):
            use_case_test.catalog_id

    def test_generate_items_existing_catalog(self):
        # Given
        fasta_path = os.path.join("fake_path.fa")
        use_case_test = CreateOrUpdateGenesUseCase(
            repo=MockGenesRepo(),
            fasta_path=fasta_path,
            batch_size=2,
            catalog_name=EXISTING_CATALOG,
            catalog_repo=MockCatalogsRepo(),
        )
        use_case_test.catalog_id = use_case_test._get_catalog_id()
        name = "seq"
        sequence = "actg"
        test_sequence = FakeSequence(name=name, seq=sequence)
        print(test_sequence)
        expected_output = {"gene_id": name, "sequence": sequence, "catalogs": [1]}
        # When
        create_item = use_case_test._generate_create_item(test_sequence)
        update_item = use_case_test._generate_update_item(test_sequence)
        # Then
        self.assertDictEqual(create_item.dict(), expected_output)
        self.assertDictEqual(update_item.dict(), expected_output)

    def test_generate_items_new_catalog(self):
        # Given
        fasta_path = os.path.join("fake_path.fa")
        use_case_test = CreateOrUpdateGenesUseCase(
            repo=MockGenesRepo(),
            fasta_path=fasta_path,
            batch_size=2,
            catalog_name="new",
            catalog_repo=MockCatalogsRepo(),
        )
        use_case_test.catalog_id = use_case_test._get_catalog_id()
        name = "seq"
        sequence = "actg"
        test_sequence = FakeSequence(name=name, seq=sequence)
        print(test_sequence)
        expected_output = {"gene_id": name, "sequence": sequence, "catalogs": [2]}
        # When
        create_item = use_case_test._generate_create_item(test_sequence)
        update_item = use_case_test._generate_update_item(test_sequence)
        # Then
        self.assertDictEqual(create_item.dict(), expected_output)
        self.assertDictEqual(update_item.dict(), expected_output)

    def test_create_or_update_all_entries_no_fasta(self):
        # Given
        use_case_test = CreateOrUpdateGenesUseCase(
            repo=MockGenesRepo(),
            batch_size=2,
        )
        # Then
        with self.assertRaises(UserWarning):
            use_case_test.create_or_update_all_entries()

    def test_create_or_update_all_entries_from_fasta(self):
        # Given
        fasta_path = os.path.join(os.path.dirname(__file__), "data/genes.fa")
        use_case_test = CreateOrUpdateGenesUseCase(
            repo=MockGenesRepo(),
            fasta_path=fasta_path,
            batch_size=2,
        )
        expected_output = [
            {
                "created": 1,
                "updated": 2,
                "deleted": 0,
                "skipped": 0,
                "extra": None,
                "name": use_case_test.BASE_OPERATION_NAME,
            }
        ]
        # When
        tested_output = use_case_test.create_or_update_all_entries()
        # Then
        self.assertListEqual(tested_output, expected_output)

    def test_create_or_update_all_entries_from_fasta_gz(self):
        # Given
        fasta_path = os.path.join(os.path.dirname(__file__), "data/genes.fasta.gz")
        use_case_test = CreateOrUpdateGenesUseCase(
            repo=MockGenesRepo(),
            fasta_path=fasta_path,
            batch_size=2,
        )
        expected_output = [
            {
                "created": 1,
                "updated": 2,
                "deleted": 0,
                "skipped": 0,
                "extra": None,
                "name": use_case_test.BASE_OPERATION_NAME,
            }
        ]
        # When
        tested_output = use_case_test.create_or_update_all_entries()
        # Then
        self.assertListEqual(tested_output, expected_output)

    def test_create_or_update_all_entries_with_existing_catalog(self):
        # Given
        fasta_path = os.path.join(os.path.dirname(__file__), "data/genes.fa")
        use_case_test = CreateOrUpdateGenesUseCase(
            repo=MockGenesRepo(),
            fasta_path=fasta_path,
            batch_size=2,
            catalog_name=EXISTING_CATALOG,
            catalog_repo=MockCatalogsRepo(),
        )
        expected_output = [
            {
                "created": 1,
                "updated": 2,
                "deleted": 0,
                "skipped": 0,
                "extra": None,
                "name": use_case_test.BASE_OPERATION_NAME,
            }
        ]
        # When
        tested_output = use_case_test.create_or_update_all_entries()
        # Then
        self.assertListEqual(tested_output, expected_output)
        self.assertEqual(use_case_test.catalog_id, 1)

    def test_create_or_update_all_entries_with_unexisting_catalog(self):
        # Given
        fasta_path = os.path.join(os.path.dirname(__file__), "data/genes.fa")
        use_case_test = CreateOrUpdateGenesUseCase(
            repo=MockGenesRepo(),
            fasta_path=fasta_path,
            batch_size=2,
            catalog_name="I-do-not-exist",
            catalog_repo=MockCatalogsRepo(),
        )
        expected_output = [
            {
                "created": 1,
                "updated": 2,
                "deleted": 0,
                "skipped": 0,
                "extra": None,
                "name": use_case_test.BASE_OPERATION_NAME,
            }
        ]
        # When
        tested_output = use_case_test.create_or_update_all_entries()
        # Then
        self.assertListEqual(tested_output, expected_output)
        self.assertEqual(use_case_test.catalog_id, 2)
