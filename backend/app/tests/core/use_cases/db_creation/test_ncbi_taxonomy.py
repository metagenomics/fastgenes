import os
import unittest

from app.core.use_cases.db_creation.ncbi_taxonomy import (
    CreateOrUpdateNcbiTaxonomyEntriesUseCase,
)
from app.core.utils.parsers.ncbi_taxonomy import NcbiTaxonomyNode


CREATE_ACTION = "CREATE_ACTION"
UPDATE_ACTION = "UPDATE_ACTION"


class MockNcbiTaxonomysRepo:
    def get_all_ids(self, **kwargs):
        return {
            10,
        }

    def get_tax_id_to_id_mapping(self, **kwargs):
        return {10: 1}

    def create_batch(self, *args, **kwargs):
        return CREATE_ACTION

    def update_batch(self, *args, **kwargs):
        return UPDATE_ACTION


class TestCreateOrUpdateNcbiTaxonomyEntriesUseCaseNoPreLoad(unittest.TestCase):
    def setUp(self) -> None:
        self.use_case_test = CreateOrUpdateNcbiTaxonomyEntriesUseCase(
            repo=MockNcbiTaxonomysRepo(), preload_data=False, batch_size=2
        )

    def test_create_or_update_all_entries_one_item(self):
        # Given
        entries = [NcbiTaxonomyNode(tax_id=1, parent_tax_id=0, rank="genus")]
        names = {1: "test-one"}
        self.use_case_test._all_entries = entries
        self.use_case_test._names_dict = names
        expected_output = [
            {
                "created": 1,
                "updated": 0,
                "deleted": 0,
                "skipped": 0,
                "extra": None,
                "name": self.use_case_test.BASE_OPERATION_NAME,
            },
            {
                "created": 0,
                "name": "Link to parents",
                "updated": 1,
                "deleted": 0,
                "skipped": 0,
                "extra": None,
            },
        ]
        # When
        tested_output = self.use_case_test.create_or_update_all_entries()
        # Then
        self.assertListEqual(tested_output, expected_output)

    def test_create_or_update_all_entries_three_item(self):
        # Given
        entries = [
            NcbiTaxonomyNode(tax_id=1, parent_tax_id=0, rank="genus"),
            NcbiTaxonomyNode(tax_id=10, parent_tax_id=1, rank="species"),
            NcbiTaxonomyNode(tax_id=101, parent_tax_id=10, rank="other"),
        ]
        names = {1: "test-one", 10: "test-two", 101: "test-three"}
        self.use_case_test._all_entries = entries
        self.use_case_test._names_dict = names
        expected_output = [
            {
                "created": 2,
                "updated": 1,
                "deleted": 0,
                "skipped": 0,
                "name": self.use_case_test.BASE_OPERATION_NAME,
                "extra": None,
            },
            {
                "created": 0,
                "extra": None,
                "name": "Link to parents",
                "updated": 3,
                "deleted": 0,
                "skipped": 0,
            },
        ]
        # When
        tested_output = self.use_case_test.create_or_update_all_entries()
        # Then
        self.assertListEqual(tested_output, expected_output)

    def test_names_dict(self):
        # Given
        self.use_case_test.nodes_path = os.path.join(
            os.path.dirname(__file__), "data/nodes_test.dmp"
        )
        self.use_case_test.names_path = os.path.join(
            os.path.dirname(__file__), "data/names_test.dmp"
        )
        expected_dict = {
            1: "root",
            2: "Bacteria",
            6: "Azorhizobium",
            7: "Azorhizobium caulinodans",
        }
        # When
        tested_dict = self.use_case_test.names_dict
        # Then
        self.assertDictEqual(tested_dict, expected_dict)

    def test_all_entries(self):
        # Given
        self.use_case_test.nodes_path = os.path.join(
            os.path.dirname(__file__), "data/nodes_test.dmp"
        )
        self.use_case_test.names_path = os.path.join(
            os.path.dirname(__file__), "data/names_test.dmp"
        )
        expected_entries = [
            NcbiTaxonomyNode(tax_id=1, parent_tax_id=1, rank="no_rank"),
            NcbiTaxonomyNode(tax_id=2, parent_tax_id=131567, rank="superkingdom"),
            NcbiTaxonomyNode(tax_id=6, parent_tax_id=335928, rank="genus"),
            NcbiTaxonomyNode(tax_id=7, parent_tax_id=6, rank="species"),
        ]
        # When
        tested_entries = self.use_case_test.all_entries
        # Then
        self.assertListEqual(tested_entries, expected_entries)


class TestCreateOrUpdateNcbiTaxonomyEntriesUseCasePreLoad(unittest.TestCase):
    def setUp(self) -> None:
        nodes_path = os.path.join(os.path.dirname(__file__), "data/nodes_test.dmp")
        names_path = os.path.join(os.path.dirname(__file__), "data/names_test.dmp")
        self.use_case_test = CreateOrUpdateNcbiTaxonomyEntriesUseCase(
            repo=MockNcbiTaxonomysRepo(),
            nodes_path=nodes_path,
            names_path=names_path,
            preload_data=True,
        )

    def test_create_or_update_all_entries(self):
        # Given
        expected_output = [
            {
                "created": 4,
                "updated": 0,
                "deleted": 0,
                "skipped": 0,
                "name": self.use_case_test.BASE_OPERATION_NAME,
                "extra": None,
            },
            {
                "created": 0,
                "extra": None,
                "name": "Link to parents",
                "updated": 4,
                "deleted": 0,
                "skipped": 0,
            },
        ]
        # When
        tested_output = self.use_case_test.create_or_update_all_entries()
        # Then
        self.assertListEqual(tested_output, expected_output)

    def test_overwrite_with_setters(self):
        # Given
        nodes_path = os.path.join(os.path.dirname(__file__), "data/nodes_bis_test.dmp")
        names_path = os.path.join(os.path.dirname(__file__), "data/names_bis_test.dmp")
        self.use_case_test.nodes_path = nodes_path
        self.use_case_test.names_path = names_path
        self.use_case_test._preload_data()
        expected_output = [
            {
                "created": 2,
                "updated": 0,
                "deleted": 0,
                "skipped": 0,
                "name": self.use_case_test.BASE_OPERATION_NAME,
                "extra": None,
            },
            {
                "created": 0,
                "extra": None,
                "name": "Link to parents",
                "updated": 2,
                "deleted": 0,
                "skipped": 0,
            },
        ]
        # When
        tested_output = self.use_case_test.create_or_update_all_entries()
        # Then
        self.assertListEqual(tested_output, expected_output)

    def test_names_dict(self):
        # Given
        expected_dict = {
            1: "root",
            2: "Bacteria",
            6: "Azorhizobium",
            7: "Azorhizobium caulinodans",
        }
        # When
        tested_dict = self.use_case_test.names_dict
        # Then
        self.assertDictEqual(tested_dict, expected_dict)

    def test_all_entries(self):
        # Given
        expected_entries = [
            NcbiTaxonomyNode(tax_id=1, parent_tax_id=1, rank="no_rank"),
            NcbiTaxonomyNode(tax_id=2, parent_tax_id=131567, rank="superkingdom"),
            NcbiTaxonomyNode(tax_id=6, parent_tax_id=335928, rank="genus"),
            NcbiTaxonomyNode(tax_id=7, parent_tax_id=6, rank="species"),
        ]
        # When
        tested_entries = self.use_case_test.all_entries
        # Then
        self.assertListEqual(tested_entries, expected_entries)


class TestCreateOrUpdateNcbiTaxonomyEntriesUseCasePreLoadErrors(unittest.TestCase):
    def test_no_nodes_path(self):
        # Given
        names_path = os.path.join(os.path.dirname(__file__), "data/names_test.dmp")
        # Then
        with self.assertRaises(UserWarning):
            self.use_case_test = CreateOrUpdateNcbiTaxonomyEntriesUseCase(
                repo=MockNcbiTaxonomysRepo(),
                names_path=names_path,
                preload_data=True,
            )

    def test_no_names_path(self):
        # Given
        nodes_path = os.path.join(os.path.dirname(__file__), "data/nodes_test.dmp")
        # Then
        with self.assertRaises(UserWarning):
            self.use_case_test = CreateOrUpdateNcbiTaxonomyEntriesUseCase(
                repo=MockNcbiTaxonomysRepo(),
                nodes_path=nodes_path,
                preload_data=True,
            )
