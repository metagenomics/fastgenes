import unittest
from unittest.mock import patch

from dabeplech.models.kegg import LightKeggOrthologyModel, KeggOrthologyListModel

from app.core.use_cases.db_creation.kegg import (
    CreateOrUpdateKeggOrthologyEntriesUseCase,
)

CREATE_ACTION = "CREATE_ACTION"
UPDATE_ACTION = "UPDATE_ACTION"


class MockKeggOrthologysRepo:
    def get_all(self, **kwargs):
        return {}

    def create_batch(self, *args, **kwargs):
        return CREATE_ACTION

    def update_batch(self, *args, **kwargs):
        return UPDATE_ACTION

    def get_all_ids(self, *args, **kwargs):
        return {"k01", "k02", "k03"}


class TestCreateOrUpdateKeggOrthologyEntriesUseCase(unittest.TestCase):
    def setUp(self) -> None:
        self.use_case_test = CreateOrUpdateKeggOrthologyEntriesUseCase(
            repo=MockKeggOrthologysRepo(), batch_size=2
        )

    @patch("app.core.use_cases.db_creation.kegg.KEGGAPI")
    def test_create_or_update_all_entries_one_item(self, mock_KEGGAPI):
        # Given
        entries = [LightKeggOrthologyModel(entry_id="k01", names=["K1"])]
        list_ko_result = KeggOrthologyListModel(entries=entries)
        mc = mock_KEGGAPI.return_value
        mc.list.return_value = list_ko_result
        expected_output = [
            {
                "created": 0,
                "updated": 1,
                "deleted": 0,
                "skipped": 0,
                "extra": None,
                "name": self.use_case_test.BASE_OPERATION_NAME,
            }
        ]
        # When
        tested_output = self.use_case_test.create_or_update_all_entries()
        # Then
        self.assertListEqual(tested_output, expected_output)

    @patch("app.core.use_cases.db_creation.kegg.KEGGAPI")
    def test_create_or_update_all_entries_three_item(self, mock_KEGGAPI):
        # Given
        entries = [
            LightKeggOrthologyModel(entry_id="k01", names=["K1"]),
            LightKeggOrthologyModel(entry_id="k02", names=["K2"]),
            LightKeggOrthologyModel(entry_id="k03", names=["K3"]),
            LightKeggOrthologyModel(entry_id="k04", names=["K4"]),
            LightKeggOrthologyModel(entry_id="k05", names=["K5"]),
            LightKeggOrthologyModel(entry_id="k06", names=["K6"]),
        ]
        list_ko_result = KeggOrthologyListModel(entries=entries)
        mc = mock_KEGGAPI.return_value
        mc.list.return_value = list_ko_result
        expected_output = [
            {
                "created": 3,
                "updated": 3,
                "deleted": 0,
                "skipped": 0,
                "extra": None,
                "name": self.use_case_test.BASE_OPERATION_NAME,
            }
        ]
        # When
        tested_output = self.use_case_test.create_or_update_all_entries()
        # Then
        self.assertListEqual(tested_output, expected_output)
