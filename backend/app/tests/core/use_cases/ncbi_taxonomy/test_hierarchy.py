from typing import Optional
import unittest

from sqlmodel import SQLModel

from app.core.use_cases.ncbi_taxonomy.hierarchy import (
    GenerateNcbiTaxonomyHierarchyUseCase,
)


UPDATE_ACTION = "UPDATE_ACTION"


class MockNcbiTaxonomy(SQLModel):
    tax_id: int
    hierarchy: Optional[dict]

    def build_hierarchy(self) -> dict:
        return {self.tax_id: self.tax_id + 10}


class MockNcbiTaxonomysRepo:
    def get_all_ids(self, **kwargs):
        return {10, 11, 12, 13}

    def get_entries(self, ids, **kwargs):
        all_entries = [
            MockNcbiTaxonomy(tax_id=10),
            MockNcbiTaxonomy(tax_id=11),
            MockNcbiTaxonomy(tax_id=12),
            MockNcbiTaxonomy(tax_id=13),
        ]
        return [tax for tax in all_entries if tax.tax_id in ids]

    def update_batch(self, *args, **kwargs):
        return UPDATE_ACTION


class TestGenerateNcbiTaxonomyHierarchyUseCase(unittest.TestCase):
    def test_generate_all_ncbi_taxonomy_hierarchy(self):
        # Given
        batch_size = 2
        expected_output = {
            "updated": 4,
            "created": 0,
            "deleted": 0,
            "skipped": 0,
            "extra": None,
            "name": "create-NCBI-taxonomy-hierarchy",
        }
        # When
        test_use_case = GenerateNcbiTaxonomyHierarchyUseCase(
            ncbi_taxonomy_repo=MockNcbiTaxonomysRepo(), batch_size=batch_size
        )
        # Then
        self.assertDictEqual(
            test_use_case.generate_all_ncbi_taxonomy_hierarchy().dict(), expected_output
        )
