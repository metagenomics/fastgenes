import unittest

from app.core.use_cases.crud.experiment import CrudExperimentUseCase
from app.core.schemas.entities.experiment import ExperimentCreate, ExperimentUpdate

GET_ACTION = "Returning experiment"
GET_ALL_ACTION = "Returning all experiments"
CREATE_ACTION = "Creating experiment"
UPDATE_ACTION = "Updating experiment"
DELETE_ACTION = "Deleting experiment"


class MockExperimentsRepo:
    def get(self, experiment_id: str):
        return GET_ACTION

    def get_all(self):
        return GET_ALL_ACTION

    def create(self, experiment_input: ExperimentCreate):
        return CREATE_ACTION

    def update(self, experiment_input: ExperimentUpdate):
        return UPDATE_ACTION

    def delete(self, experiment_id: str):
        return DELETE_ACTION


class TestCrudExperimentUseCase(unittest.TestCase):
    def setUp(self) -> None:
        self.use_case_test = CrudExperimentUseCase(
            experiments_repo=MockExperimentsRepo()
        )

    def test_create_experiment(self):
        # Given
        experiment_test = ExperimentCreate(name="test-experiment", date="2020-10-10")
        expected_output = CREATE_ACTION
        # When
        test_output = self.use_case_test.create_experiment(experiment_test)
        # Then
        self.assertEqual(test_output, expected_output)

    def test_update_experiment(self):
        # Given
        experiment_test = ExperimentUpdate(id=1, name="test-experiment")
        expected_output = UPDATE_ACTION
        # When
        test_output = self.use_case_test.update_experiment(experiment_test)
        # Then
        self.assertEqual(test_output, expected_output)

    def test_delete_experiment(self):
        # Given
        experiment_id_test = "test-experiment"
        expected_output = DELETE_ACTION
        # When
        test_output = self.use_case_test.delete_experiment(experiment_id_test)
        # Then
        self.assertEqual(test_output, expected_output)

    def test_get_experiment(self):
        # Given
        experiment_id_test = "test-experiment"
        expected_output = GET_ACTION
        # When
        test_output = self.use_case_test.get_experiment(experiment_id_test)
        # Then
        self.assertEqual(test_output, expected_output)

    def test_get_all(self):
        # Given
        expected_output = GET_ALL_ACTION
        # When
        output = self.use_case_test.get_all()
        # Then
        self.assertEqual(output, expected_output)
