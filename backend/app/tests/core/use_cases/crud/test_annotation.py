import unittest

from app.core.use_cases.crud.annotations import (
    CrudEggnogAnnotationUseCase,
    CrudKeggOrthologyAnnotationUseCase,
    CrudNcbiTaxonomyAnnotationUseCase,
)
from app.core.schemas.entities.annotations import (
    EggnogAnnotationCreate,
    KeggOrthologyAnnotationCreate,
    NcbiTaxonomyAnnotationCreate,
)

GET_ACTION = "Returning annotation"
GET_ALL_ACTION = "Returning all annotations"
CREATE_ACTION = "Creating annotation"
DELETE_ACTION = "Deleting annotation"


class _BaseMockAnnotationsRepo:
    def get(self, annotation_id: str):
        return GET_ACTION

    def get_all(self):
        return GET_ALL_ACTION

    def delete(self, annotation_id: str):
        return DELETE_ACTION


class MockEggnogAnnotationsRepo(_BaseMockAnnotationsRepo):
    def create(self, annotation_input: EggnogAnnotationCreate):
        return CREATE_ACTION


class MockKeggAnnotationsRepo(_BaseMockAnnotationsRepo):
    def create(self, annotation_input: KeggOrthologyAnnotationCreate):
        return CREATE_ACTION


class MockNcbiTaxonomyAnnotationsRepo(_BaseMockAnnotationsRepo):
    def create(self, annotation_input: NcbiTaxonomyAnnotationCreate):
        return CREATE_ACTION


class TestCrudEggnogAnnotationUseCase(unittest.TestCase):
    def setUp(self) -> None:
        self.use_case_test = CrudEggnogAnnotationUseCase(
            eggnog_annotations_repo=MockEggnogAnnotationsRepo()
        )

    def test_create_eggnog_annotation(self):
        # Given
        eggnog_annotation_test = EggnogAnnotationCreate(
            eggnog_id=1, experiment_id=1, genes=[1, 2, 3]
        )
        expected_output = CREATE_ACTION
        # When
        test_output = self.use_case_test.create_annotation(eggnog_annotation_test)
        # Then
        self.assertEqual(test_output, expected_output)

    def test_delete_eggnog_annotation(self):
        # Given
        eggnog_annotation_id_test = 29
        expected_output = DELETE_ACTION
        # When
        test_output = self.use_case_test.delete_annotation(eggnog_annotation_id_test)
        # Then
        self.assertEqual(test_output, expected_output)

    def test_get_eggnog_annotation(self):
        # Given
        eggnog_annotation_id_test = 29
        expected_output = GET_ACTION
        # When
        test_output = self.use_case_test.get_annotation(eggnog_annotation_id_test)
        # Then
        self.assertEqual(test_output, expected_output)

    def test_get_all_annotations(self):
        # Given
        expected_output = GET_ALL_ACTION
        # When
        output = self.use_case_test.get_all()
        # Then
        self.assertEqual(output, expected_output)


class TestCrudKeggAnnotationUseCase(unittest.TestCase):
    def setUp(self) -> None:
        self.use_case_test = CrudKeggOrthologyAnnotationUseCase(
            kegg_annotations_repo=MockKeggAnnotationsRepo()
        )

    def test_create_kegg_annotation(self):
        # Given
        kegg_annotation_test = KeggOrthologyAnnotationCreate(
            kegg_id=1, experiment_id=1, genes=[1, 2, 3]
        )
        expected_output = CREATE_ACTION
        # When
        test_output = self.use_case_test.create_annotation(kegg_annotation_test)
        # Then
        self.assertEqual(test_output, expected_output)

    def test_delete_kegg_annotation(self):
        # Given
        kegg_annotation_id_test = 23
        expected_output = DELETE_ACTION
        # When
        test_output = self.use_case_test.delete_annotation(kegg_annotation_id_test)
        # Then
        self.assertEqual(test_output, expected_output)

    def test_get_kegg_annotation(self):
        # Given
        kegg_annotation_id_test = 23
        expected_output = GET_ACTION
        # When
        test_output = self.use_case_test.get_annotation(kegg_annotation_id_test)
        # Then
        self.assertEqual(test_output, expected_output)

    def test_get_all_annotations(self):
        # Given
        expected_output = GET_ALL_ACTION
        # When
        output = self.use_case_test.get_all()
        # Then
        self.assertEqual(output, expected_output)


class TestCrudNcbiTaxonomyAnnotationUseCase(unittest.TestCase):
    def setUp(self) -> None:
        self.use_case_test = CrudNcbiTaxonomyAnnotationUseCase(
            ncbi_tax_annotations_repo=MockNcbiTaxonomyAnnotationsRepo()
        )

    def test_create_ncbi_taxonomy_annotation(self):
        # Given
        ncbi_tax_annotation_test = NcbiTaxonomyAnnotationCreate(
            ncbi_taxonomy_id=1, experiment_id=1, genes=[1, 2, 3]
        )
        expected_output = CREATE_ACTION
        # When
        test_output = self.use_case_test.create_annotation(ncbi_tax_annotation_test)
        # Then
        self.assertEqual(test_output, expected_output)

    def test_delete_ncbi_taxonomy_annotation(self):
        # Given
        eggnog_annotation_id_test = "test-eggnog"
        expected_output = DELETE_ACTION
        # When
        test_output = self.use_case_test.delete_annotation(eggnog_annotation_id_test)
        # Then
        self.assertEqual(test_output, expected_output)

    def test_get_ncbi_taxonomy_annotation(self):
        # Given
        eggnog_annotation_id_test = "test-eggnog"
        expected_output = GET_ACTION
        # When
        test_output = self.use_case_test.get_annotation(eggnog_annotation_id_test)
        # Then
        self.assertEqual(test_output, expected_output)

    def test_get_all_annotations(self):
        # Given
        expected_output = GET_ALL_ACTION
        # When
        output = self.use_case_test.get_all()
        # Then
        self.assertEqual(output, expected_output)
