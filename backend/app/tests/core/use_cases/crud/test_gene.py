import unittest

from app.core.use_cases.crud.gene import CrudGeneUseCase
from app.core.schemas.entities.gene import GeneCreate, GeneUpdate

GET_ACTION = "Returning gene"
GET_ALL_ACTION = "Returning all genes"
CREATE_ACTION = "Creating gene"
UPDATE_ACTION = "Updating gene"
DELETE_ACTION = "Deleting gene"


class MockGenesRepo:
    def get(self, gene_id: str):
        return GET_ACTION

    def get_all(self):
        return GET_ALL_ACTION

    def create(self, gene_input: GeneCreate):
        return CREATE_ACTION

    def update(self, gene_input: GeneUpdate):
        return UPDATE_ACTION

    def delete(self, gene_id: str):
        return DELETE_ACTION


class TestCrudGeneUseCase(unittest.TestCase):
    def setUp(self) -> None:
        self.use_case_test = CrudGeneUseCase(genes_repo=MockGenesRepo())

    def test_create_gene(self):
        # Given
        gene_test = GeneCreate(gene_id="gene1", sequence="actg", length=4)
        expected_output = CREATE_ACTION
        # When
        test_output = self.use_case_test.create_gene(gene_test)
        # Then
        self.assertEqual(test_output, expected_output)

    def test_update_gene(self):
        # Given
        gene_test = GeneUpdate(gene_id="gene1", sequence="actg", length=4)
        expected_output = UPDATE_ACTION
        # When
        test_output = self.use_case_test.update_gene(gene_test)
        # Then
        self.assertEqual(test_output, expected_output)

    def test_delete_gene(self):
        # Given
        gene_id_test = "test-gene"
        expected_output = DELETE_ACTION
        # When
        test_output = self.use_case_test.delete_gene(gene_id_test)
        # Then
        self.assertEqual(test_output, expected_output)

    def test_get_gene(self):
        # Given
        gene_id_test = "test-gene"
        expected_output = GET_ACTION
        # When
        test_output = self.use_case_test.get_gene(gene_id_test)
        # Then
        self.assertEqual(test_output, expected_output)

    def test_get_all(self):
        # Given
        expected_output = GET_ALL_ACTION
        # When
        output = self.use_case_test.get_all()
        # Then
        self.assertEqual(output, expected_output)
