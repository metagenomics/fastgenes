import unittest

from app.core.use_cases.crud.kegg import CrudKeggOrthologyUseCase
from app.core.schemas.entities.kegg import (
    KeggOrthologyCreate,
    KeggOrthologyUpdate,
)

GET_ACTION = "Returning kegg"
GET_ALL_ACTION = "Returning all keggs"
CREATE_ACTION = "Creating kegg"
UPDATE_ACTION = "Updating kegg"
UPSERT_ACTION = "Upsert kegg"
DELETE_ACTION = "Deleting kegg"


class MockKeggOrthologysRepo:
    def get(self, kegg_id: str):
        return GET_ACTION

    def get_all(self):
        return GET_ALL_ACTION

    def create(self, kegg_input: KeggOrthologyCreate):
        return CREATE_ACTION

    def update(self, kegg_input: KeggOrthologyUpdate):
        return UPDATE_ACTION

    def delete(self, kegg_id: str):
        return DELETE_ACTION


class TestCrudKeggOrthologyUseCase(unittest.TestCase):
    def setUp(self) -> None:
        self.use_case_test = CrudKeggOrthologyUseCase(
            keggs_repo=MockKeggOrthologysRepo()
        )

    def test_create_kegg(self):
        # Given
        kegg_test = KeggOrthologyCreate(kegg_id="k01", name="test-kegg")
        expected_output = CREATE_ACTION
        # When
        test_output = self.use_case_test.create_kegg(kegg_test)
        # Then
        self.assertEqual(test_output, expected_output)

    def test_update_kegg(self):
        # Given
        kegg_test = KeggOrthologyUpdate(kegg_id="k01", name="test-kegg")
        expected_output = UPDATE_ACTION
        # When
        test_output = self.use_case_test.update_kegg(kegg_test)
        # Then
        self.assertEqual(test_output, expected_output)

    def test_delete_kegg(self):
        # Given
        kegg_id_test = "test-kegg"
        expected_output = DELETE_ACTION
        # When
        test_output = self.use_case_test.delete_kegg(kegg_id_test)
        # Then
        self.assertEqual(test_output, expected_output)

    def test_get_kegg(self):
        # Given
        kegg_id_test = "test-kegg"
        expected_output = GET_ACTION
        # When
        test_output = self.use_case_test.get_kegg(kegg_id_test)
        # Then
        self.assertEqual(test_output, expected_output)

    def test_get_all(self):
        # Given
        expected_output = GET_ALL_ACTION
        # When
        output = self.use_case_test.get_all()
        # Then
        self.assertEqual(output, expected_output)
