import unittest

from app.core.use_cases.crud.eggnog import CrudEggnogUseCase
from app.core.schemas.entities.eggnog import EggnogCreate, EggnogUpdate

GET_ACTION = "Returning eggnog"
GET_ALL_ACTION = "Returning all eggnogs"
CREATE_ACTION = "Creating eggnog"
UPDATE_ACTION = "Updating eggnog"
DELETE_ACTION = "Deleting eggnog"


class MockEggnogsRepo:
    def get(self, eggnog_id: str):
        return GET_ACTION

    def get_all(self):
        return GET_ALL_ACTION

    def create(self, eggnog_input: EggnogCreate):
        return CREATE_ACTION

    def update(self, eggnog_input: EggnogUpdate):
        return UPDATE_ACTION

    def delete(self, eggnog_id: str):
        return DELETE_ACTION


class TestCrudEggnogUseCase(unittest.TestCase):
    def setUp(self) -> None:
        self.use_case_test = CrudEggnogUseCase(eggnogs_repo=MockEggnogsRepo())

    def test_create_eggnog(self):
        # Given
        eggnog_test = EggnogCreate(eggnog_id="COG01", name="test-eggnog", version="5.0")
        expected_output = CREATE_ACTION
        # When
        test_output = self.use_case_test.create_eggnog(eggnog_test)
        # Then
        self.assertEqual(test_output, expected_output)

    def test_update_eggnog(self):
        # Given
        eggnog_test = EggnogUpdate(eggnog_id="COG01", name="test-eggnog", version="5.0")
        expected_output = UPDATE_ACTION
        # When
        test_output = self.use_case_test.update_eggnog(eggnog_test)
        # Then
        self.assertEqual(test_output, expected_output)

    def test_delete_eggnog(self):
        # Given
        eggnog_id_test = "test-eggnog"
        expected_output = DELETE_ACTION
        # When
        test_output = self.use_case_test.delete_eggnog(eggnog_id_test)
        # Then
        self.assertEqual(test_output, expected_output)

    def test_get_eggnog(self):
        # Given
        eggnog_id_test = "test-eggnog"
        expected_output = GET_ACTION
        # When
        test_output = self.use_case_test.get_eggnog(eggnog_id_test)
        # Then
        self.assertEqual(test_output, expected_output)

    def test_get_all(self):
        # Given
        expected_output = GET_ALL_ACTION
        # When
        output = self.use_case_test.get_all()
        # Then
        self.assertEqual(output, expected_output)
