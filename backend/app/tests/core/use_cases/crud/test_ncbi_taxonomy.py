import unittest

from app.core.use_cases.crud.ncbi_taxonomy import CrudNcbiTaxonomyUseCase
from app.core.schemas.entities.ncbi_taxonomy import (
    NcbiTaxonomyCreate,
    NcbiTaxonomyUpdate,
)

GET_ACTION = "Returning ncbi_taxonomy"
GET_ALL_ACTION = "Returning all ncbi_taxonomy entries"
CREATE_ACTION = "Creating ncbi_taxonomy"
UPDATE_ACTION = "Updating ncbi_taxonomy"
DELETE_ACTION = "Deleting ncbi_taxonomy"


class MockNcbiTaxonomysRepo:
    def get(self, ncbi_taxonomy_name: str):
        return GET_ACTION

    def get_all(self):
        return GET_ALL_ACTION

    def create(self, ncbi_taxonomy_input: NcbiTaxonomyCreate):
        return CREATE_ACTION

    def update(self, ncbi_taxonomy_input: NcbiTaxonomyUpdate):
        return UPDATE_ACTION

    def delete(self, ncbi_taxonomy_name: str):
        return DELETE_ACTION


class TestCrudNcbiTaxonomyUseCase(unittest.TestCase):
    def setUp(self) -> None:
        self.use_case_test = CrudNcbiTaxonomyUseCase(
            ncbi_taxonomys_repo=MockNcbiTaxonomysRepo()
        )

    def test_create_ncbi_taxonomy(self):
        # Given
        ncbi_taxonomy_test = NcbiTaxonomyCreate(
            tax_id=1, rank="species", name="test-ncbi_taxonomy"
        )
        expected_output = CREATE_ACTION
        # When
        test_output = self.use_case_test.create_ncbi_taxonomy(ncbi_taxonomy_test)
        # Then
        self.assertEqual(test_output, expected_output)

    def test_update_ncbi_taxonomy(self):
        # Given
        ncbi_taxonomy_test = NcbiTaxonomyUpdate(
            tax_id=1, rank="species", name="test-ncbi_taxonomy"
        )
        expected_output = UPDATE_ACTION
        # When
        test_output = self.use_case_test.update_ncbi_taxonomy(ncbi_taxonomy_test)
        # Then
        self.assertEqual(test_output, expected_output)

    def test_delete_ncbi_taxonomy(self):
        # Given
        ncbi_taxonomy_test_name = "test-ncbi_taxonomy"
        expected_output = DELETE_ACTION
        # When
        test_output = self.use_case_test.delete_ncbi_taxonomy(ncbi_taxonomy_test_name)
        # Then
        self.assertEqual(test_output, expected_output)

    def test_get_ncbi_taxonomy(self):
        # Given
        ncbi_taxonomy_test_name = "test-ncbi_taxonomy"
        expected_output = GET_ACTION
        # When
        test_output = self.use_case_test.get_ncbi_taxonomy(ncbi_taxonomy_test_name)
        # Then
        self.assertEqual(test_output, expected_output)

    def test_get_all(self):
        # Given
        expected_output = GET_ALL_ACTION
        # When
        output = self.use_case_test.get_all()
        # Then
        self.assertEqual(output, expected_output)
