import unittest

from app.core.use_cases.crud.catalog import CrudCatalogUseCase
from app.core.schemas.entities.catalog import CatalogCreate, CatalogUpdate

GET_ACTION = "Returning catalog"
GET_ALL_ACTION = "Returning all catalogs"
CREATE_ACTION = "Creating catalog"
UPDATE_ACTION = "Updating catalog"
DELETE_ACTION = "Deleting catalog"


class MockCatalogsRepo:
    def get(self, catalog_name: str):
        return GET_ACTION

    def get_all(self):
        return GET_ALL_ACTION

    def create(self, catalog_input: CatalogCreate):
        return CREATE_ACTION

    def update(self, catalog_input: CatalogUpdate):
        return UPDATE_ACTION

    def delete(self, catalog_name: str):
        return DELETE_ACTION


class TestCrudCatalogUseCase(unittest.TestCase):
    def setUp(self) -> None:
        self.use_case_test = CrudCatalogUseCase(catalogs_repo=MockCatalogsRepo())

    def test_create_catalog(self):
        # Given
        catalog_test = CatalogCreate(name="test-catalog")
        expected_output = CREATE_ACTION
        # When
        test_output = self.use_case_test.create_catalog(catalog_test)
        # Then
        self.assertEqual(test_output, expected_output)

    def test_update_catalog(self):
        # Given
        catalog_test = CatalogUpdate(name="test-catalog")
        expected_output = UPDATE_ACTION
        # When
        test_output = self.use_case_test.update_catalog(catalog_test)
        # Then
        self.assertEqual(test_output, expected_output)

    def test_delete_catalog(self):
        # Given
        catalog_test_name = "test-catalog"
        expected_output = DELETE_ACTION
        # When
        test_output = self.use_case_test.delete_catalog(catalog_test_name)
        # Then
        self.assertEqual(test_output, expected_output)

    def test_get_catalog(self):
        # Given
        catalog_test_name = "test-catalog"
        expected_output = GET_ACTION
        # When
        test_output = self.use_case_test.get_catalog(catalog_test_name)
        # Then
        self.assertEqual(test_output, expected_output)

    def test_get_all(self):
        # Given
        expected_output = GET_ALL_ACTION
        # When
        output = self.use_case_test.get_all()
        # Then
        self.assertEqual(output, expected_output)
