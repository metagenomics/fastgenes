import logging
import unittest

from sqlmodel import create_engine, Session, SQLModel
from sqlmodel.pool import StaticPool


logging.basicConfig()
logger = logging.getLogger()


class BaseRepositoriesTests(unittest.TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        cls.engine = create_engine(
            "sqlite://", connect_args={"check_same_thread": False}, poolclass=StaticPool
        )
        SQLModel.metadata.create_all(cls.engine)
        with Session(cls.engine) as session:
            cls._create_test_db(session)

    @classmethod
    def _create_test_db(cls, session: Session):
        pass

    def setUp(self) -> None:
        self.session = Session(self.engine)

    def tearDown(self) -> None:
        self.session.close()

    def _delete_entry(self, entry):
        self.session.rollback()
        logger.info(f"Deleting {entry}...")
        self.session.delete(entry)
        self.session.commit()
