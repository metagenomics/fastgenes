from sqlalchemy.exc import NoResultFound, IntegrityError
from sqlmodel import Session, select

from app.core.models.ncbi_taxonomy import NcbiTaxonomy
from app.core.repositories.ncbi_taxonomy import SqlModelNcbiTaxonomysRepo
from app.core.schemas.entities.ncbi_taxonomy import (
    NcbiTaxonomyCreate,
    NcbiTaxonomyUpdate,
)

from .base_repositories_test import BaseRepositoriesTests


class TestSqlModelNcbiTaxonomysRepo(BaseRepositoriesTests):
    @classmethod
    def _create_test_db(cls, session: Session):
        test_ncbi_taxonomy = NcbiTaxonomy(
            tax_id=123, name="test-ncbi_taxonomy", rank="test"
        )
        session.add(test_ncbi_taxonomy)
        session.commit()
        session.refresh(test_ncbi_taxonomy)
        cls.created_id = test_ncbi_taxonomy.id

    def setUp(self) -> None:
        super().setUp()
        self.repo = SqlModelNcbiTaxonomysRepo()

    def test_get_non_existing(self):
        with self.assertRaises(NoResultFound):
            self.repo.get("non-existing", self.session)

    def test_get_existing(self):
        # Given
        expected_id = 123
        expected_data = {
            "tax_id": expected_id,
            "name": "test-ncbi_taxonomy",
            "rank": "test",
            "parent_id": None,
            "hierarchy": None,
            "id": self.created_id,
        }
        # When
        ncbi_taxonomy_output = self.repo.get(expected_id, self.session)
        # Then
        self.assertDictEqual(ncbi_taxonomy_output.dict(), expected_data)

    def test_get_entries_existing(self):
        # Given
        expected_id = 123
        expected_data = [
            {
                "tax_id": expected_id,
                "name": "test-ncbi_taxonomy",
                "rank": "test",
                "parent_id": None,
                "hierarchy": None,
                "id": self.created_id,
            }
        ]
        # When
        ncbi_taxonomy_output_list = self.repo.get_entries([expected_id], self.session)
        ncbi_taxonomy_output_dict_list = [i.dict() for i in ncbi_taxonomy_output_list]
        # Then
        self.assertListEqual(ncbi_taxonomy_output_dict_list, expected_data)

    def test_get_entries_unexisting(self):
        # Given
        expected_id = 987654321
        expected_data = []
        # When
        ncbi_taxonomy_output_list = self.repo.get_entries([expected_id], self.session)
        ncbi_taxonomy_output_dict_list = [i.dict() for i in ncbi_taxonomy_output_list]
        # Then
        self.assertListEqual(ncbi_taxonomy_output_dict_list, expected_data)

    def test_get_all(self):
        # Given
        expected_data = [
            {
                "tax_id": 123,
                "name": "test-ncbi_taxonomy",
                "rank": "test",
                "parent_id": None,
                "hierarchy": None,
                "id": self.created_id,
            }
        ]
        # When
        ncbi_taxonomy_output_list = self.repo.get_all(self.session)
        ncbi_taxonomy_output_dict_list = [i.dict() for i in ncbi_taxonomy_output_list]
        # Then
        self.assertListEqual(ncbi_taxonomy_output_dict_list, expected_data)

    def test_get_all_return_dict(self):
        # Given
        expected_data = {
            123: {
                "tax_id": 123,
                "name": "test-ncbi_taxonomy",
                "rank": "test",
                "parent_id": None,
                "hierarchy": None,
                "id": self.created_id,
            }
        }
        # When
        ncbi_taxonomy_output_dict = self.repo.get_all(self.session, return_dict=True)
        ncbi_taxonomy_output_dict_dict = {
            k: v.dict() for k, v in ncbi_taxonomy_output_dict.items()
        }
        # Then
        self.assertDictEqual(ncbi_taxonomy_output_dict_dict, expected_data)

    def test_get_all_ids(self):
        # Given
        expected_data = {123}
        # When
        ncbi_taxonomy_output_tuple = self.repo.get_all_ids(self.session)
        # Then
        self.assertSetEqual(ncbi_taxonomy_output_tuple, expected_data)

    def test_get_tax_id_to_id_mapping(self):
        # Given
        expected_data = {123: self.created_id}
        # When
        ncbi_taxonomy_output_tuple = self.repo.get_tax_id_to_id_mapping(self.session)
        # Then
        self.assertDictEqual(ncbi_taxonomy_output_tuple, expected_data)

    def test_create(self):
        # Given
        tax_id = 321
        ncbi_taxonomy_name = "created_ncbi_taxonomy"
        ncbi_taxonomy_input = NcbiTaxonomyCreate(
            tax_id=tax_id, name=ncbi_taxonomy_name, rank="test"
        )
        expected_data = {
            "tax_id": tax_id,
            "name": ncbi_taxonomy_name,
            "rank": "test",
            "parent_id": None,
            "hierarchy": None,
        }
        try:
            # When
            ncbi_taxonomy_output = self.repo.create(ncbi_taxonomy_input, self.session)
            # Then
            for k, v in ncbi_taxonomy_output.dict().items():
                if k == "id":
                    continue
                self.assertEqual(v, expected_data[k])
        finally:
            ncbi_taxonomy = self.session.exec(
                select(NcbiTaxonomy).where(NcbiTaxonomy.tax_id == tax_id)
            ).one()
            self._delete_entry(ncbi_taxonomy)

    def test_create_existing(self):
        # Given
        tax_id = 123
        ncbi_taxonomy_name = "test-ncbi_taxonomy"
        ncbi_taxonomy_input = NcbiTaxonomyCreate(
            tax_id=tax_id, name=ncbi_taxonomy_name, rank="test"
        )
        # Then
        with self.assertRaises(IntegrityError):
            self.repo.create(ncbi_taxonomy_input, self.session)

    def test_create_batch(self):
        # Given
        ncbi_taxonomy_id = 987
        ncbi_taxonomy_name = "created_ncbi_taxonomy"
        ncbi_taxonomy_input = NcbiTaxonomyCreate(
            tax_id=ncbi_taxonomy_id, name=ncbi_taxonomy_name, rank="test"
        )
        expected_output = 1
        try:
            # When
            output = self.repo.create_batch([ncbi_taxonomy_input], self.session)
            # Then
            self.assertEqual(output, expected_output)
        finally:
            ncbi_taxonomy = self.session.exec(
                select(NcbiTaxonomy).where(NcbiTaxonomy.tax_id == ncbi_taxonomy_id)
            ).one()
            self._delete_entry(ncbi_taxonomy)

    def test_create_batch_existing(self):
        # Given
        ncbi_taxonomy_id = 123
        ncbi_taxonomy_name = "test-ncbi_taxonomy"
        ncbi_taxonomy_input = NcbiTaxonomyCreate(
            tax_id=ncbi_taxonomy_id, name=ncbi_taxonomy_name, rank="test"
        )
        with self.assertRaises(IntegrityError):
            self.repo.create_batch([ncbi_taxonomy_input], self.session)

    def test_update_non_existing(self):
        # Given
        tax_id = 456
        ncbi_taxonomy_name = "update-ncbi_taxonomy"
        ncbi_taxonomy_input = NcbiTaxonomyUpdate(
            tax_id=tax_id, name=ncbi_taxonomy_name, ec_numbers="test"
        )
        with self.assertRaises(NoResultFound):
            self.repo.update(ncbi_taxonomy_input, self.session)

    def test_update(self):
        # Given
        tax_id = 789
        name_to_update = "update-me"
        new_name = "new-name"
        # - create ncbi_taxonomy to update
        ncbi_taxonomy_to_update = NcbiTaxonomy(
            tax_id=tax_id, name=name_to_update, rank="test"
        )
        self.session.add(ncbi_taxonomy_to_update)
        self.session.commit()
        ncbi_taxonomy_input = NcbiTaxonomyUpdate(tax_id=tax_id, name=new_name)
        expected_data = {
            "tax_id": tax_id,
            "name": new_name,
            "rank": "test",
            "parent_id": None,
            "hierarchy": None,
        }
        try:
            # When
            ncbi_taxonomy_output = self.repo.update(ncbi_taxonomy_input, self.session)
            # Then
            for k, v in ncbi_taxonomy_output.dict().items():
                if k == "id":
                    continue
                self.assertEqual(v, expected_data[k])
        finally:
            self._delete_entry(ncbi_taxonomy_to_update)

    def test_update_ncbi_taxonomy_no_name_exclude_none_false(self):
        # Given
        tax_id = 987
        name_to_update = "update-me"
        # - create ncbi_taxonomy to update
        ncbi_taxonomy_to_update = NcbiTaxonomy(
            tax_id=tax_id, name=name_to_update, rank="test"
        )
        self.session.add(ncbi_taxonomy_to_update)
        self.session.commit()
        ncbi_taxonomy_input = NcbiTaxonomyUpdate(tax_id=tax_id, name=name_to_update)
        try:
            # When
            with self.assertRaises(IntegrityError):
                self.repo.update(ncbi_taxonomy_input, self.session, exclude_none=False)
        finally:
            self._delete_entry(ncbi_taxonomy_to_update)

    def test_update_batch_non_existing(self):
        # Given
        tax_id = 963
        ncbi_taxonomy_name = "update-ncbi_taxonomy"
        ncbi_taxonomy_input = NcbiTaxonomyUpdate(
            tax_id=tax_id, name=ncbi_taxonomy_name, rank="test"
        )
        expected_output = 0
        # When
        output = self.repo.update_batch([ncbi_taxonomy_input], self.session)
        # Then
        self.assertEqual(output, expected_output)

    def test_update_batch(self):
        # Given
        tax_id = 741
        name_to_update = "update-me"
        new_name = "new-name"
        # - create ncbi_taxonomy to update
        ncbi_taxonomy_to_update = NcbiTaxonomy(
            tax_id=tax_id, name=name_to_update, rank="test"
        )
        self.session.add(ncbi_taxonomy_to_update)
        self.session.commit()
        ncbi_taxonomy_input = NcbiTaxonomyUpdate(
            tax_id=tax_id, name=new_name, rank="test"
        )
        expected_output = 1
        try:
            # When
            output = self.repo.update_batch([ncbi_taxonomy_input], self.session)
            # Then
            self.assertEqual(output, expected_output)
        finally:
            self._delete_entry(ncbi_taxonomy_to_update)

    def test_delete(self):
        # Given
        tax_id = 963
        name_to_delete = "delete-me"
        # - create ncbi_taxonomy to delete
        ncbi_taxonomy_to_delete = NcbiTaxonomy(
            tax_id=tax_id, name=name_to_delete, rank="test"
        )
        self.session.add(ncbi_taxonomy_to_delete)
        self.session.commit()
        # When
        self.repo.delete(tax_id, self.session)
        # Then
        with self.assertRaises(NoResultFound):
            self.session.exec(
                select(NcbiTaxonomy).where(NcbiTaxonomy.tax_id == tax_id)
            ).one()
