from sqlalchemy.exc import NoResultFound, IntegrityError
from sqlmodel import Session, select

from app.core.models.kegg import KeggOrthology
from app.core.repositories.kegg import SqlModelKeggOrthologysRepo
from app.core.schemas.entities.kegg import (
    KeggOrthologyCreate,
    KeggOrthologyUpdate,
)

from .base_repositories_test import BaseRepositoriesTests


class TestSqlModelKeggOrthologysRepo(BaseRepositoriesTests):
    @classmethod
    def _create_test_db(cls, session: Session):
        test_kegg = KeggOrthology(kegg_id="k1", name="test-kegg")
        session.add(test_kegg)
        session.commit()
        session.refresh(test_kegg)
        cls.created_id = test_kegg.id

    def setUp(self) -> None:
        super().setUp()
        self.repo = SqlModelKeggOrthologysRepo()

    def test_get_non_existing(self):
        with self.assertRaises(NoResultFound):
            self.repo.get("non-existing", self.session)

    def test_get_existing(self):
        # Given
        expected_id = "k1"
        expected_data = {
            "kegg_id": expected_id,
            "name": "test-kegg",
            "definition": None,
            "ec_numbers": None,
            "id": self.created_id,
        }
        # When
        kegg_output = self.repo.get(expected_id, self.session)
        # Then
        self.assertDictEqual(kegg_output.dict(), expected_data)

    def test_get_entries_existing(self):
        # Given
        expected_id = "k1"
        expected_data = [
            {
                "kegg_id": expected_id,
                "name": "test-kegg",
                "id": self.created_id,
                "definition": None,
                "ec_numbers": None,
            }
        ]
        # When
        kegg_output_list = self.repo.get_entries([expected_id], self.session)
        kegg_output_dict_list = [i.dict() for i in kegg_output_list]
        # Then
        self.assertListEqual(kegg_output_dict_list, expected_data)

    def test_get_entries_unexisting(self):
        # Given
        expected_id = "k101"
        expected_data = []
        # When
        kegg_output_list = self.repo.get_entries([expected_id], self.session)
        kegg_output_dict_list = [i.dict() for i in kegg_output_list]
        # Then
        self.assertListEqual(kegg_output_dict_list, expected_data)

    def test_get_all(self):
        # Given
        expected_data = [
            {
                "kegg_id": "k1",
                "name": "test-kegg",
                "id": self.created_id,
                "definition": None,
                "ec_numbers": None,
            }
        ]
        # When
        kegg_output_list = self.repo.get_all(self.session)
        kegg_output_dict_list = [i.dict() for i in kegg_output_list]
        # Then
        self.assertListEqual(kegg_output_dict_list, expected_data)

    def test_get_all_return_dict(self):
        # Given
        expected_data = {
            "k1": {
                "kegg_id": "k1",
                "name": "test-kegg",
                "id": self.created_id,
                "definition": None,
                "ec_numbers": None,
            }
        }
        # When
        kegg_output_dict = self.repo.get_all(self.session, return_dict=True)
        kegg_output_dict_dict = {k: v.dict() for k, v in kegg_output_dict.items()}
        # Then
        self.assertDictEqual(kegg_output_dict_dict, expected_data)

    def test_get_all_ids(self):
        # Given
        expected_data = {"k1"}
        # When
        kegg_output_tuple = self.repo.get_all_ids(self.session)
        # Then
        self.assertSetEqual(kegg_output_tuple, expected_data)

    def test_create(self):
        # Given
        kegg_id = "k9"
        kegg_name = "created_kegg"
        kegg_input = KeggOrthologyCreate(
            kegg_id=kegg_id, name=kegg_name, ec_numbers=["ec1"]
        )
        expected_data = {
            "kegg_id": kegg_id,
            "name": kegg_name,
            "definition": None,
            "ec_numbers": ["ec1"],
        }
        try:
            # When
            kegg_output = self.repo.create(kegg_input, self.session)
            # Then
            for k, v in kegg_output.dict().items():
                if k == "id":
                    continue
                self.assertEqual(v, expected_data[k])
        finally:
            kegg = self.session.exec(
                select(KeggOrthology).where(KeggOrthology.kegg_id == kegg_id)
            ).one()
            self._delete_entry(kegg)

    def test_create_existing(self):
        # Given
        kegg_id = "k1"
        kegg_name = "test-kegg"
        kegg_input = KeggOrthologyCreate(
            kegg_id=kegg_id, name=kegg_name, ec_numbers=["ec1"]
        )
        # Then
        with self.assertRaises(IntegrityError):
            self.repo.create(kegg_input, self.session)

    def test_create_batch(self):
        # Given
        kegg_id = "k19"
        kegg_name = "created_kegg"
        kegg_input = KeggOrthologyCreate(
            kegg_id=kegg_id, name=kegg_name, ec_numbers=["ec1"]
        )
        expected_output = 1
        try:
            # When
            output = self.repo.create_batch([kegg_input], self.session)
            # Then
            self.assertEqual(output, expected_output)
        finally:
            kegg = self.session.exec(
                select(KeggOrthology).where(KeggOrthology.kegg_id == kegg_id)
            ).one()
            self._delete_entry(kegg)

    def test_create_batch_existing(self):
        # Given
        kegg_id = "k1"
        kegg_name = "test-kegg"
        kegg_input = KeggOrthologyCreate(
            kegg_id=kegg_id, name=kegg_name, ec_numbers=["ec1"]
        )
        with self.assertRaises(IntegrityError):
            self.repo.create_batch([kegg_input], self.session)

    def test_update_non_existing(self):
        # Given
        kegg_id = "k8"
        kegg_name = "update-kegg"
        kegg_input = KeggOrthologyUpdate(
            kegg_id=kegg_id, name=kegg_name, ec_numbers=["ec1"]
        )
        with self.assertRaises(NoResultFound):
            self.repo.update(kegg_input, self.session)

    def test_update(self):
        # Given
        kegg_id = "k2"
        name_to_update = "update-me"
        new_name = "new-name"
        # - create kegg to update
        kegg_to_update = KeggOrthology(kegg_id=kegg_id, name=name_to_update)
        self.session.add(kegg_to_update)
        self.session.commit()
        kegg_input = KeggOrthologyUpdate(
            kegg_id=kegg_id, name=new_name, ec_numbers=["ec1"]
        )
        expected_data = {
            "kegg_id": kegg_id,
            "name": new_name,
            "ec_numbers": ["ec1"],
            "definition": None,
        }
        try:
            # When
            kegg_output = self.repo.update(kegg_input, self.session)
            # Then
            for k, v in kegg_output.dict().items():
                if k == "id":
                    continue
                self.assertEqual(v, expected_data[k])
        finally:
            self._delete_entry(kegg_to_update)

    def test_update_kegg_no_name_exclude_none_false(self):
        # Given
        kegg_id = "k2"
        name_to_update = "update-me"
        # - create kegg to update
        kegg_to_update = KeggOrthology(kegg_id=kegg_id, name=name_to_update)
        self.session.add(kegg_to_update)
        self.session.commit()
        kegg_input = KeggOrthologyUpdate(kegg_id=kegg_id)
        try:
            # When
            with self.assertRaises(IntegrityError):
                self.repo.update(kegg_input, self.session, exclude_none=False)
        finally:
            self._delete_entry(kegg_to_update)

    def test_update_batch_non_existing(self):
        # Given
        kegg_id = "k8"
        kegg_name = "update-kegg"
        kegg_input = KeggOrthologyUpdate(
            kegg_id=kegg_id, name=kegg_name, ec_numbers=["ec1"]
        )
        expected_output = 0
        # When
        output = self.repo.update_batch([kegg_input], self.session)
        # Then
        self.assertEqual(output, expected_output)

    def test_update_batch(self):
        # Given
        kegg_id = "k2"
        name_to_update = "update-me"
        new_name = "new-name"
        # - create kegg to update
        kegg_to_update = KeggOrthology(kegg_id=kegg_id, name=name_to_update)
        self.session.add(kegg_to_update)
        self.session.commit()
        kegg_input = KeggOrthologyUpdate(
            kegg_id=kegg_id, name=new_name, ec_numbers=["ec1"]
        )
        expected_output = 1
        try:
            # When
            output = self.repo.update_batch([kegg_input], self.session)
            # Then
            self.assertEqual(output, expected_output)
        finally:
            self._delete_entry(kegg_to_update)

    def test_delete_non_existing(self):
        with self.assertRaises(NoResultFound):
            self.repo.delete("non-existing", self.session)

    def test_delete(self):
        # Given
        kegg_id = "k5"
        name_to_delete = "delete-me"
        # - create kegg to delete
        kegg_to_delete = KeggOrthology(kegg_id=kegg_id, name=name_to_delete)
        self.session.add(kegg_to_delete)
        self.session.commit()
        # When
        self.repo.delete(kegg_id, self.session)
        # Then
        with self.assertRaises(NoResultFound):
            self.session.exec(
                select(KeggOrthology).where(KeggOrthology.kegg_id == kegg_id)
            ).one()
