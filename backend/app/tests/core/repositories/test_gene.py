from sqlalchemy.exc import NoResultFound, IntegrityError
from sqlmodel import Session, select

from app.core.models.gene import Gene
from app.core.repositories.gene import SqlModelGenesRepo
from app.core.schemas.entities.gene import (
    GeneCreate,
    GeneUpdate,
)

from .base_repositories_test import BaseRepositoriesTests


class TestSqlModelGenesRepo(BaseRepositoriesTests):
    @classmethod
    def _create_test_db(cls, session: Session):
        test_gene = Gene(gene_id="g1", sequence="atcg", length=4)
        session.add(test_gene)
        session.commit()
        session.refresh(test_gene)
        cls.created_id = test_gene.id

    def setUp(self) -> None:
        super().setUp()
        self.repo = SqlModelGenesRepo()

    def test_get_non_existing(self):
        with self.assertRaises(NoResultFound):
            self.repo.get("non-existing", self.session)

    def test_get_existing(self):
        # Given
        expected_id = "g1"
        expected_data = {
            "gene_id": expected_id,
            "sequence": "atcg",
            "length": 4,
            "id": self.created_id,
        }
        # When
        gene_output = self.repo.get(expected_id, self.session)
        # Then
        self.assertDictEqual(gene_output.dict(), expected_data)

    def test_get_entries_existing(self):
        # Given
        expected_id = "g1"
        expected_data = [
            {
                "gene_id": expected_id,
                "sequence": "atcg",
                "length": 4,
                "id": self.created_id,
            }
        ]
        # When
        gene_output_list = self.repo.get_entries([expected_id], self.session)
        gene_output_dict_list = [i.dict() for i in gene_output_list]
        # Then
        self.assertListEqual(gene_output_dict_list, expected_data)

    def test_get_entries_unexisting(self):
        # Given
        expected_id = "g1234"
        expected_data = []
        # When
        gene_output_list = self.repo.get_entries([expected_id], self.session)
        gene_output_dict_list = [i.dict() for i in gene_output_list]
        # Then
        self.assertListEqual(gene_output_dict_list, expected_data)

    def test_get_all(self):
        # Given
        expected_data = [
            {"gene_id": "g1", "sequence": "atcg", "length": 4, "id": self.created_id}
        ]
        # When
        gene_output_list = self.repo.get_all(self.session)
        gene_output_dict_list = [i.dict() for i in gene_output_list]
        # Then
        self.assertListEqual(gene_output_dict_list, expected_data)

    def test_get_all_return_dict(self):
        # Given
        expected_id = "g1"
        expected_data = {
            expected_id: {
                "gene_id": expected_id,
                "sequence": "atcg",
                "length": 4,
                "id": self.created_id,
            }
        }
        # When
        gene_output_dict = self.repo.get_all(self.session, return_dict=True)
        gene_output_dict_dict = {k: v.dict() for k, v in gene_output_dict.items()}
        # Then
        self.assertDictEqual(gene_output_dict_dict, expected_data)

    def test_get_all_ids(self):
        # Given
        expected_data = {"g1"}
        # When
        gene_output_tuple = self.repo.get_all_ids(self.session)
        # Then
        self.assertSetEqual(gene_output_tuple, expected_data)

    def test_create(self):
        # Given
        gene_id = "g2"
        sequence = "tgca"
        gene_input = GeneCreate(gene_id=gene_id, sequence=sequence)
        expected_data = {
            "gene_id": gene_id,
            "sequence": sequence,
            "length": 4,
        }
        try:
            # When
            gene_output = self.repo.create(gene_input, self.session)
            # Then
            for k, v in gene_output.dict().items():
                if k == "id":
                    continue
                self.assertEqual(v, expected_data[k])
        finally:
            gene = self.session.exec(select(Gene).where(Gene.gene_id == gene_id)).one()
            self._delete_entry(gene)

    def test_create_existing(self):
        # Given
        gene_id = "g1"
        sequence = "tgac"
        gene_input = GeneCreate(gene_id=gene_id, sequence=sequence)
        # Then
        with self.assertRaises(IntegrityError):
            self.repo.create(gene_input, self.session)

    def test_create_batch(self):
        # Given
        gene_id = "g3"
        sequence = "aaaa"
        gene_input = GeneCreate(gene_id=gene_id, sequence=sequence)
        expected_output = 1
        try:
            # When
            output = self.repo.create_batch([gene_input], self.session)
            # Then
            self.assertEqual(output, expected_output)
        finally:
            gene = self.session.exec(select(Gene).where(Gene.gene_id == gene_id)).one()
            self._delete_entry(gene)

    def test_create_batch_existing(self):
        # Given
        gene_id = "g1"
        sequence = "tttt"
        gene_input = GeneCreate(gene_id=gene_id, sequence=sequence)
        with self.assertRaises(IntegrityError):
            self.repo.create_batch([gene_input], self.session)

    def test_update_non_existing(self):
        # Given
        gene_id = "g6"
        sequence = "atatatat"
        gene_input = GeneUpdate(gene_id=gene_id, sequence=sequence)
        with self.assertRaises(NoResultFound):
            self.repo.update(gene_input, self.session)

    def test_update(self):
        # Given
        gene_id = "g7"
        sequence = "atg"
        new_sequence = "ctga"
        # - create gene to update
        gene_to_update = Gene(gene_id=gene_id, sequence=sequence, length=3)
        self.session.add(gene_to_update)
        self.session.commit()
        gene_input = GeneUpdate(gene_id=gene_id, sequence=new_sequence)
        expected_data = {
            "gene_id": gene_id,
            "sequence": new_sequence,
            "length": 4,
        }
        try:
            # When
            gene_output = self.repo.update(gene_input, self.session)
            # Then
            for k, v in gene_output.dict().items():
                if k == "id":
                    continue
                self.assertEqual(v, expected_data[k])
        finally:
            self._delete_entry(gene_to_update)

    def test_update_gene_no_sequence_exclude_none_false(self):
        # Given
        gene_id = "g0"
        sequence = "aaa"
        # - create gene to update
        gene_to_update = Gene(gene_id=gene_id, sequence=sequence, length=3)
        self.session.add(gene_to_update)
        self.session.commit()
        gene_input = GeneUpdate(gene_id=gene_id)
        try:
            # When
            with self.assertRaises(IntegrityError):
                self.repo.update(gene_input, self.session, exclude_none=False)
        finally:
            self._delete_entry(gene_to_update)

    def test_update_batch_non_existing(self):
        # Given
        gene_id = "g14"
        sequence = "atat"
        gene_input = GeneUpdate(gene_id=gene_id, sequence=sequence)
        expected_output = 0
        # When
        output = self.repo.update_batch([gene_input], self.session)
        # Then
        self.assertEqual(output, expected_output)

    def test_update_batch(self):
        # Given
        gene_id = "g16"
        sequence = "ggcc"
        new_sequence = "cg"
        # - create gene to update
        gene_to_update = Gene(gene_id=gene_id, sequence=sequence, length=4)
        self.session.add(gene_to_update)
        self.session.commit()
        gene_input = GeneUpdate(gene_id=gene_id, new_sequence=new_sequence)
        expected_output = 1
        try:
            # When
            output = self.repo.update_batch([gene_input], self.session)
            # Then
            self.assertEqual(output, expected_output)
        finally:
            self._delete_entry(gene_to_update)

    def test_delete(self):
        # Given
        gene_id = "g9"
        sequence = "cg"
        # - create gene to delete
        gene_to_delete = Gene(gene_id=gene_id, sequence=sequence, length=2)
        self.session.add(gene_to_delete)
        self.session.commit()
        # When
        self.repo.delete(gene_id, self.session)
        # Then
        with self.assertRaises(NoResultFound):
            self.session.exec(select(Gene).where(Gene.gene_id == gene_id)).one()
