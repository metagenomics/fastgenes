import unittest

from app.core.utils.parsers.ncbi_taxonomy import NCBITaxonomyLineParser


class TestNCBITaxonomyLineParser(unittest.TestCase):
    def test_node(self):
        # Given
        node_line = "6	|	335928	|	genus	|		|	0	|	1	|	11	|	1	|	0	|	1	|	0	|	0	|		|\n"
        expected_dict = {"tax_id": 6, "parent_tax_id": 335928, "rank": "genus"}
        # When
        test_node = NCBITaxonomyLineParser.node(node_line)
        # Then
        self.assertDictEqual(test_node.dict(), expected_dict)

    def test_node_wrong_format(self):
        # Given
        node_line = "This is a wrong line format, with; information   and tab"
        # Then
        with self.assertRaises(Exception) as context:  # noqa
            NCBITaxonomyLineParser.node(node_line)

    def test_name(self):
        # Given
        name_line = "2	|	Bacteria	|	Bacteria <prokaryotes>	|	scientific name	|\n"
        expected_dict = {
            "tax_id": 2,
            "name_txt": "Bacteria",
            "unique_name": "Bacteria <prokaryotes>",
            "name_class": "scientific name",
        }
        # When
        test_name = NCBITaxonomyLineParser.name(name_line)
        # Then
        self.assertDictEqual(test_name.dict(), expected_dict)

    def test_name_wrong_format(self):
        # Given
        name_line = "This is a wrong line format, with; information   and tab"
        # Then
        with self.assertRaises(Exception) as context:  # noqa
            NCBITaxonomyLineParser.name(name_line)
