import unittest
from unittest.mock import patch, Mock

from app.celery import utils


class TestIsRunningTask(unittest.TestCase):
    @patch("app.celery.utils.current_app.control.inspect")
    def test_task_is_running(self, mock_inspect):
        # Given
        task_name = "running-task"
        dict_values = {"key": [{"name": task_name}]}
        inspect_mock = Mock()
        inspect_mock.active.return_value = dict_values
        mock_inspect.return_value = inspect_mock
        # When
        tested_result = utils.is_running_task(task_name)
        # Then
        self.assertTrue(tested_result)

    @patch("app.celery.utils.current_app.control.inspect")
    def test_task_is_not_running(self, mock_inspect):
        # Given
        task_name = "not-running-task"
        dict_values = {"key": [{"name": "running-task"}]}
        inspect_mock = Mock()
        inspect_mock.active.return_value = dict_values
        mock_inspect.return_value = inspect_mock
        # When
        tested_result = utils.is_running_task(task_name)
        # Then
        self.assertFalse(tested_result)


class TestGetTaskId(unittest.TestCase):
    @patch("app.celery.utils.current_app.control.inspect")
    def test_task_is_running(self, mock_inspect):
        # Given
        task_name = "running-task"
        task_id = "29"
        dict_values = {"key": [{"name": task_name, "id": task_id}]}
        inspect_mock = Mock()
        inspect_mock.active.return_value = dict_values
        mock_inspect.return_value = inspect_mock
        # When
        tested_result = utils.get_task_id(task_name)
        # Then
        self.assertEqual(tested_result, task_id)

    @patch("app.celery.utils.current_app.control.inspect")
    def test_task_is_not_running(self, mock_inspect):
        # Given
        task_name = "not-running-task"
        dict_values = {"key": [{"name": "running-task", "id": "35"}]}
        inspect_mock = Mock()
        inspect_mock.active.return_value = dict_values
        mock_inspect.return_value = inspect_mock
        # When
        tested_result = utils.get_task_id(task_name)
        # Then
        self.assertEqual(tested_result, "")
