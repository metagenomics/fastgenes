from sqlmodel import Session, select

from app.core.models.kegg import KeggOrthology

from .base_api_test import BaseApiTests


class TestKeggOrthologys(BaseApiTests):
    @classmethod
    def _create_test_db(cls, session: Session):
        test_kegg = KeggOrthology(kegg_id="k1", name="test-kegg")
        session.add(test_kegg)
        session.commit()
        session.refresh(test_kegg)
        cls.created_id = test_kegg.id

    def test_get_kegg_non_existing(self):
        # When
        response = self.client.get("/api/kegg-orthology/non-existing")
        # Then
        self.assertEqual(response.status_code, 404)

    def test_get_kegg_existing(self):
        # Given
        expected_id = "k1"
        expected_data = {
            "kegg_id": expected_id,
            "name": "test-kegg",
            "definition": None,
            "ec_numbers": None,
            "id": self.created_id,
        }
        # When
        response = self.client.get(f"/api/kegg-orthology/{expected_id}")
        # Then
        self.assertEqual(response.status_code, 200)
        self.assertDictEqual(response.json(), expected_data)

    def test_get_all_keggs(self):
        # Given
        expected_data = [
            {
                "kegg_id": "k1",
                "name": "test-kegg",
                "id": self.created_id,
                "definition": None,
                "ec_numbers": None,
            }
        ]
        # When
        response = self.client.get("/api/kegg-orthology/")
        # Then
        self.assertEqual(response.status_code, 200)
        self.assertListEqual(response.json()["items"], expected_data)

    def test_create_kegg(self):
        # Given
        kegg_id = "k9"
        kegg_name = "created_kegg"
        json_input = {"kegg_id": kegg_id, "name": kegg_name, "ec_numbers": ["ec1"]}
        expected_data = {
            "kegg_id": kegg_id,
            "name": kegg_name,
            "definition": None,
            "ec_numbers": ["ec1"],
        }
        try:
            # When
            response = self.client.post("/api/kegg-orthology/", json=json_input)
            # Then
            self.assertEqual(response.status_code, 200)
            for k, v in response.json().items():
                if k == "id":
                    continue
                self.assertEqual(v, expected_data[k])
        finally:
            kegg = self.session.exec(
                select(KeggOrthology).where(KeggOrthology.kegg_id == kegg_id)
            ).one()
            self._delete_entry(kegg)

    def test_create_existing_kegg(self):
        # Given
        kegg_id = "k1"
        kegg_name = "test-kegg"
        json_input = {"kegg_id": kegg_id, "name": kegg_name}
        # When
        response = self.client.post("/api/kegg-orthology/", json=json_input)
        # Then
        self.assertEqual(response.status_code, 403)

    def test_delete_kegg_non_existing(self):
        # When
        response = self.client.delete("/api/kegg-orthology/non-existing")
        # Then
        self.assertEqual(response.status_code, 404)

    def test_delete_kegg(self):
        # Given
        kegg_id = "k5"
        name_to_delete = "delete-me"
        # - create kegg to delete
        kegg_to_delete = KeggOrthology(kegg_id=kegg_id, name=name_to_delete)
        self.session.add(kegg_to_delete)
        self.session.commit()
        # When
        response = self.client.delete(f"/api/kegg-orthology/{kegg_id}")
        # Then
        self.assertEqual(response.status_code, 200)

    def test_update_kegg_non_existing(self):
        # When
        json_input = {"kegg_id": "i-dont-exist", "name": "12345"}
        response = self.client.put("/api/kegg-orthology/", json=json_input)
        # Then
        self.assertEqual(response.status_code, 404)

    def test_update_kegg(self):
        # Given
        kegg_id = "k2"
        name_to_update = "update-me"
        new_name = "new-name"
        # - create kegg to update
        kegg_to_update = KeggOrthology(kegg_id=kegg_id, name=name_to_update)
        self.session.add(kegg_to_update)
        self.session.commit()
        json_input = {"kegg_id": kegg_id, "name": new_name, "ec_numbers": ["ec1"]}
        expected_data = {
            "kegg_id": kegg_id,
            "name": new_name,
            "ec_numbers": ["ec1"],
            "definition": None,
        }
        try:
            # When
            response = self.client.put("/api/kegg-orthology/", json=json_input)
            # Then
            self.assertEqual(response.status_code, 200)
            for k, v in response.json().items():
                if k == "id":
                    continue
                self.assertEqual(v, expected_data[k])
        finally:
            self._delete_entry(kegg_to_update)

    def test_update_kegg_no_name_exclude_none_false(self):
        # Given
        kegg_id = "k2"
        name_to_update = "update-me"
        # - create kegg to update
        kegg_to_update = KeggOrthology(kegg_id=kegg_id, name=name_to_update)
        self.session.add(kegg_to_update)
        self.session.commit()
        json_input = {"kegg_id": kegg_id}
        try:
            # When
            response = self.client.put(
                "/api/kegg-orthology/?exclude_none=false", json=json_input
            )
            # Then
            self.assertEqual(response.status_code, 422)
        finally:
            self._delete_entry(kegg_to_update)
