from sqlmodel import Session, select

from app.core.models.catalog import Catalog

from .base_api_test import BaseApiTests


class TestCatalogs(BaseApiTests):
    @classmethod
    def _create_test_db(cls, session: Session):
        test_catalog = Catalog(name="test-catalog")
        session.add(test_catalog)
        session.commit()
        session.refresh(test_catalog)
        cls.created_id = test_catalog.id

    def test_get_catalog_non_existing(self):
        # When
        response = self.client.get("/api/catalogs/non-existing")
        # Then
        self.assertEqual(response.status_code, 404)

    def test_get_catalog_existing(self):
        # Given
        expected_name = "test-catalog"
        expected_data = {"name": expected_name, "doi": None, "id": self.created_id}
        # When
        response = self.client.get(f"/api/catalogs/{expected_name}")
        # Then
        self.assertEqual(response.status_code, 200)
        self.assertDictEqual(response.json(), expected_data)

    def test_get_all_catalogs(self):
        # Given
        expected_data = [{"name": "test-catalog", "doi": None, "id": self.created_id}]
        # When
        response = self.client.get("/api/catalogs/")
        # Then
        self.assertEqual(response.status_code, 200)
        self.assertListEqual(response.json(), expected_data)

    def test_create_catalog(self):
        # Given
        cat_name = "created_catalog"
        json_input = {"name": cat_name}
        expected_data = {"name": cat_name, "doi": None}
        try:
            # When
            response = self.client.post("/api/catalogs/", json=json_input)
            # Then
            self.assertEqual(response.status_code, 200)
            for k, v in response.json().items():
                if k == "id":
                    continue
                self.assertEqual(v, expected_data[k])
        finally:
            cat = self.session.exec(
                select(Catalog).where(Catalog.name == cat_name)
            ).one()
            self._delete_entry(cat)

    def test_create_existing_catalog(self):
        # Given
        cat_name = "test-catalog"
        json_input = {"name": cat_name}
        # When
        response = self.client.post("/api/catalogs/", json=json_input)
        # Then
        self.assertEqual(response.status_code, 403)

    def test_delete_catalog_non_existing(self):
        # When
        response = self.client.delete("/api/catalogs/non-existing")
        # Then
        self.assertEqual(response.status_code, 404)

    def test_delete_catalog(self):
        # Given
        name_to_delete = "delete-me"
        # - create catalog to delete
        catalog_to_delete = Catalog(name=name_to_delete)
        self.session.add(catalog_to_delete)
        self.session.commit()
        # When
        response = self.client.delete(f"/api/catalogs/{name_to_delete}")
        # Then
        self.assertEqual(response.status_code, 200)

    def test_update_catalog_non_existing(self):
        # When
        json_input = {"name": "i-dont-exist", "doi": "12345"}
        response = self.client.put("/api/catalogs/", json=json_input)
        # Then
        self.assertEqual(response.status_code, 404)

    def test_update_catalog(self):
        # Given
        name_to_update = "update-me"
        # - create catalog to update
        catalog_to_update = Catalog(name=name_to_update)
        self.session.add(catalog_to_update)
        self.session.commit()
        json_input = {"name": name_to_update, "doi": "12345"}
        expected_data = json_input
        try:
            # When
            response = self.client.put("/api/catalogs/", json=json_input)
            # Then
            self.assertEqual(response.status_code, 200)
            for k, v in response.json().items():
                if k == "id":
                    continue
                self.assertEqual(v, expected_data[k])
        finally:
            self._delete_entry(catalog_to_update)
