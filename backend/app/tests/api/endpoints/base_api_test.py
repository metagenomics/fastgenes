import logging
import unittest

from sqlmodel import create_engine, Session, SQLModel
from sqlmodel.pool import StaticPool
from fastapi.testclient import TestClient

from app.db import get_session
from app.dependency_injections import run_di, clear_di
from app.main import app

logging.basicConfig()
logger = logging.getLogger()


class BaseApiTests(unittest.TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        # Dependency injections
        run_di()

        cls.engine = create_engine(
            "sqlite://", connect_args={"check_same_thread": False}, poolclass=StaticPool
        )
        SQLModel.metadata.create_all(cls.engine)
        with Session(cls.engine) as session:
            cls._create_test_db(session)

    @classmethod
    def _create_test_db(cls, session: Session):
        pass

    def setUp(self) -> None:
        self.session = Session(self.engine)

        def get_session_override():
            return self.session

        app.dependency_overrides[get_session] = get_session_override
        self.client = TestClient(app)

    def tearDown(self) -> None:
        app.dependency_overrides.clear()
        self.session.close()

    @classmethod
    def tearDownClass(cls) -> None:
        clear_di()

    def _delete_entry(self, entry):
        self.session.rollback()
        logger.info(f"Deleting {entry}...")
        self.session.delete(entry)
        self.session.commit()
