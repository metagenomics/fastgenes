from sqlmodel import Session, select

from app.core.models.experiment import Experiment

from .base_api_test import BaseApiTests


class TestExperiments(BaseApiTests):
    @classmethod
    def _create_test_db(cls, session: Session):
        test_experiment = Experiment(name="test-experiment", date="2020-10-10")
        session.add(test_experiment)
        session.commit()
        session.refresh(test_experiment)
        cls.experiment_id_existing = test_experiment.id

    def test_get_experiment_non_existing(self):
        # When
        response = self.client.get("/api/experiments/654")
        # Then
        self.assertEqual(response.status_code, 404)

    def test_get_experiment_existing(self):
        # Given
        expected_id = self.experiment_id_existing
        expected_data = {
            "id": expected_id,
            "name": "test-experiment",
            "date": "2020-10-10",
            "description": None,
            "doi": None,
        }
        # When
        response = self.client.get(f"/api/experiments/{expected_id}")
        # Then
        self.assertEqual(response.status_code, 200)
        self.assertDictEqual(response.json(), expected_data)

    def test_get_all_experiments(self):
        # Given
        expected_data = [
            {
                "id": 1,
                "name": "test-experiment",
                "date": "2020-10-10",
                "description": None,
                "doi": None,
            }
        ]
        # When
        response = self.client.get("/api/experiments/")
        # Then
        self.assertEqual(response.status_code, 200)
        self.assertListEqual(response.json(), expected_data)

    def test_create_experiment(self):
        # Given
        experiment_name = "created_experiment"
        json_input = {
            "name": experiment_name,
            "date": "2021-11-11",
        }
        expected_data = {
            "name": experiment_name,
            "date": "2021-11-11",
            "description": None,
            "doi": None,
        }
        created_id = None
        try:
            # When
            response = self.client.post("/api/experiments/", json=json_input)
            # Then
            self.assertEqual(response.status_code, 200)
            for k, v in response.json().items():
                if k == "id":
                    created_id = v
                else:
                    self.assertEqual(v, expected_data[k])
        finally:
            try:
                experiment = self.session.exec(
                    select(Experiment).where(Experiment.id == created_id)
                ).one()
                self._delete_entry(experiment)
            except Exception:
                pass

    def test_create_experiment_invalid_date(self):
        # Given
        json_input = {
            "name": "invalid-date_experiment",
            "date": "20-2",
        }
        # When
        response = self.client.post("/api/experiments/", json=json_input)
        # Then
        self.assertEqual(response.status_code, 422)

    def test_delete_experiment_non_existing(self):
        # When
        response = self.client.delete("/api/experiments/123456")
        # Then
        self.assertEqual(response.status_code, 404)

    def test_delete_experiment(self):
        # Given
        name_to_delete = "delete-me"
        # - create experiment to delete
        experiment_to_delete = Experiment(name=name_to_delete, date="2010-10-10")
        self.session.add(experiment_to_delete)
        self.session.commit()
        self.session.refresh(experiment_to_delete)
        # When
        response = self.client.delete(f"/api/experiments/{experiment_to_delete.id}")
        # Then
        self.assertEqual(response.status_code, 200)

    def test_update_experiment_non_existing(self):
        # When
        json_input = {
            "id": 123456,
            "name": "never-updated-experiment",
        }
        response = self.client.put("/api/experiments/", json=json_input)
        # Then
        self.assertEqual(response.status_code, 404)

    def test_update_experiment_no_date_exclude_none_true(self):
        # Given
        name_to_update = "update-me"
        new_name = "new-name"
        new_description = "this experiment was difficult"
        # - create experiment to update
        experiment_to_update = Experiment(name=name_to_update, date="2021-10-10")
        self.session.add(experiment_to_update)
        self.session.commit()
        self.session.refresh(experiment_to_update)
        json_input = {
            "id": experiment_to_update.id,
            "name": new_name,
            "description": new_description,
        }
        expected_data = {
            "id": experiment_to_update.id,
            "name": new_name,
            "description": new_description,
            "date": "2021-10-10",
            "doi": None,
        }
        try:
            # When
            response = self.client.put(
                "/api/experiments/?exclude_none=true", json=json_input
            )
            # Then
            self.assertEqual(response.status_code, 200)
            self.assertDictEqual(response.json(), expected_data)
        finally:
            self._delete_entry(experiment_to_update)

    def test_update_experiment_no_date_exclude_none_false(self):
        # Given
        name_to_update = "update-me"
        new_name = "new-name"
        new_description = "this experiment was difficult"
        # - create experiment to update
        experiment_to_update = Experiment(name=name_to_update, date="2021-10-10")
        self.session.add(experiment_to_update)
        self.session.commit()
        self.session.refresh(experiment_to_update)
        json_input = {
            "id": experiment_to_update.id,
            "name": new_name,
            "description": new_description,
        }
        try:
            # When
            response = self.client.put(
                "/api/experiments/?exclude_none=false", json=json_input
            )
            # Then
            self.assertEqual(response.status_code, 422)
        finally:
            self._delete_entry(experiment_to_update)
