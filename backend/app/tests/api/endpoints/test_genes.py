from sqlmodel import Session, select

from app.core.models.catalog import Catalog
from app.core.models.gene import Gene

from .base_api_test import BaseApiTests


class TestGenes(BaseApiTests):
    @classmethod
    def _create_test_db(cls, session: Session):
        test_catalog = Catalog(name="test-catalog")
        session.add(test_catalog)
        session.commit()
        session.refresh(test_catalog)
        cls.catalog_item = test_catalog.dict()
        test_gene = Gene(gene_id="gene1", sequence="actg", length=4)
        session.add(test_gene)
        session.commit()
        session.refresh(test_gene)
        cls.created_id = test_gene.id
        cls.gene_item = test_gene.dict()

    def test_get_gene_non_existing(self):
        # When
        response = self.client.get("/api/genes/non-existing")
        # Then
        self.assertEqual(response.status_code, 404)

    def test_get_gene_existing(self):
        # Given
        expected_id = self.gene_item.get("gene_id")
        expected_data = self.gene_item
        expected_data.update(
            catalogs=[],
            eggnog_annotations=[],
            kegg_annotations=[],
            ncbi_taxonomy_annotations=[],
        )
        # When
        response = self.client.get(f"/api/genes/{expected_id}")
        # Then
        self.assertEqual(response.status_code, 200)
        self.assertDictEqual(response.json(), expected_data)

    def test_get_all_genes(self):
        # Given
        expected_item = self.gene_item
        expected_item.update(
            catalogs=[],
            eggnog_annotations=[],
            kegg_annotations=[],
            ncbi_taxonomy_annotations=[],
        )
        expected_data = [expected_item]
        # When
        response = self.client.get("/api/genes/")
        # Then
        self.assertEqual(response.status_code, 200)
        self.assertListEqual(response.json()["items"], expected_data)

    def test_create_gene(self):
        # Given
        gene_id = "gene9"
        sequence = "atgc"
        json_input = {"gene_id": gene_id, "sequence": sequence}
        expected_data = {
            "gene_id": gene_id,
            "sequence": sequence,
            "length": len(sequence),
            "catalogs": [],
            "eggnog_annotations": [],
            "kegg_annotations": [],
            "ncbi_taxonomy_annotations": [],
        }
        try:
            # When
            response = self.client.post("/api/genes/", json=json_input)
            # Then
            self.assertEqual(response.status_code, 200)
            for k, v in response.json().items():
                if k == "id":
                    continue
                self.assertEqual(v, expected_data.get(k))
        finally:
            gene = self.session.exec(select(Gene).where(Gene.gene_id == gene_id)).one()
            self._delete_entry(gene)

    def test_create_gene_with_existing_catalog(self):
        # Given
        gene_id = "gene9"
        sequence = "atgc"
        json_input = {
            "gene_id": gene_id,
            "sequence": sequence,
            "catalogs": [self.catalog_item["id"]],
        }
        expected_data = {
            "gene_id": gene_id,
            "sequence": sequence,
            "length": len(sequence),
            "catalogs": [self.catalog_item],
            "eggnog_annotations": [],
            "kegg_annotations": [],
            "ncbi_taxonomy_annotations": [],
        }
        try:
            # When
            response = self.client.post("/api/genes/", json=json_input)
            # Then
            self.assertEqual(response.status_code, 200)
            for k, v in response.json().items():
                if k == "id":
                    continue
                self.assertEqual(v, expected_data.get(k))
        finally:
            gene = self.session.exec(select(Gene).where(Gene.gene_id == gene_id)).one()
            self._delete_entry(gene)

    def test_create_gene_with_unexisting_catalog(self):
        # Given
        gene_id = "gene9"
        sequence = "atgc"
        json_input = {"gene_id": gene_id, "sequence": sequence, "catalogs": [456]}
        expected_data = {
            "gene_id": gene_id,
            "sequence": sequence,
            "length": len(sequence),
            "catalogs": [],
            "eggnog_annotations": [],
            "kegg_annotations": [],
            "ncbi_taxonomy_annotations": [],
        }
        try:
            # When
            response = self.client.post("/api/genes/", json=json_input)
            # Then
            self.assertEqual(response.status_code, 200)
            for k, v in response.json().items():
                if k == "id":
                    continue
                self.assertEqual(v, expected_data.get(k))
        finally:
            gene = self.session.exec(select(Gene).where(Gene.gene_id == gene_id)).one()
            self._delete_entry(gene)

    def test_create_existing_gene(self):
        # Given
        gene_id = self.gene_item.get("gene_id")
        json_input = {"gene_id": gene_id, "sequence": "tttt"}
        # When
        response = self.client.post("/api/genes/", json=json_input)
        # Then
        self.assertEqual(response.status_code, 403)

    def test_delete_gene_non_existing(self):
        # When
        response = self.client.delete("/api/genes/non-existing")
        # Then
        self.assertEqual(response.status_code, 404)

    def test_delete_gene(self):
        # Given
        gene_id = "k5"
        sequence = "gggg"
        # - create gene to delete
        gene_to_delete = Gene(gene_id=gene_id, sequence=sequence, length=len(sequence))
        self.session.add(gene_to_delete)
        self.session.commit()
        # When
        response = self.client.delete(f"/api/genes/{gene_id}")
        # Then
        self.assertEqual(response.status_code, 200)

    def test_update_gene_non_existing(self):
        # When
        json_input = {"gene_id": "i-dont-exist", "sequence": "cccc"}
        response = self.client.put("/api/genes/", json=json_input)
        # Then
        self.assertEqual(response.status_code, 404)

    def test_update_gene(self):
        # Given
        gene_id = "gene2"
        sequence = "aaccttgg"
        new_sequence = "tgtgtg"
        # - create gene to update
        gene_to_update = Gene(gene_id=gene_id, sequence=sequence, length=len(sequence))
        self.session.add(gene_to_update)
        self.session.commit()
        json_input = {"gene_id": gene_id, "sequence": new_sequence}
        expected_data = {
            "gene_id": gene_id,
            "sequence": new_sequence,
            "length": len(new_sequence),
            "catalogs": [],
            "eggnog_annotations": [],
            "kegg_annotations": [],
            "ncbi_taxonomy_annotations": [],
        }
        try:
            # When
            response = self.client.put("/api/genes/", json=json_input)
            # Then
            self.assertEqual(response.status_code, 200)
            for k, v in response.json().items():
                if k == "id":
                    continue
                self.assertEqual(v, expected_data[k])
        finally:
            self._delete_entry(gene_to_update)

    def test_update_gene_with_existing_catalog(self):
        # Given
        gene_id = "gene2"
        sequence = "aaccttgg"
        new_sequence = "tgtgtg"
        # - create gene to update
        gene_to_update = Gene(gene_id=gene_id, sequence=sequence, length=len(sequence))
        self.session.add(gene_to_update)
        self.session.commit()
        json_input = {
            "gene_id": gene_id,
            "sequence": new_sequence,
            "catalogs": [self.catalog_item["id"]],
        }
        expected_data = {
            "gene_id": gene_id,
            "sequence": new_sequence,
            "length": len(new_sequence),
            "catalogs": [],
            "eggnog_annotations": [],
            "kegg_annotations": [],
            "ncbi_taxonomy_annotations": [],
        }
        try:
            # When
            response = self.client.put("/api/genes/", json=json_input)
            # Then
            self.assertEqual(response.status_code, 200)
            for k, v in response.json().items():
                if k == "id":
                    continue
                self.assertEqual(v, expected_data[k])
        finally:
            self._delete_entry(gene_to_update)

    def test_update_gene_with_unexisting_catalog(self):
        # Given
        gene_id = "gene2"
        sequence = "aaccttgg"
        new_sequence = "tgtgtg"
        # - create gene to update
        gene_to_update = Gene(gene_id=gene_id, sequence=sequence, length=len(sequence))
        self.session.add(gene_to_update)
        self.session.commit()
        json_input = {
            "gene_id": gene_id,
            "sequence": new_sequence,
            "catalogs": [123456],
        }
        expected_data = {
            "gene_id": gene_id,
            "sequence": new_sequence,
            "length": len(new_sequence),
            "catalogs": [],
            "eggnog_annotations": [],
            "kegg_annotations": [],
            "ncbi_taxonomy_annotations": [],
        }
        try:
            # When
            response = self.client.put("/api/genes/", json=json_input)
            # Then
            self.assertEqual(response.status_code, 200)
            for k, v in response.json().items():
                if k == "id":
                    continue
                self.assertEqual(v, expected_data[k])
        finally:
            self._delete_entry(gene_to_update)

    def test_update_gene_no_name_exclude_none_false(self):
        # Given
        gene_id = "gene2"
        sequence = "aaccttgg"
        # - create gene to update
        gene_to_update = Gene(gene_id=gene_id, sequence=sequence, length=len(sequence))
        self.session.add(gene_to_update)
        self.session.commit()
        json_input = {"gene_id": gene_id}
        try:
            # When
            response = self.client.put(
                "/api/genes/?exclude_none=false", json=json_input
            )
            # Then
            self.assertEqual(response.status_code, 422)
        finally:
            self._delete_entry(gene_to_update)
