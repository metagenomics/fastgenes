from sqlmodel import Session, select

from app.core.models.ncbi_taxonomy import NcbiTaxonomy

from .base_api_test import BaseApiTests


class TestNcbiTaxonomys(BaseApiTests):
    @classmethod
    def _create_test_db(cls, session: Session):
        test_ncbi_taxonomy = NcbiTaxonomy(
            tax_id=1, name="test-ncbi_taxonomy", rank="species"
        )
        session.add(test_ncbi_taxonomy)
        session.commit()
        session.refresh(test_ncbi_taxonomy)
        cls.created_id = test_ncbi_taxonomy.id

    def test_get_ncbi_taxonomy_non_existing(self):
        # When
        response = self.client.get("/api/ncbi_taxonomy/non-existing")
        # Then
        self.assertEqual(response.status_code, 404)

    def test_get_ncbi_taxonomy_existing(self):
        # Given
        expected_id = 1
        expected_data = {
            "tax_id": expected_id,
            "name": "test-ncbi_taxonomy",
            "rank": "species",
            "id": self.created_id,
            "parent_id": None,
            "hierarchy": None,
        }
        # When
        response = self.client.get(f"/api/ncbi_taxonomy/{expected_id}")
        # Then
        self.assertEqual(response.status_code, 200)
        print(response.json())
        self.assertDictEqual(response.json(), expected_data)

    def test_get_all_ncbi_taxonomys(self):
        # Given
        expected_data = [
            {
                "tax_id": 1,
                "name": "test-ncbi_taxonomy",
                "rank": "species",
                "id": self.created_id,
                "parent_id": None,
                "hierarchy": None,
            }
        ]
        # When
        response = self.client.get("/api/ncbi_taxonomy/")
        # Then
        self.assertEqual(response.status_code, 200)
        self.assertListEqual(response.json()["items"], expected_data)

    def test_create_ncbi_taxonomy(self):
        # Given
        tax_id = 2
        ncbi_taxonomy_name = "created_ncbi_taxonomy"
        rank_created = "species"
        json_input = {
            "tax_id": tax_id,
            "name": ncbi_taxonomy_name,
            "rank": rank_created,
        }
        expected_data = {
            "tax_id": tax_id,
            "name": ncbi_taxonomy_name,
            "rank": rank_created,
            "parent_id": None,
            "hierarchy": None,
        }
        try:
            # When
            response = self.client.post("/api/ncbi_taxonomy/", json=json_input)
            # Then
            self.assertEqual(response.status_code, 200)
            for k, v in response.json().items():
                if k == "id":
                    continue
                self.assertEqual(v, expected_data[k])
        finally:
            ncbi_tax = self.session.exec(
                select(NcbiTaxonomy).where(NcbiTaxonomy.tax_id == tax_id)
            ).one()
            self._delete_entry(ncbi_tax)

    def test_create_existing_ncbi_taxonomy(self):
        # Given
        tax_id = 1
        ncbi_taxonomy_name = "test-ncbi_taxonomy"
        json_input = {"tax_id": tax_id, "name": ncbi_taxonomy_name, "rank": "species"}
        # When
        response = self.client.post("/api/ncbi_taxonomy/", json=json_input)
        # Then
        self.assertEqual(response.status_code, 403)

    def test_delete_ncbi_taxonomy_non_existing(self):
        # When
        response = self.client.delete("/api/ncbi_taxonomy/35")
        # Then
        self.assertEqual(response.status_code, 404)

    def test_delete_ncbi_taxonomy(self):
        # Given
        tax_id = 4
        name_to_delete = "delete-me"
        # - create ncbi_taxonomy to delete
        ncbi_taxonomy_to_delete = NcbiTaxonomy(
            tax_id=tax_id, name=name_to_delete, rank="species"
        )
        self.session.add(ncbi_taxonomy_to_delete)
        self.session.commit()
        # When
        response = self.client.delete(f"/api/ncbi_taxonomy/{tax_id}")
        # Then
        self.assertEqual(response.status_code, 200)

    def test_update_ncbi_taxonomy_non_existing_no_rank_exclude_none_true(self):
        # When
        json_input = {"tax_id": 29, "name": "12345"}
        response = self.client.put(
            "/api/ncbi_taxonomy/?exclude_none=true", json=json_input
        )
        # Then
        self.assertEqual(response.status_code, 404)

    def test_update_ncbi_taxonomy_no_rank_exclude_none_true(self):
        # Given
        tax_id = 56
        name_to_update = "update-me"
        new_name = "new-name"
        rank = "species"
        # - create ncbi_taxonomy to update
        ncbi_taxonomy_to_update = NcbiTaxonomy(
            tax_id=tax_id, name=name_to_update, rank=rank
        )
        self.session.add(ncbi_taxonomy_to_update)
        self.session.commit()
        json_input = {"tax_id": tax_id, "name": new_name}
        expected_data = {
            "tax_id": tax_id,
            "name": new_name,
            "rank": rank,
            "parent_id": None,
            "hierarchy": None,
        }
        try:
            # When
            response = self.client.put(
                "/api/ncbi_taxonomy/?exclude_none=true", json=json_input
            )
            # Then
            self.assertEqual(response.status_code, 200)
            for k, v in response.json().items():
                if k == "id":
                    continue
                self.assertEqual(v, expected_data[k])
        finally:
            self._delete_entry(ncbi_taxonomy_to_update)

    def test_update_ncbi_taxonomy_no_rank_exclude_none_false(self):
        # Given
        tax_id = 56
        name_to_update = "update-me"
        new_name = "new-name"
        rank = "species"
        # - create ncbi_taxonomy to update
        ncbi_taxonomy_to_update = NcbiTaxonomy(
            tax_id=tax_id, name=name_to_update, rank=rank
        )
        self.session.add(ncbi_taxonomy_to_update)
        self.session.commit()
        json_input = {"tax_id": tax_id, "name": new_name}
        try:
            # When
            response = self.client.put(
                "/api/ncbi_taxonomy/?exclude_none=false", json=json_input
            )
            self.assertEqual(response.status_code, 422)
        finally:
            self._delete_entry(ncbi_taxonomy_to_update)
