from sqlmodel import Session, select

from app.core.models.eggnog import Eggnog

from .base_api_test import BaseApiTests


class TestEggnogs(BaseApiTests):
    @classmethod
    def _create_test_db(cls, session: Session):
        test_eggnog = Eggnog(eggnog_id="COG1", name="test-eggnog", version="5.0")
        session.add(test_eggnog)
        session.commit()
        session.refresh(test_eggnog)
        cls.created_id = test_eggnog.id

    def test_get_eggnog_non_existing(self):
        # When
        response = self.client.get("/api/eggnog/non-existing")
        # Then
        self.assertEqual(response.status_code, 404)

    def test_get_eggnog_existing(self):
        # Given
        expected_id = "COG1"
        expected_data = {
            "eggnog_id": expected_id,
            "name": "test-eggnog",
            "version": "5.0",
            "id": self.created_id,
        }
        # When
        response = self.client.get(f"/api/eggnog/{expected_id}")
        # Then
        self.assertEqual(response.status_code, 200)
        self.assertDictEqual(response.json(), expected_data)

    def test_get_all_eggnogs(self):
        # Given
        expected_data = [
            {
                "eggnog_id": "COG1",
                "name": "test-eggnog",
                "version": "5.0",
                "id": self.created_id,
            }
        ]
        # When
        response = self.client.get("/api/eggnog/")
        # Then
        self.assertEqual(response.status_code, 200)
        self.assertListEqual(response.json(), expected_data)

    def test_create_eggnog(self):
        # Given
        eggnog_id = "COG9"
        eggnog_name = "created_eggnog"
        json_input = {"eggnog_id": eggnog_id, "name": eggnog_name, "version": "5.0"}
        expected_data = {"eggnog_id": eggnog_id, "name": eggnog_name, "version": "5.0"}
        try:
            # When
            response = self.client.post("/api/eggnog/", json=json_input)
            # Then
            self.assertEqual(response.status_code, 200)
            for k, v in response.json().items():
                if k == "id":
                    continue
                self.assertEqual(v, expected_data[k])
        finally:
            eggnog = self.session.exec(
                select(Eggnog).where(Eggnog.eggnog_id == eggnog_id)
            ).one()
            self._delete_entry(eggnog)

    def test_create_eggnog_invalid_version(self):
        # Given
        eggnog_id = "COG9"
        eggnog_name = "created_eggnog"
        json_input = {"eggnog_id": eggnog_id, "name": eggnog_name, "version": "8.0"}
        # When
        response = self.client.post("/api/eggnog/", json=json_input)
        # Then
        self.assertEqual(response.status_code, 422)

    def test_create_existing_eggnog(self):
        """This test should fail and need to be dealt with in issue#15"""
        # # Given
        # eggnog_id = "COG1"
        # eggnog_name = "test-eggnog"
        # json_input = {"eggnog_id": eggnog_id, "name": eggnog_name, 'version':"5.0"}
        # # When
        # response = self.client.post("/api/eggnog/", json=json_input)
        # # Then
        # self.assertEqual(response.status_code, 403)

    def test_delete_eggnog_non_existing(self):
        # When
        response = self.client.delete("/api/eggnog/non-existing")
        # Then
        self.assertEqual(response.status_code, 404)

    def test_delete_eggnog(self):
        # Given
        eggnog_id = "COG5"
        name_to_delete = "delete-me"
        # - create eggnog to delete
        eggnog_to_delete = Eggnog(
            eggnog_id=eggnog_id, name=name_to_delete, version="5.0"
        )
        self.session.add(eggnog_to_delete)
        self.session.commit()
        # When
        response = self.client.delete(f"/api/eggnog/{eggnog_id}")
        # Then
        self.assertEqual(response.status_code, 200)

    def test_update_eggnog_non_existing(self):
        # When
        json_input = {"eggnog_id": "i-dont-exist", "name": "12345", "version": "5.0"}
        response = self.client.put("/api/eggnog/", json=json_input)
        # Then
        self.assertEqual(response.status_code, 404)

    def test_update_eggnog_no_version_exclude_none_true(self):
        # Given
        eggnog_id = "COG2"
        name_to_update = "update-me"
        new_name = "new-name"
        # - create eggnog to update
        eggnog_to_update = Eggnog(
            eggnog_id=eggnog_id, name=name_to_update, version="5.0"
        )
        self.session.add(eggnog_to_update)
        self.session.commit()
        json_input = {"eggnog_id": eggnog_id, "name": new_name}
        expected_data = {"eggnog_id": eggnog_id, "name": new_name, "version": "5.0"}
        try:
            # When
            response = self.client.put(
                "/api/eggnog/?exclude_none=true", json=json_input
            )
            # Then
            self.assertEqual(response.status_code, 200)
            for k, v in response.json().items():
                if k == "id":
                    continue
                self.assertEqual(v, expected_data[k])
        finally:
            self._delete_entry(eggnog_to_update)

    def test_update_eggnog_no_version_exclude_none_false(self):
        # Given
        eggnog_id = "COG2"
        name_to_update = "update-me"
        new_name = "new-name"
        # - create eggnog to update
        eggnog_to_update = Eggnog(
            eggnog_id=eggnog_id, name=name_to_update, version="5.0"
        )
        self.session.add(eggnog_to_update)
        self.session.commit()
        json_input = {"eggnog_id": eggnog_id, "name": new_name}
        try:
            # When
            response = self.client.put(
                "/api/eggnog/?exclude_none=false", json=json_input
            )
            # Then
            self.assertEqual(response.status_code, 422)
        finally:
            self._delete_entry(eggnog_to_update)
