from sqlmodel import Session, select

from app.core.models.annotations import KeggOrthologyAnnotation
from app.core.models.kegg import KeggOrthology
from app.core.models.experiment import Experiment
from app.core.models.gene import Gene

from ..base_api_test import BaseApiTests


class TestKeggOrthologyAnnotations(BaseApiTests):
    @classmethod
    def _create_test_db(cls, session: Session):
        created_items = {
            "kegg": KeggOrthology(kegg_id="K1", name="test-kegg"),
            "experiment": Experiment(name="test-xp", date="2020-10-10"),
            "gene": Gene(gene_id="g1", name="G1", sequence="actg", length=4),
        }
        test_annotation = KeggOrthologyAnnotation(
            kegg=created_items["kegg"],
            experiment=created_items["experiment"],
            genes=[created_items["gene"]],
        )
        session.add(test_annotation)
        created_items["annotation"] = test_annotation
        session.commit()
        cls.created_id = {}
        for k, v in created_items.items():
            session.refresh(v)
            cls.created_id[k] = v.id

    def test_get_kegg_annotation_non_existing(self):
        # When
        response = self.client.get("/api/annotations/kegg/123")
        # Then
        self.assertEqual(response.status_code, 404)

    def test_get_kegg_annotation_existing(self):
        # Given
        expected_id = self.created_id["annotation"]
        expected_data = {
            "id": expected_id,
            "kegg": {
                "kegg_id": "K1",
                "id": 1,
                "name": "test-kegg",
                "definition": None,
                "ec_numbers": None,
            },
            "experiment": {
                "date": "2020-10-10",
                "description": None,
                "doi": None,
                "id": 1,
                "name": "test-xp",
            },
        }
        # When
        response = self.client.get(f"/api/annotations/kegg/{expected_id}")
        # Then
        self.assertEqual(response.status_code, 200)
        self.assertDictEqual(response.json(), expected_data)

    def test_get_all_kegg_annotations(self):
        # Given
        expected_data = {
            "items": [
                {
                    "id": self.created_id["annotation"],
                    "kegg": {
                        "kegg_id": "K1",
                        "id": 1,
                        "name": "test-kegg",
                        "definition": None,
                        "ec_numbers": None,
                    },
                    "experiment": {
                        "date": "2020-10-10",
                        "description": None,
                        "doi": None,
                        "id": 1,
                        "name": "test-xp",
                    },
                }
            ],
            "total": 1,
            "page": 1,
            "size": 50,
        }
        # When
        response = self.client.get("/api/annotations/kegg/")
        print("loh")
        print(response.json())
        print(expected_data)
        # Then
        self.assertEqual(response.status_code, 200)
        self.assertDictEqual(response.json(), expected_data)

    def test_create_kegg_annotation(self):
        # Given
        kegg_id = self.created_id["kegg"]
        experiment_id = self.created_id["experiment"]
        gene_id = self.created_id["gene"]
        json_input = {
            "kegg_id": kegg_id,
            "experiment_id": experiment_id,
            "genes": [gene_id],
        }
        expected_data = {
            "kegg": {
                "kegg_id": "K1",
                "id": 1,
                "name": "test-kegg",
                "definition": None,
                "ec_numbers": None,
            },
            "experiment": {
                "date": "2020-10-10",
                "description": None,
                "doi": None,
                "id": 1,
                "name": "test-xp",
            },
        }
        try:
            # When
            response = self.client.post("/api/annotations/kegg/", json=json_input)
            print(response.json())
            # Then
            self.assertEqual(response.status_code, 200)
            for k, v in response.json().items():
                if k == "id":
                    created_id = v
                else:
                    self.assertDictEqual(v, expected_data[k])
        finally:
            kegg_annot = self.session.exec(
                select(KeggOrthologyAnnotation).where(
                    KeggOrthologyAnnotation.id == created_id
                )
            ).one()
            self._delete_entry(kegg_annot)

    def test_delete_kegg_annotation_non_existing(self):
        # When
        response = self.client.delete("/api/annotations/kegg/non-existing")
        # Then
        self.assertEqual(response.status_code, 404)

    def test_delete_kegg_annotation(self):
        # Given
        kegg_id = self.created_id["kegg"]
        experiment_id = self.created_id["experiment"]
        gene_id = self.created_id["gene"]
        genes = self.session.exec(select(Gene).where(Gene.id.in_([gene_id]))).all()
        # - create kegg to delete
        kegg_annot_to_delete = KeggOrthologyAnnotation(
            kegg_id=kegg_id, experiment_id=experiment_id, genes=genes
        )
        self.session.add(kegg_annot_to_delete)
        self.session.commit()
        self.session.refresh(kegg_annot_to_delete)
        # When
        response = self.client.delete(
            f"/api/annotations/kegg/{kegg_annot_to_delete.id}"
        )
        # Then
        self.assertEqual(response.status_code, 200)
