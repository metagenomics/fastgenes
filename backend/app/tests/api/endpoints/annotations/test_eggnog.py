from sqlmodel import Session, select

from app.core.models.annotations import EggnogAnnotation
from app.core.models.eggnog import Eggnog
from app.core.models.experiment import Experiment
from app.core.models.gene import Gene

from ..base_api_test import BaseApiTests


class TestEggnogAnnotations(BaseApiTests):
    @classmethod
    def _create_test_db(cls, session: Session):
        created_items = {
            "eggnog": Eggnog(eggnog_id="COG1", name="test-eggnog", version="5.0"),
            "experiment": Experiment(name="test-xp", date="2020-10-10"),
            "gene": Gene(gene_id="g1", name="G1", sequence="actg", length=4),
        }
        test_annotation = EggnogAnnotation(
            eggnog=created_items["eggnog"],
            experiment=created_items["experiment"],
            genes=[created_items["gene"]],
        )
        session.add(test_annotation)
        created_items["annotation"] = test_annotation
        session.commit()
        cls.created_id = {}
        for k, v in created_items.items():
            session.refresh(v)
            cls.created_id[k] = v.id

    def test_get_eggnog_annotation_non_existing(self):
        # When
        response = self.client.get("/api/annotations/eggnog/123")
        # Then
        self.assertEqual(response.status_code, 404)

    def test_get_eggnog_annotation_existing(self):
        # Given
        expected_id = self.created_id["annotation"]
        expected_data = {
            "id": expected_id,
            "eggnog": {
                "eggnog_id": "COG1",
                "id": 1,
                "name": "test-eggnog",
                "version": "5.0",
            },
            "experiment": {
                "date": "2020-10-10",
                "description": None,
                "doi": None,
                "id": 1,
                "name": "test-xp",
            },
        }
        # When
        response = self.client.get(f"/api/annotations/eggnog/{expected_id}")
        # Then
        self.assertEqual(response.status_code, 200)
        self.assertDictEqual(response.json(), expected_data)

    def test_get_all_eggnog_annotations(self):
        # Given
        expected_data = [
            {
                "id": self.created_id["annotation"],
                "eggnog": {
                    "eggnog_id": "COG1",
                    "id": 1,
                    "name": "test-eggnog",
                    "version": "5.0",
                },
                "experiment": {
                    "date": "2020-10-10",
                    "description": None,
                    "doi": None,
                    "id": 1,
                    "name": "test-xp",
                },
            }
        ]
        # When
        response = self.client.get("/api/annotations/eggnog/")
        # Then
        self.assertEqual(response.status_code, 200)
        self.assertListEqual(response.json(), expected_data)

    def test_create_eggnog_annotation(self):
        # Given
        eggnog_id = self.created_id["eggnog"]
        experiment_id = self.created_id["experiment"]
        gene_id = self.created_id["gene"]
        json_input = {
            "eggnog_id": eggnog_id,
            "experiment_id": experiment_id,
            "genes": [gene_id],
        }
        expected_data = {
            "eggnog": {
                "eggnog_id": "COG1",
                "id": 1,
                "name": "test-eggnog",
                "version": "5.0",
            },
            "experiment": {
                "date": "2020-10-10",
                "description": None,
                "doi": None,
                "id": 1,
                "name": "test-xp",
            },
        }
        try:
            # When
            response = self.client.post("/api/annotations/eggnog/", json=json_input)
            # Then
            self.assertEqual(response.status_code, 200)
            for k, v in response.json().items():
                if k == "id":
                    created_id = v
                else:
                    self.assertDictEqual(v, expected_data[k])
        finally:
            eggnog_annot = self.session.exec(
                select(EggnogAnnotation).where(EggnogAnnotation.id == created_id)
            ).one()
            self._delete_entry(eggnog_annot)

    def test_delete_eggnog_annotation_non_existing(self):
        # When
        response = self.client.delete("/api/annotations/eggnog/non-existing")
        # Then
        self.assertEqual(response.status_code, 404)

    def test_delete_eggnog_annotation(self):
        # Given
        eggnog_id = self.created_id["eggnog"]
        experiment_id = self.created_id["experiment"]
        gene_id = self.created_id["gene"]
        genes = self.session.exec(select(Gene).where(Gene.id.in_([gene_id]))).all()
        # - create eggnog to delete
        eggnog_annot_to_delete = EggnogAnnotation(
            eggnog_id=eggnog_id, experiment_id=experiment_id, genes=genes
        )
        self.session.add(eggnog_annot_to_delete)
        self.session.commit()
        self.session.refresh(eggnog_annot_to_delete)
        # When
        response = self.client.delete(
            f"/api/annotations/eggnog/{eggnog_annot_to_delete.id}"
        )
        # Then
        self.assertEqual(response.status_code, 200)
