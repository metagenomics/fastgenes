from sqlmodel import Session, select

from app.core.models.annotations import NcbiTaxonomyAnnotation
from app.core.models.ncbi_taxonomy import NcbiTaxonomy
from app.core.models.experiment import Experiment
from app.core.models.gene import Gene

from ..base_api_test import BaseApiTests


class TestNcbiTaxonomyAnnotations(BaseApiTests):
    @classmethod
    def _create_test_db(cls, session: Session):
        created_items = {
            "ncbi_taxonomy": NcbiTaxonomy(
                tax_id=562, name="Escherichia coli", rank="species"
            ),
            "experiment": Experiment(name="test-xp", date="2020-10-10"),
            "gene": Gene(gene_id="g1", name="G1", sequence="actg", length=4),
        }
        test_annotation = NcbiTaxonomyAnnotation(
            ncbi_taxonomy=created_items["ncbi_taxonomy"],
            experiment=created_items["experiment"],
            genes=[created_items["gene"]],
        )
        session.add(test_annotation)
        created_items["annotation"] = test_annotation
        session.commit()
        cls.created_id = {}
        for k, v in created_items.items():
            session.refresh(v)
            cls.created_id[k] = v.id

    def test_get_ncbi_taxonomy_annotation_non_existing(self):
        # When
        response = self.client.get("/api/annotations/ncbi_taxonomy/123")
        # Then
        self.assertEqual(response.status_code, 404)

    def test_get_ncbi_taxonomy_annotation_existing(self):
        # Given
        expected_id = self.created_id["annotation"]
        expected_data = {
            "id": expected_id,
            "ncbi_taxonomy": {
                "id": 1,
                "tax_id": 562,
                "name": "Escherichia coli",
                "rank": "species",
                "parent_id": None,
                "hierarchy": None,
            },
            "experiment": {
                "date": "2020-10-10",
                "description": None,
                "doi": None,
                "id": 1,
                "name": "test-xp",
            },
        }
        # When
        response = self.client.get(f"/api/annotations/ncbi_taxonomy/{expected_id}")
        # Then
        self.assertEqual(response.status_code, 200)
        self.assertDictEqual(response.json(), expected_data)

    def test_get_all_ncbi_taxonomy_annotations(self):
        # Given
        expected_data = [
            {
                "id": self.created_id["annotation"],
                "ncbi_taxonomy": {
                    "id": 1,
                    "tax_id": 562,
                    "name": "Escherichia coli",
                    "rank": "species",
                    "parent_id": None,
                    "hierarchy": None,
                },
                "experiment": {
                    "date": "2020-10-10",
                    "description": None,
                    "doi": None,
                    "id": 1,
                    "name": "test-xp",
                },
            }
        ]
        # When
        response = self.client.get("/api/annotations/ncbi_taxonomy/")
        # Then
        self.assertEqual(response.status_code, 200)
        self.assertListEqual(response.json(), expected_data)

    def test_create_ncbi_taxonomy_annotation(self):
        # Given
        ncbi_taxonomy_id = self.created_id["ncbi_taxonomy"]
        experiment_id = self.created_id["experiment"]
        gene_id = self.created_id["gene"]
        json_input = {
            "ncbi_taxonomy_id": ncbi_taxonomy_id,
            "experiment_id": experiment_id,
            "genes": [gene_id],
        }
        expected_data = {
            "ncbi_taxonomy": {
                "id": 1,
                "tax_id": 562,
                "name": "Escherichia coli",
                "rank": "species",
                "parent_id": None,
                "hierarchy": None,
            },
            "experiment": {
                "date": "2020-10-10",
                "description": None,
                "doi": None,
                "id": 1,
                "name": "test-xp",
            },
        }
        try:
            # When
            response = self.client.post(
                "/api/annotations/ncbi_taxonomy/", json=json_input
            )
            # Then
            self.assertEqual(response.status_code, 200)
            for k, v in response.json().items():
                if k == "id":
                    created_id = v
                else:
                    self.assertDictEqual(v, expected_data[k])
        finally:
            try:
                ncbi_taxonomy_annot = self.session.exec(
                    select(NcbiTaxonomyAnnotation).where(
                        NcbiTaxonomyAnnotation.id == created_id
                    )
                ).one()
                self._delete_entry(ncbi_taxonomy_annot)
            except Exception:
                pass

    def test_delete_ncbi_taxonomy_annotation_non_existing(self):
        # When
        response = self.client.delete("/api/annotations/ncbi_taxonomy/non-existing")
        # Then
        self.assertEqual(response.status_code, 404)

    def test_delete_ncbi_taxonomy_annotation(self):
        # Given
        ncbi_taxonomy_id = self.created_id["ncbi_taxonomy"]
        experiment_id = self.created_id["experiment"]
        gene_id = self.created_id["gene"]
        genes = self.session.exec(select(Gene).where(Gene.id.in_([gene_id]))).all()
        # - create ncbi_taxonomy to delete
        ncbi_taxonomy_annot_to_delete = NcbiTaxonomyAnnotation(
            ncbi_taxonomy_id=ncbi_taxonomy_id, experiment_id=experiment_id, genes=genes
        )
        self.session.add(ncbi_taxonomy_annot_to_delete)
        self.session.commit()
        # When
        response = self.client.delete(
            f"/api/annotations/ncbi_taxonomy/{ncbi_taxonomy_annot_to_delete.id}"
        )
        # Then
        self.assertEqual(response.status_code, 200)
