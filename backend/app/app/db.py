import redis
from sqlmodel import create_engine, SQLModel, Session

from app.core.config import settings


def _build_database_url():
    if settings.DATABASE_PASSWORD:
        return "postgresql://{user}:{pwd}@{host}:{port}/{db}".format(
            user=settings.DATABASE_USER,
            pwd=settings.DATABASE_PASSWORD,
            host=settings.DATABASE_HOST,
            port=settings.DATABASE_PORT,
            db=settings.DATABASE_NAME,
        )
    return "postgresql://{user}@{host}:{port}/{db}".format(
        user=settings.DATABASE_USER,
        host=settings.DATABASE_HOST,
        port=settings.DATABASE_PORT,
        db=settings.DATABASE_NAME,
    )


engine = create_engine(_build_database_url(), echo=settings.DATABASE_LOGS)


def init_db():
    SQLModel.metadata.create_all(engine)


def reinit_db():
    SQLModel.metadata.drop_all(engine)
    SQLModel.metadata.create_all(engine)


def get_session():
    with Session(engine) as session:
        yield session


def get_redis_client():
    return redis.Redis(settings.REDIS_HOST, settings.REDIS_PORT)
