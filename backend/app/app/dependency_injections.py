import inject

from app.core.repositories.annotations import (
    EggnogAnnotationsRepo,
    KeggOrthologyAnnotationsRepo,
    NcbiTaxonomyAnnotationsRepo,
    SqlModelEggnogAnnotationsRepo,
    SqlModelKeggAnnotationsRepo,
    SqlModelNcbiTaxonomyAnnotationsRepo,
)
from app.core.repositories.catalog import CatalogsRepo, SqlModelCatalogsRepo
from app.core.repositories.eggnog import EggnogsRepo, SqlModelEggnogsRepo
from app.core.repositories.experiment import ExperimentsRepo, SqlModelExperimentsRepo
from app.core.repositories.gene import GenesRepo, SqlModelGenesRepo
from app.core.repositories.kegg import KeggOrthologysRepo, SqlModelKeggOrthologysRepo
from app.core.repositories.ncbi_taxonomy import (
    NcbiTaxonomysRepo,
    SqlModelNcbiTaxonomysRepo,
)


def _di_annotations(binder) -> None:
    binder.bind(EggnogAnnotationsRepo, SqlModelEggnogAnnotationsRepo())
    binder.bind(KeggOrthologyAnnotationsRepo, SqlModelKeggAnnotationsRepo())
    binder.bind(NcbiTaxonomyAnnotationsRepo, SqlModelNcbiTaxonomyAnnotationsRepo())


def _di_entities(binder) -> None:
    binder.bind(GenesRepo, SqlModelGenesRepo())
    binder.bind(CatalogsRepo, SqlModelCatalogsRepo())
    binder.bind(EggnogsRepo, SqlModelEggnogsRepo())
    binder.bind(KeggOrthologysRepo, SqlModelKeggOrthologysRepo())
    binder.bind(NcbiTaxonomysRepo, SqlModelNcbiTaxonomysRepo())
    binder.bind(ExperimentsRepo, SqlModelExperimentsRepo())


def di_configuration(binder) -> None:
    _di_entities(binder)
    _di_annotations(binder)


def run_di() -> None:
    inject.configure(di_configuration)


def clear_di() -> None:
    inject.clear()
