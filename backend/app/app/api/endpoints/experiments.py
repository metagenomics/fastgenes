from typing import List

from fastapi import Depends, APIRouter, HTTPException
from sqlalchemy.exc import NoResultFound, IntegrityError
from sqlmodel import Session

from app.db import get_session
from app.core.schemas.entities.experiment import (
    ExperimentRead,
    ExperimentUpdate,
    ExperimentCreate,
)
from app.core.use_cases.crud.experiment import CrudExperimentUseCase


router = APIRouter()


@router.get("/", response_model=List[ExperimentRead])
async def get_experiments(session: Session = Depends(get_session)):
    use_case = CrudExperimentUseCase()
    experiments = use_case.get_all(session=session)
    return experiments


@router.get("/{experiment_id}", response_model=ExperimentRead)
async def get_experiment(experiment_id: int, session: Session = Depends(get_session)):
    use_case = CrudExperimentUseCase()
    try:
        experiment = use_case.get_experiment(experiment_id, session=session)
    except NoResultFound:
        raise HTTPException(
            status_code=404, detail=f"Experiment [{experiment_id}] not found."
        )
    return experiment


@router.post("/", response_model=ExperimentRead)
async def create_experiment(
    experiment: ExperimentCreate, session: Session = Depends(get_session)
):
    use_case = CrudExperimentUseCase()
    experiment = use_case.create_experiment(experiment, session=session)
    return experiment


@router.put("/", response_model=ExperimentRead)
async def update_experiment(
    experiment: ExperimentUpdate,
    exclude_none: bool = True,
    session: Session = Depends(get_session),
):
    use_case = CrudExperimentUseCase()
    try:
        experiment = use_case.update_experiment(
            experiment, exclude_none=exclude_none, session=session
        )
    except NoResultFound:
        raise HTTPException(
            status_code=404, detail=f"Experiment [{experiment.id}] not found."
        )
    except IntegrityError:
        raise HTTPException(status_code=422, detail="Missing data in input data.")
    return experiment


@router.delete("/{experiment_id}")
async def delete_experiment(
    experiment_id: int, session: Session = Depends(get_session)
):
    use_case = CrudExperimentUseCase()
    try:
        use_case.delete_experiment(experiment_id, session=session)
    except NoResultFound:
        raise HTTPException(
            status_code=404, detail=f"Experiment [{experiment_id}] not found."
        )
    return {"deleted": True}
