from fastapi import Depends, APIRouter, HTTPException
from fastapi_pagination import Page
from fastapi_pagination.ext.sqlmodel import paginate
from sqlalchemy.exc import NoResultFound, IntegrityError
from sqlmodel import Session, select

from app.db import get_session
from app.core.models.gene import Gene
from app.core.schemas.entities.gene import (
    GeneUpdate,
    GeneCreate,
)
from app.core.schemas.tasks.db import DbOperation
from app.core.schemas.nested.gene import GeneReadWithAnnotations
from app.core.use_cases.crud.gene import CrudGeneUseCase


router = APIRouter()


@router.get("/", response_model=Page[GeneReadWithAnnotations])
async def get_gene_orthology_entries(session: Session = Depends(get_session)):
    return paginate(session, select(Gene))


@router.get("/{gene_id}", response_model=GeneReadWithAnnotations)
async def get_gene_orthology(gene_id: str, session: Session = Depends(get_session)):
    use_case = CrudGeneUseCase()
    try:
        gene = use_case.get_gene(gene_id, session=session)
    except NoResultFound:
        raise HTTPException(status_code=404, detail=f"Gene [{gene_id}] not found.")
    return gene


@router.post("/", response_model=GeneReadWithAnnotations)
async def create_gene_orthology(
    gene: GeneCreate, session: Session = Depends(get_session)
):
    use_case = CrudGeneUseCase()
    try:
        gene = use_case.create_gene(gene, session=session)
    except IntegrityError:
        raise HTTPException(
            status_code=403, detail=f"Gene [{gene.gene_id}] already exists."
        )
    return gene


@router.put("/", response_model=GeneReadWithAnnotations)
async def update_gene_orthology(
    gene: GeneUpdate,
    exclude_none: bool = True,
    session: Session = Depends(get_session),
):
    use_case = CrudGeneUseCase()
    try:
        gene = use_case.update_gene(gene, session=session, exclude_none=exclude_none)
    except NoResultFound:
        raise HTTPException(status_code=404, detail=f"Gene [{gene.gene_id}] not found.")
    except IntegrityError:
        raise HTTPException(status_code=422, detail="Missing data in input data.")
    return gene


@router.delete("/{gene_id}")
async def delete_gene_orthology(gene_id: str, session: Session = Depends(get_session)):
    use_case = CrudGeneUseCase()
    try:
        use_case.delete_gene(gene_id, session=session)
    except NoResultFound:
        raise HTTPException(status_code=404, detail=f"Gene [{gene_id}] not found.")
    return {"deleted": True}


@router.delete("/", response_model=DbOperation)
async def delete_all_genes(session: Session = Depends(get_session)):
    use_case = CrudGeneUseCase()
    return use_case.delete_all(session=session)
