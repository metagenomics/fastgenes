from typing import List

from fastapi import Depends, APIRouter, HTTPException
from sqlalchemy.exc import NoResultFound, IntegrityError
from sqlmodel import Session

from app.db import get_session
from app.core.schemas.entities.eggnog import EggnogRead, EggnogUpdate, EggnogCreate
from app.core.use_cases.crud.eggnog import CrudEggnogUseCase


router = APIRouter()


@router.get("/", response_model=List[EggnogRead])
async def get_eggnogs(session: Session = Depends(get_session)):
    use_case = CrudEggnogUseCase()
    eggnogs = use_case.get_all(session=session)
    return eggnogs


@router.get("/{eggnog_id}", response_model=EggnogRead)
async def get_eggnog(eggnog_id: str, session: Session = Depends(get_session)):
    use_case = CrudEggnogUseCase()
    try:
        eggnog = use_case.get_eggnog(eggnog_id, session=session)
    except NoResultFound:
        raise HTTPException(status_code=404, detail=f"Eggnog [{eggnog_id}] not found.")
    return eggnog


@router.post("/", response_model=EggnogRead)
async def create_eggnog(eggnog: EggnogCreate, session: Session = Depends(get_session)):
    use_case = CrudEggnogUseCase()
    eggnog = use_case.create_eggnog(eggnog, session=session)
    return eggnog


@router.put("/", response_model=EggnogRead)
async def update_eggnog(
    eggnog: EggnogUpdate,
    exclude_none: bool = True,
    session: Session = Depends(get_session),
):
    use_case = CrudEggnogUseCase()
    try:
        eggnog = use_case.update_eggnog(
            eggnog, exclude_none=exclude_none, session=session
        )
    except NoResultFound:
        raise HTTPException(
            status_code=404, detail=f"Eggnog [{eggnog.eggnog_id}] not found."
        )
    except IntegrityError:
        raise HTTPException(status_code=422, detail="Missing data in input data.")
    return eggnog


@router.delete("/{eggnog_id}")
async def delete_eggnog(eggnog_id: str, session: Session = Depends(get_session)):
    use_case = CrudEggnogUseCase()
    try:
        use_case.delete_eggnog(eggnog_id, session=session)
    except NoResultFound:
        raise HTTPException(status_code=404, detail=f"Eggnog [{eggnog_id}] not found.")
    return {"deleted": True}
