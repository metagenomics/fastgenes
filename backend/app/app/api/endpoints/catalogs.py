from typing import List

from fastapi import Depends, APIRouter, HTTPException
from sqlalchemy.exc import NoResultFound, IntegrityError
from sqlmodel import Session

from app.db import get_session
from app.core.schemas.entities.catalog import CatalogRead, CatalogUpdate, CatalogCreate
from app.core.use_cases.crud.catalog import CrudCatalogUseCase


router = APIRouter()


@router.get("/", response_model=List[CatalogRead])
async def get_catalogs(session: Session = Depends(get_session)):
    use_case = CrudCatalogUseCase()
    catalogs = use_case.get_all(session=session)
    return catalogs


@router.get("/{catalog_name}", response_model=CatalogRead)
async def get_catalog(catalog_name: str, session: Session = Depends(get_session)):
    use_case = CrudCatalogUseCase()
    try:
        catalog = use_case.get_catalog(catalog_name, session=session)
    except NoResultFound:
        raise HTTPException(
            status_code=404, detail=f"Catalog [{catalog_name}] not found."
        )
    return catalog


@router.post("/", response_model=CatalogRead)
async def create_catalog(
    catalog: CatalogCreate, session: Session = Depends(get_session)
):
    use_case = CrudCatalogUseCase()
    try:
        catalog = use_case.create_catalog(catalog, session=session)
    except IntegrityError:
        raise HTTPException(
            status_code=403, detail=f"Catalog [{catalog.name}] already exists."
        )
    return catalog


@router.put("/", response_model=CatalogRead)
async def update_catalog(
    catalog: CatalogUpdate, session: Session = Depends(get_session)
):
    use_case = CrudCatalogUseCase()
    try:
        catalog = use_case.update_catalog(catalog, session=session)
    except NoResultFound:
        raise HTTPException(
            status_code=404, detail=f"Catalog [{catalog.name}] not found."
        )
    return catalog


@router.delete("/{catalog_name}")
async def delete_catalog(catalog_name: str, session: Session = Depends(get_session)):
    use_case = CrudCatalogUseCase()
    try:
        use_case.delete_catalog(catalog_name, session=session)
    except NoResultFound:
        raise HTTPException(
            status_code=404, detail=f"Catalog [{catalog_name}] not found."
        )
    return {"deleted": True}
