from fastapi import Depends, APIRouter, HTTPException
from fastapi_pagination import Page
from fastapi_pagination.ext.sqlmodel import paginate
from sqlalchemy.exc import NoResultFound, IntegrityError
from sqlmodel import Session, select


from app.db import get_session
from app.core.models.kegg import KeggOrthology
from app.core.schemas.entities.kegg import (
    KeggOrthologyRead,
    KeggOrthologyUpdate,
    KeggOrthologyCreate,
)
from app.core.use_cases.crud.kegg import CrudKeggOrthologyUseCase


router = APIRouter()


@router.get("/", response_model=Page[KeggOrthologyRead])
async def get_kegg_orthology_entries(session: Session = Depends(get_session)):
    return paginate(session, select(KeggOrthology))


@router.get("/{kegg_id}", response_model=KeggOrthologyRead)
async def get_kegg_orthology(kegg_id: str, session: Session = Depends(get_session)):
    use_case = CrudKeggOrthologyUseCase()
    try:
        kegg = use_case.get_kegg(kegg_id, session=session)
    except NoResultFound:
        raise HTTPException(
            status_code=404, detail=f"KeggOrthology [{kegg_id}] not found."
        )
    return kegg


@router.post("/", response_model=KeggOrthologyRead)
async def create_kegg_orthology(
    kegg: KeggOrthologyCreate, session: Session = Depends(get_session)
):
    use_case = CrudKeggOrthologyUseCase()
    try:
        kegg = use_case.create_kegg(kegg, session=session)
    except IntegrityError:
        raise HTTPException(
            status_code=403, detail=f"KeggOrthology [{kegg.name}] already exists."
        )
    return kegg


@router.put("/", response_model=KeggOrthologyRead)
async def update_kegg_orthology(
    kegg: KeggOrthologyUpdate,
    exclude_none: bool = True,
    session: Session = Depends(get_session),
):
    use_case = CrudKeggOrthologyUseCase()
    try:
        kegg = use_case.update_kegg(kegg, session=session, exclude_none=exclude_none)
    except NoResultFound:
        raise HTTPException(
            status_code=404, detail=f"KeggOrthology [{kegg.kegg_id}] not found."
        )
    except IntegrityError:
        raise HTTPException(status_code=422, detail="Missing data in input data.")
    return kegg


@router.delete("/{kegg_id}")
async def delete_kegg_orthology(kegg_id: str, session: Session = Depends(get_session)):
    use_case = CrudKeggOrthologyUseCase()
    try:
        use_case.delete_kegg(kegg_id, session=session)
    except NoResultFound:
        raise HTTPException(
            status_code=404, detail=f"KeggOrthology [{kegg_id}] not found."
        )
    return {"deleted": True}
