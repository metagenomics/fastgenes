from fastapi import Depends, APIRouter, HTTPException
from fastapi_pagination import Page
from fastapi_pagination.ext.sqlmodel import paginate
from sqlalchemy.exc import NoResultFound, IntegrityError
from sqlmodel import Session, select

from app.db import get_session
from app.core.models.ncbi_taxonomy import NcbiTaxonomy
from app.core.schemas.entities.ncbi_taxonomy import (
    NcbiTaxonomyRead,
    NcbiTaxonomyUpdate,
    NcbiTaxonomyCreate,
)
from app.core.use_cases.crud.ncbi_taxonomy import CrudNcbiTaxonomyUseCase


router = APIRouter()


@router.get("/", response_model=Page[NcbiTaxonomyRead])
async def get_ncbi_taxonomy_entries(session: Session = Depends(get_session)):
    return paginate(session, select(NcbiTaxonomy))


@router.get("/{tax_id}", response_model=NcbiTaxonomyRead)
async def get_ncbi_taxonomy(tax_id: str, session: Session = Depends(get_session)):
    use_case = CrudNcbiTaxonomyUseCase()
    try:
        ncbi_taxonomy = use_case.get_ncbi_taxonomy(tax_id, session=session)
    except NoResultFound:
        raise HTTPException(
            status_code=404, detail=f"NcbiTaxonomy [tax_id: {tax_id}] not found."
        )
    return ncbi_taxonomy


@router.post("/", response_model=NcbiTaxonomyRead)
async def create_ncbi_taxonomy(
    ncbi_taxonomy: NcbiTaxonomyCreate, session: Session = Depends(get_session)
):
    use_case = CrudNcbiTaxonomyUseCase()
    try:
        ncbi_taxonomy = use_case.create_ncbi_taxonomy(ncbi_taxonomy, session=session)
    except IntegrityError:
        raise HTTPException(
            status_code=403,
            detail=f"NcbiTaxonomy [{ncbi_taxonomy.tax_id}: {ncbi_taxonomy.name}] already exists.",
        )
    return ncbi_taxonomy


@router.put("/", response_model=NcbiTaxonomyRead)
async def update_ncbi_taxonomy(
    ncbi_taxonomy: NcbiTaxonomyUpdate,
    exclude_none: bool = True,
    session: Session = Depends(get_session),
):
    use_case = CrudNcbiTaxonomyUseCase()
    try:
        ncbi_taxonomy = use_case.update_ncbi_taxonomy(
            ncbi_taxonomy, session=session, exclude_none=exclude_none
        )
    except NoResultFound as e:
        raise HTTPException(status_code=404, detail=f"{e}.")
    except IntegrityError:
        raise HTTPException(status_code=422, detail="Missing data in input data.")
    return ncbi_taxonomy


@router.delete("/{tax_id}")
async def delete_ncbi_taxonomy(tax_id: str, session: Session = Depends(get_session)):
    use_case = CrudNcbiTaxonomyUseCase()
    try:
        use_case.delete_ncbi_taxonomy(tax_id, session=session)
    except NoResultFound:
        raise HTTPException(
            status_code=404, detail=f"NcbiTaxonomy [{tax_id}] not found."
        )
    return {"deleted": True}
