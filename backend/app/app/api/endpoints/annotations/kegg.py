from fastapi import Depends, APIRouter, HTTPException
from fastapi_pagination import Page
from fastapi_pagination.ext.sqlmodel import paginate
from sqlalchemy.exc import NoResultFound
from sqlmodel import Session, select

from app.db import get_session
from app.core.models.annotations import KeggOrthologyAnnotation
from app.core.schemas.entities.annotations import (
    KeggOrthologyAnnotationRead,
    KeggOrthologyAnnotationCreate,
)
from app.core.use_cases.crud.annotations import CrudKeggOrthologyAnnotationUseCase


router = APIRouter()


@router.get("/", response_model=Page[KeggOrthologyAnnotationRead])
async def get_kegg_orthology_annotations(session: Session = Depends(get_session)):
    return paginate(session, select(KeggOrthologyAnnotation))


@router.get(
    "/{kegg_orthology_annotation_id}", response_model=KeggOrthologyAnnotationRead
)
async def get_kegg_orthology_annotation(
    kegg_orthology_annotation_id: str, session: Session = Depends(get_session)
):
    use_case = CrudKeggOrthologyAnnotationUseCase()
    try:
        kegg_orthology_annotation = use_case.get_annotation(
            kegg_orthology_annotation_id, session=session
        )
    except NoResultFound:
        raise HTTPException(
            status_code=404,
            detail=f"KeggOrthologyAnnotation [{kegg_orthology_annotation_id}] not found.",
        )
    return kegg_orthology_annotation


@router.post("/", response_model=KeggOrthologyAnnotationRead)
async def create_kegg_orthology_annotation(
    kegg_orthology_annotation: KeggOrthologyAnnotationCreate,
    session: Session = Depends(get_session),
):
    use_case = CrudKeggOrthologyAnnotationUseCase()
    kegg_orthology_annotation = use_case.create_annotation(
        kegg_orthology_annotation, session=session
    )
    return kegg_orthology_annotation


@router.delete("/{kegg_orthology_annotation_id}")
async def delete_kegg_orthology_annotation(
    kegg_orthology_annotation_id: str, session: Session = Depends(get_session)
):
    use_case = CrudKeggOrthologyAnnotationUseCase()
    try:
        use_case.delete_annotation(kegg_orthology_annotation_id, session=session)
    except NoResultFound:
        raise HTTPException(
            status_code=404,
            detail=f"KeggOrthologyAnnotation [{kegg_orthology_annotation_id}] not found.",
        )
    return {"deleted": True}
