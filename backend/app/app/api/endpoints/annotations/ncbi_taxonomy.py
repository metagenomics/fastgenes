from typing import List

from fastapi import Depends, APIRouter, HTTPException
from sqlalchemy.exc import NoResultFound
from sqlmodel import Session

from app.db import get_session
from app.core.schemas.entities.annotations import (
    NcbiTaxonomyAnnotationRead,
    NcbiTaxonomyAnnotationCreate,
)
from app.core.use_cases.crud.annotations import CrudNcbiTaxonomyAnnotationUseCase


router = APIRouter()


@router.get("/", response_model=List[NcbiTaxonomyAnnotationRead])
async def get_ncbi_taxonomy_annotations(session: Session = Depends(get_session)):
    use_case = CrudNcbiTaxonomyAnnotationUseCase()
    ncbi_taxonomy_annotations = use_case.get_all(session=session)
    for annot in ncbi_taxonomy_annotations:
        print(annot.genes)
    return ncbi_taxonomy_annotations


@router.get("/{ncbi_taxonomy_annotation_id}", response_model=NcbiTaxonomyAnnotationRead)
async def get_ncbi_taxonomy_annotation(
    ncbi_taxonomy_annotation_id: str, session: Session = Depends(get_session)
):
    use_case = CrudNcbiTaxonomyAnnotationUseCase()
    try:
        ncbi_taxonomy_annotation = use_case.get_annotation(
            ncbi_taxonomy_annotation_id, session=session
        )
    except NoResultFound:
        raise HTTPException(
            status_code=404,
            detail=f"NcbiTaxonomyAnnotation [{ncbi_taxonomy_annotation_id}] not found.",
        )
    return ncbi_taxonomy_annotation


@router.post("/", response_model=NcbiTaxonomyAnnotationRead)
async def create_ncbi_taxonomy_annotation(
    ncbi_taxonomy_annotation: NcbiTaxonomyAnnotationCreate,
    session: Session = Depends(get_session),
):
    use_case = CrudNcbiTaxonomyAnnotationUseCase()
    ncbi_taxonomy_annotation = use_case.create_annotation(
        ncbi_taxonomy_annotation, session=session
    )
    return ncbi_taxonomy_annotation


@router.delete("/{ncbi_taxonomy_annotation_id}")
async def delete_ncbi_taxonomy_annotation(
    ncbi_taxonomy_annotation_id: str, session: Session = Depends(get_session)
):
    use_case = CrudNcbiTaxonomyAnnotationUseCase()
    try:
        use_case.delete_annotation(ncbi_taxonomy_annotation_id, session=session)
    except NoResultFound:
        raise HTTPException(
            status_code=404,
            detail=f"NcbiTaxonomyAnnotation [{ncbi_taxonomy_annotation_id}] not found.",
        )
    return {"deleted": True}
