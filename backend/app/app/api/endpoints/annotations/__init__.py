from fastapi import APIRouter

from app.api.endpoints.annotations import eggnog, kegg, ncbi_taxonomy

router = APIRouter()

router.include_router(eggnog.router, prefix="/eggnog")
router.include_router(kegg.router, prefix="/kegg")
router.include_router(ncbi_taxonomy.router, prefix="/ncbi_taxonomy")
