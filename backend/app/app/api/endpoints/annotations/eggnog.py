from typing import List

from fastapi import Depends, APIRouter, HTTPException
from sqlalchemy.exc import NoResultFound
from sqlmodel import Session

from app.db import get_session
from app.core.schemas.entities.annotations import (
    EggnogAnnotationRead,
    EggnogAnnotationCreate,
)
from app.core.use_cases.crud.annotations import CrudEggnogAnnotationUseCase


router = APIRouter()


@router.get("/", response_model=List[EggnogAnnotationRead])
async def get_eggnog_annotations(session: Session = Depends(get_session)):
    use_case = CrudEggnogAnnotationUseCase()
    eggnog_annotations = use_case.get_all(session=session)
    return eggnog_annotations


@router.get("/{eggnog_annotation_id}", response_model=EggnogAnnotationRead)
async def get_eggnog_annotation(
    eggnog_annotation_id: str, session: Session = Depends(get_session)
):
    use_case = CrudEggnogAnnotationUseCase()
    try:
        eggnog_annotation = use_case.get_annotation(
            eggnog_annotation_id, session=session
        )
    except NoResultFound:
        raise HTTPException(
            status_code=404,
            detail=f"EggnogAnnotation [{eggnog_annotation_id}] not found.",
        )
    return eggnog_annotation


@router.post("/", response_model=EggnogAnnotationRead)
async def create_eggnog_annotation(
    eggnog_annotation: EggnogAnnotationCreate, session: Session = Depends(get_session)
):
    use_case = CrudEggnogAnnotationUseCase()
    eggnog_annotation = use_case.create_annotation(eggnog_annotation, session=session)
    return eggnog_annotation


@router.delete("/{eggnog_annotation_id}")
async def delete_eggnog_annotation(
    eggnog_annotation_id: str, session: Session = Depends(get_session)
):
    use_case = CrudEggnogAnnotationUseCase()
    try:
        use_case.delete_annotation(eggnog_annotation_id, session=session)
    except NoResultFound:
        raise HTTPException(
            status_code=404,
            detail=f"EggnogAnnotation [{eggnog_annotation_id}] not found.",
        )
    return {"deleted": True}
