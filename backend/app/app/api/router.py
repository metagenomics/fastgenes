from fastapi import APIRouter
from fastapi_pagination import add_pagination

from app.api.endpoints import (
    annotations,
    catalogs,
    experiments,
    genes,
    keggs,
    ncbi_taxonomy,
    eggnogs,
)
from app.api.endpoints.admin import db

api_router = APIRouter()
api_router.include_router(genes.router, prefix="/genes", tags=["Genes"])
api_router.include_router(catalogs.router, prefix="/catalogs", tags=["Catalogs"])
api_router.include_router(
    annotations.router, prefix="/annotations", tags=["Annotations"]
)
api_router.include_router(
    experiments.router, prefix="/experiments", tags=["Experiments"]
)
api_router.include_router(
    keggs.router, prefix="/kegg-orthology", tags=["KeggOrthology"]
)
api_router.include_router(eggnogs.router, prefix="/eggnog", tags=["EggNOG"])
api_router.include_router(
    ncbi_taxonomy.router, prefix="/ncbi_taxonomy", tags=["NCBI Taxonomy"]
)

api_router.include_router(db.router, prefix="/admin_db", tags=["Admin DB"])

add_pagination(api_router)
