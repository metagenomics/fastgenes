from enum import Enum
from typing import List

from slugify import slugify
from sqlmodel import Field, SQLModel, Relationship


class Version(str, Enum):
    eggnogv3 = "3.0"
    eggnogv5 = "5.0"


class EggnogUnique(SQLModel):
    """Schema to support retrieval of unique eggnog entry."""

    eggnog_id: str
    version: Version


class EggnogBase(EggnogUnique):
    name: str

    @property
    def slug_name(self):
        return slugify(self.name)


class Eggnog(EggnogBase, table=True):
    id: int = Field(default=None, primary_key=True)

    annotations: List["EggnogAnnotation"] = Relationship(back_populates="eggnog")
