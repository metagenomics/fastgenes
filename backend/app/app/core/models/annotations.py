from typing import List

from sqlmodel import Field, SQLModel, Relationship

from app.core.models.gene_links import (
    GeneKeggOrthologyAnnotationLink,
    GeneEggnogAnnotationLink,
    GeneNcbiTaxonomyLink,
)


class AnnotationBase(SQLModel):
    experiment_id: int = Field(foreign_key="experiment.id")


# ------------------ EggNOG ------------------


class EggnogAnnotationBase(AnnotationBase):
    eggnog_id: int = Field(foreign_key="eggnog.id")


class EggnogAnnotation(EggnogAnnotationBase, table=True):
    id: int = Field(default=None, primary_key=True)

    genes: List["Gene"] = Relationship(
        back_populates="eggnog_annotations", link_model=GeneEggnogAnnotationLink
    )
    eggnog: "Eggnog" = Relationship(back_populates="annotations")
    experiment: "Experiment" = Relationship(back_populates="eggnog_annotations")


# ------------------ KEGG ------------------


class KeggOrthologyAnnotationBase(AnnotationBase):
    kegg_id: int = Field(foreign_key="keggorthology.id")


class KeggOrthologyAnnotation(KeggOrthologyAnnotationBase, table=True):
    id: int = Field(default=None, primary_key=True)

    genes: List["Gene"] = Relationship(
        back_populates="kegg_annotations", link_model=GeneKeggOrthologyAnnotationLink
    )
    kegg: "KeggOrthology" = Relationship(back_populates="annotations")
    experiment: "Experiment" = Relationship(back_populates="kegg_annotations")


# ------------------ NCBI Taxonomy ------------------


class NcbiTaxonomyAnnotationBase(AnnotationBase):
    ncbi_taxonomy_id: int = Field(foreign_key="ncbitaxonomy.id")


class NcbiTaxonomyAnnotation(NcbiTaxonomyAnnotationBase, table=True):
    id: int = Field(default=None, primary_key=True)

    genes: List["Gene"] = Relationship(
        back_populates="ncbi_taxonomy_annotations", link_model=GeneNcbiTaxonomyLink
    )
    ncbi_taxonomy: "NcbiTaxonomy" = Relationship(back_populates="annotations")
    experiment: "Experiment" = Relationship(back_populates="ncbi_taxonomy_annotations")
