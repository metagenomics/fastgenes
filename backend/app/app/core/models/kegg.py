from typing import List, Optional

from sqlmodel import Field, Relationship, SQLModel, VARCHAR, Column, JSON


class KeggOrthologyBase(SQLModel):
    kegg_id: str = Field(sa_column=Column("kegg_id", VARCHAR, unique=True))
    name: str
    definition: Optional[str]
    ec_numbers: Optional[List[str]] = Field(sa_column=Column(JSON))


class KeggOrthology(KeggOrthologyBase, table=True):
    id: int = Field(default=None, primary_key=True)

    annotations: List["KeggOrthologyAnnotation"] = Relationship(back_populates="kegg")
