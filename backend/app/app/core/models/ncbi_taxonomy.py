from typing import List, Optional

import slugify
from sqlalchemy.orm import backref
from sqlmodel import Field, Relationship, Column, JSON, SQLModel, INTEGER


class NcbiTaxonomyBase(SQLModel):
    tax_id: int = Field(sa_column=Column("tax_id", INTEGER, unique=True))
    name: str
    rank: str = Field(index=True)

    @property
    def slug_name(self):
        return slugify(self.name)


class NcbiTaxonomy(NcbiTaxonomyBase, table=True):
    id: int = Field(default=None, primary_key=True)
    parent_id: Optional[int] = Field(default=None, foreign_key="ncbitaxonomy.id")
    hierarchy: Optional[dict] = Field(sa_column=Column(JSON))

    # parent: Optional["NcbiTaxonomy"] = Relationship(back_populates="children")
    children: List["NcbiTaxonomy"] = Relationship(
        sa_relationship_kwargs=dict(
            cascade="all", backref=backref("parent", remote_side="NcbiTaxonomy.id")
        )
    )
    annotations: List["NcbiTaxonomyAnnotation"] = Relationship(
        back_populates="ncbi_taxonomy"
    )

    def build_hierarchy(self) -> dict:
        """
        Build and save parental hierarchy for an entry
        """
        hierarchy = {}
        if self.name != "root":
            hierarchy[self.rank] = {"tax_id": self.tax_id, "name": self.name}
            if self.parent is not None:
                hierarchy = {
                    **hierarchy,
                    **getattr(self.parent, "hierarchy", self.parent.build_hierarchy()),
                }
        self.hierarchy = hierarchy
        return hierarchy
