from typing import Optional

from sqlmodel import Field, SQLModel


class GeneLinkBase(SQLModel):
    gene_id: Optional[int] = Field(
        default=None, foreign_key="gene.id", primary_key=True
    )


class GeneCatalogLink(GeneLinkBase, table=True):
    catalog_id: Optional[int] = Field(
        default=None, foreign_key="catalog.id", primary_key=True
    )


class GeneKeggOrthologyAnnotationLink(GeneLinkBase, table=True):
    kegg_annotation_id: Optional[int] = Field(
        default=None, foreign_key="keggorthologyannotation.id", primary_key=True
    )


class GeneEggnogAnnotationLink(GeneLinkBase, table=True):
    eggnog_annotation_id: Optional[int] = Field(
        default=None, foreign_key="eggnogannotation.id", primary_key=True
    )


class GeneNcbiTaxonomyLink(GeneLinkBase, table=True):
    ncbi_taxonomy_annotation_id: Optional[int] = Field(
        default=None, foreign_key="ncbitaxonomyannotation.id", primary_key=True
    )
