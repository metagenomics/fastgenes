import datetime
from typing import List, Optional

from sqlmodel import Field, SQLModel, Relationship


class ExperimentBase(SQLModel):
    name: str
    date: datetime.date
    description: Optional[str]
    doi: Optional[str]


class Experiment(ExperimentBase, table=True):
    id: int = Field(default=None, primary_key=True)

    kegg_annotations: List["KeggOrthologyAnnotation"] = Relationship(
        back_populates="experiment"
    )
    eggnog_annotations: List["EggnogAnnotation"] = Relationship(
        back_populates="experiment"
    )
    ncbi_taxonomy_annotations: List["NcbiTaxonomyAnnotation"] = Relationship(
        back_populates="experiment"
    )

    def __repr__(self) -> str:
        return f"<{self.__class__.__name__}: {self.name} (id={self.id}, {self.date})>"

    def __str__(self) -> str:
        return f"<{self.__class__.__name__}: {self.name} (id={self.id}, {self.date})>"
