from typing import List, Optional

from slugify import slugify
from sqlmodel import Field, SQLModel, Relationship, VARCHAR, Column

from app.core.models.gene_links import GeneCatalogLink


class CatalogBase(SQLModel):
    name: str = Field(sa_column=Column("name", VARCHAR, unique=True))
    doi: Optional[str]

    @property
    def slug_name(self):
        return slugify(self.name)


class Catalog(CatalogBase, table=True):
    id: int = Field(default=None, primary_key=True)

    genes: List["Gene"] = Relationship(
        back_populates="catalogs", link_model=GeneCatalogLink
    )
