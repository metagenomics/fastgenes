from typing import List

from sqlmodel import Field, SQLModel, Relationship, Column, VARCHAR

from app.core.models.gene_links import (
    GeneCatalogLink,
    GeneKeggOrthologyAnnotationLink,
    GeneEggnogAnnotationLink,
    GeneNcbiTaxonomyLink,
)


class GeneBase(SQLModel):
    gene_id: str = Field(sa_column=Column("gene_id", VARCHAR, unique=True))
    sequence: str


class Gene(GeneBase, table=True):
    id: int = Field(default=None, primary_key=True)
    length: int = Field(index=True)

    eggnog_annotations: List["EggnogAnnotation"] = Relationship(
        back_populates="genes", link_model=GeneEggnogAnnotationLink
    )
    kegg_annotations: List["KeggOrthologyAnnotation"] = Relationship(
        back_populates="genes", link_model=GeneKeggOrthologyAnnotationLink
    )
    ncbi_taxonomy_annotations: List["NcbiTaxonomyAnnotation"] = Relationship(
        back_populates="genes", link_model=GeneNcbiTaxonomyLink
    )
    catalogs: List["Catalog"] = Relationship(
        back_populates="genes", link_model=GeneCatalogLink
    )
