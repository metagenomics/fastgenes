from pydantic import BaseSettings


class Settings(BaseSettings):
    # API
    API_STR: str = "/api"
    # DB
    DATABASE_HOST: str = "postgresql"
    DATABASE_USER: str = "postgres"
    DATABASE_PASSWORD: str = ""
    DATABASE_NAME: str = "fastgenes"
    DATABASE_PORT: int = 5432
    DATABASE_LOGS: bool = False
    # Progression bar
    DISABLE_TQDM: bool = True
    # Celery
    CELERY_BROKER_URL: str = "redis://localhost:6379"
    CELERY_RESULT_BACKEND: str = "redis://localhost:6379"
    CELERY_TASK_TRACK_STARTED: bool = True
    # REDIS
    REDIS_HOST: str = "redis"
    REDIS_PORT: str = "6379"
    # Download URLs
    NCBI_TAX_DUMPS_URL: str = "https://ftp.ncbi.nih.gov/pub/taxonomy/taxdump.tar.gz"

    class Config:
        env_file = "app/.env"
        env_file_encoding = "utf-8"


settings = Settings()
