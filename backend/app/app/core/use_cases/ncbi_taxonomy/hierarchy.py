import logging

import inject
from tqdm import tqdm

from app.core.repositories.ncbi_taxonomy import NcbiTaxonomysRepo
from app.core.schemas.entities.ncbi_taxonomy import NcbiTaxonomyUpdate
from app.core.schemas.tasks.db import DbOperation

logger = logging.getLogger()


class GenerateNcbiTaxonomyHierarchyUseCase:
    @inject.autoparams("ncbi_taxonomy_repo")
    def __init__(
        self,
        ncbi_taxonomy_repo: NcbiTaxonomysRepo,
        batch_size: int = 10000,
    ):
        self._ncbi_taxonomy_repo = ncbi_taxonomy_repo
        self.batch_size = batch_size

    def generate_all_ncbi_taxonomy_hierarchy(self, **kwargs) -> DbOperation:
        logging.info("Retrieving existing NCBI taxonomy tax ids...")
        ncbi_tax_ids = tuple(self._ncbi_taxonomy_repo.get_all_ids(**kwargs))
        logging.info(f"Creating hierarchy by batch of {self.batch_size}...")
        for i in tqdm(range(0, len(ncbi_tax_ids) + 1, self.batch_size)):
            ncbi_tax_entries = self._ncbi_taxonomy_repo.get_entries(
                ncbi_tax_ids[i : i + self.batch_size], **kwargs
            )
            ncbi_tax_update_entries = [
                NcbiTaxonomyUpdate(
                    tax_id=ncbi_tax.tax_id, hierarchy=ncbi_tax.build_hierarchy()
                )
                for ncbi_tax in ncbi_tax_entries
            ]
            self._ncbi_taxonomy_repo.update_batch(
                ncbi_tax_update_entries, existing_entry_list=ncbi_tax_entries, **kwargs
            )
        return DbOperation(
            updated=len(ncbi_tax_ids), name="create-NCBI-taxonomy-hierarchy"
        )
