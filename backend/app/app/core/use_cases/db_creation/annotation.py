import logging
from typing import List

import inject

import pandas as pd

from app.core.repositories.annotations import (
    KeggOrthologyAnnotationsRepo,
    NcbiTaxonomyAnnotationsRepo,
)
from app.core.repositories.experiment import ExperimentsRepo
from app.core.repositories.ncbi_taxonomy import NcbiTaxonomysRepo
from app.core.repositories.gene import GenesRepo
from app.core.repositories.kegg import KeggOrthologysRepo

from app.core.schemas.tasks.db import DbOperation
from app.core.schemas.entities.annotations import (
    KeggOrthologyAnnotationCreate,
    NcbiTaxonomyAnnotationCreate,
)

logger = logging.getLogger()


class CreateGeneAnnotationsUseCase:
    BASE_OPERATION_NAME = "create_gene_annotations"
    ID_COL = "entry_id"

    @inject.autoparams("genes_repo")
    @inject.autoparams("experiments_repo")
    def __init__(
        self,
        annotations_df: pd.DataFrame,
        experiment_id: int,
        genes_repo: GenesRepo,
        experiments_repo: ExperimentsRepo,
        batch_size: int = 10000,
    ):
        """
        Use case to create gene annotations from dataframe.

        Index of dataframe has gene IDs.
        Columns represent different annotation types.
        """
        self.df = annotations_df
        self.experiment_id = experiment_id
        self._experiments_repo = experiments_repo
        self._genes_repo = genes_repo
        self.batch_size = batch_size
        self.db_operations: List[DbOperation] = []
        self.to_create = []

    def _create_unknown_annotations(self, **kwargs):
        message = (
            f"{self.df.columns[1]} is not a valid column name for annotations. Skipped."
        )
        logger.warning(message)
        self.db_operations.append(
            DbOperation(name="creation_skipped", extra={"message": message})
        )

    @inject.autoparams("ncbi_taxonomy_annotations_repo")
    @inject.autoparams("ncbi_taxonomys_repo")
    def _create_taxonomy_annotations(
        self,
        ncbi_taxonomy_annotations_repo: NcbiTaxonomyAnnotationsRepo,
        ncbi_taxonomys_repo: NcbiTaxonomysRepo,
        **kwargs,
    ):
        # Perform NCBI taxonomy annotations creation
        db_operation = DbOperation(name="create-ncbi_taxonomy-annotations")
        # Retrieve all gene db ids
        gene_db_dict = self._genes_repo.get_gene_db_ids(
            list(set(self.df.iloc[:, 0])), **kwargs
        )
        # Retrieve all kegg db ids
        ncbi_tax_items = ncbi_taxonomys_repo.get_entries(
            list(set(self.df["taxonomy"])), **kwargs
        )
        ncbi_tax_db_dict = {tax.tax_id: tax.id for tax in ncbi_tax_items}
        # Create annotations
        # - Create format for creation from df
        for tax_id, subdf in self.df.groupby("taxonomy"):
            annotation = NcbiTaxonomyAnnotationCreate(
                experiment_id=self.experiment_id,
                ncbi_taxonomy_id=ncbi_tax_db_dict[tax_id],
                genes=[gene_db_dict[gene_id] for gene_id in subdf.iloc[:, 0]],
            )
            ncbi_taxonomy_annotations_repo.create(annotation, **kwargs)
            db_operation.created += 1
        self.db_operations.append(db_operation)

    @inject.autoparams("kegg_orthology_annotations_repo")
    @inject.autoparams("kegg_orthologys_repo")
    def _create_kegg_annotations(
        self,
        kegg_orthology_annotations_repo: KeggOrthologyAnnotationsRepo,
        kegg_orthologys_repo: KeggOrthologysRepo,
        **kwargs,
    ):
        # Perform KEGG annotations creation
        db_operation = DbOperation(name="create-kegg-annotations")
        # Retrieve all gene db ids
        gene_db_dict = self._genes_repo.get_gene_db_ids(
            list(set(self.df.iloc[:, 0])), **kwargs
        )
        # Retrieve all kegg db ids
        keggs = kegg_orthologys_repo.get_entries(list(set(self.df["kegg"])), **kwargs)
        kegg_db_dict = {kegg.kegg_id: kegg.id for kegg in keggs}
        # Create annotations
        # - Create format for creation from df
        for kegg_id, subdf in self.df.groupby("kegg"):
            annotation = KeggOrthologyAnnotationCreate(
                experiment_id=self.experiment_id,
                kegg_id=kegg_db_dict[kegg_id],
                genes=[gene_db_dict[gene_id] for gene_id in subdf.iloc[:, 0]],
            )
            kegg_orthology_annotations_repo.create(annotation, **kwargs)
            db_operation.created += 1
        self.db_operations.append(db_operation)

    def create_annotations(self, **kwargs):
        col = self.df.columns[1]
        getattr(self, f"_create_{col}_annotations", self._create_unknown_annotations)(
            **kwargs
        )
        return self.db_operations
