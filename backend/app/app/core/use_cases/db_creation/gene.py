import logging
from typing import List

import inject
import pyfastx
from sqlalchemy.exc import NoResultFound

from app.core.models.gene import Gene  # noqa
from app.core.repositories.catalog import CatalogsRepo
from app.core.repositories.gene import GenesRepo
from app.core.schemas.entities.catalog import CatalogCreate
from app.core.schemas.entities.gene import (
    GeneCreate,
    GeneUpdate,
)
from app.core.schemas.tasks.db import DbOperation

from .base import BaseCreateOrUpdateEntriesUseCase

logger = logging.getLogger(__name__)


class CreateOrUpdateGenesUseCase(BaseCreateOrUpdateEntriesUseCase):
    BASE_OPERATION_NAME = "create_or_update_genes"
    MODEL = Gene
    ID_COL = "name"

    @inject.autoparams("repo")
    def __init__(
        self,
        repo: GenesRepo,
        fasta_path: str = None,
        batch_size: int = 10000,
        catalog_name: str = None,
        catalog_repo: CatalogsRepo = None,
    ):
        super().__init__(repo, batch_size=batch_size)
        self.fasta_path = fasta_path
        self.catalog_name = catalog_name
        self.catalog_repo = catalog_repo

    @property
    def fasta_path(self) -> str:
        """Path of nodes.dmp file."""
        if getattr(self, "_fasta_path", None) is None:
            raise UserWarning("Fasta path not set!")
        return self._fasta_path

    @fasta_path.setter
    def fasta_path(self, value: str):
        self._fasta_path = value

    def _create_all_entries(self) -> list:
        return pyfastx.Fasta(self.fasta_path)

    @property
    def all_entries(self) -> pyfastx.Fastx:
        """All Gene entries to create."""
        return self._create_all_entries()

    @property
    def catalog_id(self) -> int:
        """Catalog ID from DB."""
        if getattr(self, "_catalog_id", None) is None:
            raise UserWarning("catalog_id not set!")
        return self._catalog_id

    @catalog_id.setter
    def catalog_id(self, value: str):
        self._catalog_id = value

    def _generate_create_item(self, sequence: pyfastx.Sequence) -> GeneCreate:
        if self.catalog_name:
            return GeneCreate(
                gene_id=sequence.name, sequence=sequence.seq, catalogs=[self.catalog_id]
            )
        return GeneCreate(
            gene_id=sequence.name,
            sequence=sequence.seq,
        )

    def _generate_update_item(self, sequence: pyfastx.Sequence) -> GeneUpdate:
        if self.catalog_name:
            return GeneUpdate(
                gene_id=sequence.name, sequence=sequence.seq, catalogs=[self.catalog_id]
            )
        return GeneUpdate(
            gene_id=sequence.name,
            sequence=sequence.seq,
        )

    def _get_catalog_id(self, **kwargs) -> DbOperation:
        """Create catalog if it does not exists."""
        try:
            catalog = self.catalog_repo.get(self.catalog_name, **kwargs)
        except NoResultFound:
            catalog_input = CatalogCreate(name=self.catalog_name)
            catalog = self.catalog_repo.create(catalog_input, **kwargs)
        return catalog.id

    def create_or_update_all_entries(self, **kwargs) -> List[DbOperation]:
        if self.catalog_name is not None:
            self.catalog_id = self._get_catalog_id(**kwargs)
        return super().create_or_update_all_entries(**kwargs)
