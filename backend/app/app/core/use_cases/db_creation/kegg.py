import logging
from typing import List

import inject
from dabeplech import KEGGAPI
from dabeplech.models.kegg import LightKeggOrthologyModel

from app.core.models.kegg import KeggOrthology
from app.core.repositories.kegg import KeggOrthologysRepo
from app.core.schemas.entities.kegg import KeggOrthologyUpdate, KeggOrthologyCreate

from .base import BaseCreateOrUpdateEntriesUseCase

logger = logging.getLogger()


class CreateOrUpdateKeggOrthologyEntriesUseCase(BaseCreateOrUpdateEntriesUseCase):
    BASE_OPERATION_NAME = "create_or_update_KEGG_entries"
    MODEL = KeggOrthology
    ID_COL = "entry_id"

    @inject.autoparams("repo")
    def __init__(self, repo: KeggOrthologysRepo, batch_size: int = 10000):
        super().__init__(repo, batch_size=batch_size)

    def _create_all_entries(self) -> List[LightKeggOrthologyModel]:
        logging.info("Retrieving KEGG KO from KEGG API...")
        api = KEGGAPI()
        results = api.list("ko")
        return results.entries

    def _generate_create_item(
        self, item: LightKeggOrthologyModel
    ) -> KeggOrthologyCreate:
        return KeggOrthologyCreate(
            kegg_id=item.entry_id,
            name=item.names[0],
            definition=item.definition,
            ec_numbers=item.ec_numbers,
        )

    def _generate_update_item(
        self, item: LightKeggOrthologyModel
    ) -> KeggOrthologyUpdate:
        return KeggOrthologyUpdate(
            kegg_id=item.entry_id,
            name=item.names[0],
            definition=item.definition,
            ec_numbers=item.ec_numbers,
        )
