import abc
import logging
from typing import List

from tqdm import tqdm

from app.core.config import settings
from app.core.schemas.tasks.db import DbOperation

logging.basicConfig()
logger = logging.getLogger()


class BaseCreateOrUpdateEntriesUseCase(abc.ABC):
    BASE_OPERATION_NAME = "create_or_update_entries"
    MODEL = "Base"  # Define here model
    ID_COL = "id"

    def __init__(self, repo, batch_size: int = 10000):
        self._repo = repo
        self.batch_size = batch_size
        self.db_operations: List[DbOperation] = []
        self.to_create = []
        self.to_update = []

    @abc.abstractmethod
    def _generate_create_item(self, item):
        raise NotImplementedError

    @abc.abstractmethod
    def _generate_update_item(self, item):
        raise NotImplementedError

    @abc.abstractmethod
    def _create_all_entries(self) -> list:
        raise NotImplementedError

    @property
    def all_entries(self) -> dict:
        """All entries to create or update."""
        if getattr(self, "_all_entries", None) is None:
            self._all_entries = self._create_all_entries()
        return self._all_entries

    def _get_nb_lines(self, file_path: str):
        with open(file_path) as f:
            for i, l in enumerate(f):
                pass
        return i + 1

    def _run_create(self, db_operation: DbOperation, **kwargs):
        self._repo.create_batch(self.to_create, **kwargs)
        db_operation.created += len(self.to_create)
        self.to_create = []

    def _run_update(self, db_operation: DbOperation, **kwargs):
        self._repo.update_batch(self.to_update, **kwargs)
        db_operation.updated += len(self.to_update)
        self.to_update = []

    def _create_from_item(self, item, db_operation: DbOperation, **kwargs):
        self.to_create.append(self._generate_create_item(item))
        if len(self.to_create) == self.batch_size:
            self._run_create(db_operation, **kwargs)

    def _update_from_item(self, item, db_operation: DbOperation, **kwargs):
        self.to_update.append(self._generate_update_item(item))
        if len(self.to_update) == self.batch_size:
            self._run_update(db_operation, **kwargs)

    def _create_or_update_entries_left(self, db_operation: DbOperation, **kwargs):
        if self.to_create:
            self._run_create(db_operation, **kwargs)
        if self.to_update:
            self._run_update(db_operation, **kwargs)

    def create_or_update_all_entries(self, **kwargs) -> List[DbOperation]:
        """Create or update entries."""
        logging.info(f"Retrieving existing {self.MODEL.__name__} entries...")
        existing_entry_ids = self._repo.get_all_ids(**kwargs)
        logging.info(f"Creating or updating {self.MODEL.__name__} entries...")
        db_operation = DbOperation(name=self.BASE_OPERATION_NAME)
        for item in tqdm(self.all_entries, disable=settings.DISABLE_TQDM):
            if getattr(item, self.ID_COL) in existing_entry_ids:
                self._update_from_item(item, db_operation, **kwargs)
            else:
                self._create_from_item(item, db_operation, **kwargs)
        self._create_or_update_entries_left(db_operation, **kwargs)
        self.db_operations.append(db_operation)
        return self.db_operations
