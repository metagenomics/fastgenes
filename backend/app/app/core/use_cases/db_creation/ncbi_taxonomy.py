import logging

import inject
from tqdm import tqdm

from app.core.config import settings
from app.core.models.ncbi_taxonomy import NcbiTaxonomy
from app.core.repositories.ncbi_taxonomy import NcbiTaxonomysRepo
from app.core.schemas.entities.ncbi_taxonomy import (
    NcbiTaxonomyCreate,
    NcbiTaxonomyUpdate,
)
from app.core.schemas.tasks.db import DbOperation
from app.core.utils.parsers.ncbi_taxonomy import (
    NCBITaxonomyLineParser,
    NcbiTaxonomyNode,
)

from .base import BaseCreateOrUpdateEntriesUseCase

logger = logging.getLogger(__name__)


class CreateOrUpdateNcbiTaxonomyEntriesUseCase(BaseCreateOrUpdateEntriesUseCase):
    MODEL = NcbiTaxonomy
    ID_COL = "tax_id"

    @inject.autoparams("repo")
    def __init__(
        self,
        repo: NcbiTaxonomysRepo,
        nodes_path: str = None,
        names_path: str = None,
        preload_data: bool = True,
        batch_size: int = 10000,
    ):
        super().__init__(repo, batch_size=batch_size)
        self.nodes_path = nodes_path
        self.names_path = names_path
        if preload_data:
            self._preload_data()

    @property
    def nodes_path(self) -> str:
        """Path of nodes.dmp file."""
        if getattr(self, "_nodes_path", None) is None:
            raise UserWarning("nodes path not set!")
        return self._nodes_path

    @nodes_path.setter
    def nodes_path(self, value: str):
        self._nodes_path = value

    @property
    def names_path(self) -> str:
        """Path of names.dmp file."""
        if getattr(self, "_names_path", None) is None:
            raise UserWarning("names path not set!")
        return self._names_path

    @names_path.setter
    def names_path(self, value: str):
        self._names_path = value

    def _build_names_dict(self, select_class: str = "scientific name") -> dict:
        """
        Build and return a DICT {tax_id: taxe_name} for the chosen select_class
        """
        logger.info("Importing %s from %s...", select_class, self.names_path)
        taxo_name_dict = {}
        with open(self.names_path, "r") as file:
            for line in tqdm(file, total=self._get_nb_lines(self.names_path)):
                if select_class in line:
                    name = NCBITaxonomyLineParser.name(line)
                    taxo_name_dict[name.tax_id] = name.name_txt
        return taxo_name_dict

    @property
    def names_dict(self) -> dict:
        """Dict of tax_id -> names."""
        if getattr(self, "_names_dict", None) is None:
            self._names_dict = self._build_names_dict()
        return self._names_dict

    def _create_all_entries(self) -> list:
        logger.info("Creating NcbiTaxonomy from %s...", self.nodes_path)
        ncbi_tax_entries = []
        with open(self.nodes_path, "r") as file:
            for line in tqdm(file, total=self._get_nb_lines(self.nodes_path)):
                ncbi_tax_entries.append(NCBITaxonomyLineParser.node(line))
        return ncbi_tax_entries

    @property
    def all_entries(self) -> dict:
        """All NcbiTaxonomy entries to create."""
        if getattr(self, "_all_entries", None) is None:
            self._all_entries = self._create_all_entries()
        return self._all_entries

    def _preload_data(self):
        """Preload all data in properties."""
        self._names_dict = self._build_names_dict()
        self._all_entries = self._create_all_entries()

    def _generate_create_item(self, item: NcbiTaxonomyNode) -> NcbiTaxonomyCreate:
        return NcbiTaxonomyCreate(
            tax_id=item.tax_id,
            name=self.names_dict[item.tax_id],
            rank=item.rank,
        )

    def _generate_update_item(self, item: NcbiTaxonomyNode) -> NcbiTaxonomyUpdate:
        return NcbiTaxonomyUpdate(
            tax_id=item.tax_id,
            name=self.names_dict[item.tax_id],
            rank=item.rank,
        )

    def _update_link_from_item(
        self, tax_id, parent_id, link_operation: DbOperation, **kwargs
    ):
        self.to_update.append(NcbiTaxonomyUpdate(tax_id=tax_id, parent_id=parent_id))
        if len(self.to_update) == self.batch_size:
            self._repo.update_batch(self.to_update, **kwargs)
            link_operation.updated += len(self.to_update)
            self.to_update = []

    def create_or_update_all_entries(self, link_only=False, **kwargs) -> DbOperation:
        if not link_only:
            super().create_or_update_all_entries(**kwargs)
        # Link parent taxonomy
        link_operation = DbOperation(name="Link to parents")
        logging.info("Linking taxonomy to parents...")
        tax_id_to_db_ids = self._repo.get_tax_id_to_id_mapping(**kwargs)
        for node in tqdm(self.all_entries, disable=settings.DISABLE_TQDM):
            self._update_link_from_item(
                node.tax_id,
                tax_id_to_db_ids.get(node.parent_tax_id, None),
                link_operation,
                **kwargs
            )
        self._create_or_update_entries_left(link_operation, **kwargs)
        self.db_operations.append(link_operation)
        return self.db_operations
