from typing import List

import inject

from app.core.models.annotations import (
    EggnogAnnotation,
    KeggOrthologyAnnotation,
    NcbiTaxonomyAnnotation,
)
from app.core.schemas.entities.annotations import (
    EggnogAnnotationCreate,
    KeggOrthologyAnnotationCreate,
    NcbiTaxonomyAnnotationCreate,
)
from app.core.repositories.annotations import (
    EggnogAnnotationsRepo,
    KeggOrthologyAnnotationsRepo,
    NcbiTaxonomyAnnotationsRepo,
)


class CrudEggnogAnnotationUseCase:
    @inject.autoparams("eggnog_annotations_repo")
    def __init__(self, eggnog_annotations_repo: EggnogAnnotationsRepo):
        self._eggnog_annotations_repo = eggnog_annotations_repo

    def create_annotation(
        self, annotations_input: EggnogAnnotationCreate, **kwargs
    ) -> EggnogAnnotation:
        return self._eggnog_annotations_repo.create(annotations_input, **kwargs)

    def delete_annotation(self, annotation_id: str, **kwargs):
        return self._eggnog_annotations_repo.delete(annotation_id, **kwargs)

    def get_annotation(self, annotation_id: str, **kwargs) -> EggnogAnnotation:
        return self._eggnog_annotations_repo.get(annotation_id, **kwargs)

    def get_all(self, **kwargs) -> List[EggnogAnnotation]:
        return self._eggnog_annotations_repo.get_all(**kwargs)


class CrudKeggOrthologyAnnotationUseCase:
    @inject.autoparams("kegg_annotations_repo")
    def __init__(self, kegg_annotations_repo: KeggOrthologyAnnotationsRepo):
        self._kegg_annotations_repo = kegg_annotations_repo

    def create_annotation(
        self, annotations_input: KeggOrthologyAnnotationCreate, **kwargs
    ) -> KeggOrthologyAnnotation:
        return self._kegg_annotations_repo.create(annotations_input, **kwargs)

    def delete_annotation(self, annotation_id: str, **kwargs):
        return self._kegg_annotations_repo.delete(annotation_id, **kwargs)

    def get_annotation(self, annotation_id: str, **kwargs) -> KeggOrthologyAnnotation:
        return self._kegg_annotations_repo.get(annotation_id, **kwargs)

    def get_all(self, **kwargs) -> List[KeggOrthologyAnnotation]:
        return self._kegg_annotations_repo.get_all(**kwargs)


class CrudNcbiTaxonomyAnnotationUseCase:
    @inject.autoparams("ncbi_tax_annotations_repo")
    def __init__(self, ncbi_tax_annotations_repo: NcbiTaxonomyAnnotationsRepo):
        self._ncbi_tax_annotations_repo = ncbi_tax_annotations_repo

    def create_annotation(
        self, annotations_input: NcbiTaxonomyAnnotationCreate, **kwargs
    ) -> NcbiTaxonomyAnnotation:
        return self._ncbi_tax_annotations_repo.create(annotations_input, **kwargs)

    def delete_annotation(self, annotation_id: str, **kwargs):
        return self._ncbi_tax_annotations_repo.delete(annotation_id, **kwargs)

    def get_annotation(self, annotation_id: str, **kwargs) -> NcbiTaxonomyAnnotation:
        return self._ncbi_tax_annotations_repo.get(annotation_id, **kwargs)

    def get_all(self, **kwargs) -> List[NcbiTaxonomyAnnotation]:
        return self._ncbi_tax_annotations_repo.get_all(**kwargs)
