from typing import List

import inject

from app.core.models.catalog import Catalog
from app.core.schemas.entities.catalog import CatalogCreate, CatalogUpdate
from app.core.repositories.catalog import CatalogsRepo


class CrudCatalogUseCase:
    @inject.autoparams("catalogs_repo")
    def __init__(self, catalogs_repo: CatalogsRepo):
        self._catalogs_repo = catalogs_repo

    def create_catalog(self, catalog_input: CatalogCreate, **kwargs) -> Catalog:
        return self._catalogs_repo.create(catalog_input, **kwargs)

    def update_catalog(self, catalog_input: CatalogUpdate, **kwargs) -> Catalog:
        return self._catalogs_repo.update(catalog_input, **kwargs)

    def delete_catalog(self, catalog_name: str, **kwargs):
        return self._catalogs_repo.delete(catalog_name, **kwargs)

    def get_catalog(self, catalog_name: str, **kwargs) -> Catalog:
        return self._catalogs_repo.get(catalog_name, **kwargs)

    def get_all(self, **kwargs) -> List[Catalog]:
        return self._catalogs_repo.get_all(**kwargs)
