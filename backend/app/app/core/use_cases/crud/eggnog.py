from typing import List

import inject

from app.core.models.eggnog import Eggnog
from app.core.schemas.entities.eggnog import EggnogCreate, EggnogUpdate
from app.core.repositories.eggnog import EggnogsRepo


class CrudEggnogUseCase:
    @inject.autoparams("eggnogs_repo")
    def __init__(self, eggnogs_repo: EggnogsRepo):
        self._eggnogs_repo = eggnogs_repo

    def create_eggnog(self, eggnog_input: EggnogCreate, **kwargs) -> Eggnog:
        return self._eggnogs_repo.create(eggnog_input, **kwargs)

    def update_eggnog(self, eggnog_input: EggnogUpdate, **kwargs) -> Eggnog:
        return self._eggnogs_repo.update(eggnog_input, **kwargs)

    def delete_eggnog(self, eggnog_id: str, **kwargs):
        return self._eggnogs_repo.delete(eggnog_id, **kwargs)

    def get_eggnog(self, eggnog_id: str, **kwargs) -> Eggnog:
        return self._eggnogs_repo.get(eggnog_id, **kwargs)

    def get_all(self, **kwargs) -> List[Eggnog]:
        return self._eggnogs_repo.get_all(**kwargs)
