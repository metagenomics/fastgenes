from typing import List

import inject

from app.core.models.experiment import Experiment
from app.core.schemas.entities.experiment import ExperimentCreate, ExperimentUpdate
from app.core.repositories.experiment import ExperimentsRepo


class CrudExperimentUseCase:
    @inject.autoparams("experiments_repo")
    def __init__(self, experiments_repo: ExperimentsRepo):
        self._experiments_repo = experiments_repo

    def create_experiment(
        self, experiment_input: ExperimentCreate, **kwargs
    ) -> Experiment:
        return self._experiments_repo.create(experiment_input, **kwargs)

    def update_experiment(
        self, experiment_input: ExperimentUpdate, **kwargs
    ) -> Experiment:
        return self._experiments_repo.update(experiment_input, **kwargs)

    def delete_experiment(self, experiment_id: str, **kwargs):
        return self._experiments_repo.delete(experiment_id, **kwargs)

    def get_experiment(self, experiment_id: str, **kwargs) -> Experiment:
        return self._experiments_repo.get(experiment_id, **kwargs)

    def get_all(self, **kwargs) -> List[Experiment]:
        return self._experiments_repo.get_all(**kwargs)
