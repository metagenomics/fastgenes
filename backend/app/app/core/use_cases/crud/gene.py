from typing import List

import inject

from app.core.models.gene import Gene
from app.core.schemas.entities.gene import GeneCreate, GeneUpdate
from app.core.repositories.gene import GenesRepo


class CrudGeneUseCase:
    @inject.autoparams("genes_repo")
    def __init__(self, genes_repo: GenesRepo):
        self._genes_repo = genes_repo

    def create_gene(self, gene_input: GeneCreate, **kwargs) -> Gene:
        return self._genes_repo.create(gene_input, **kwargs)

    def update_gene(self, gene_input: GeneUpdate, **kwargs) -> Gene:
        return self._genes_repo.update(gene_input, **kwargs)

    def delete_gene(self, gene_id: str, **kwargs):
        return self._genes_repo.delete(gene_id, **kwargs)

    def delete_all(self, **kwargs):
        return self._genes_repo.delete_all(**kwargs)

    def get_gene(self, gene_id: str, **kwargs) -> Gene:
        return self._genes_repo.get(gene_id, **kwargs)

    def get_all(self, **kwargs) -> List[Gene]:
        return self._genes_repo.get_all(**kwargs)
