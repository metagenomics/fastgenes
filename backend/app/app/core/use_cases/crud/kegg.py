from typing import List

import inject

from app.core.models.kegg import KeggOrthology
from app.core.schemas.entities.kegg import (
    KeggOrthologyCreate,
    KeggOrthologyUpdate,
)

from app.core.repositories.kegg import KeggOrthologysRepo


class CrudKeggOrthologyUseCase:
    @inject.autoparams("keggs_repo")
    def __init__(self, keggs_repo: KeggOrthologysRepo):
        self._keggs_repo = keggs_repo

    def create_kegg(self, kegg_input: KeggOrthologyCreate, **kwargs) -> KeggOrthology:
        return self._keggs_repo.create(kegg_input, **kwargs)

    def update_kegg(self, kegg_input: KeggOrthologyUpdate, **kwargs) -> KeggOrthology:
        return self._keggs_repo.update(kegg_input, **kwargs)

    def delete_kegg(self, kegg_id: str, **kwargs):
        return self._keggs_repo.delete(kegg_id, **kwargs)

    def get_kegg(self, kegg_id: str, **kwargs) -> KeggOrthology:
        return self._keggs_repo.get(kegg_id, **kwargs)

    def get_all(self, **kwargs) -> List[KeggOrthology]:
        return self._keggs_repo.get_all(**kwargs)
