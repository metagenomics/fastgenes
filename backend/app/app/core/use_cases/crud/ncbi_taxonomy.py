from typing import List

import inject

from app.core.models.ncbi_taxonomy import NcbiTaxonomy
from app.core.schemas.entities.ncbi_taxonomy import (
    NcbiTaxonomyCreate,
    NcbiTaxonomyUpdate,
)
from app.core.repositories.ncbi_taxonomy import NcbiTaxonomysRepo


class CrudNcbiTaxonomyUseCase:
    @inject.autoparams("ncbi_taxonomys_repo")
    def __init__(self, ncbi_taxonomys_repo: NcbiTaxonomysRepo):
        self._ncbi_taxonomys_repo = ncbi_taxonomys_repo

    def create_ncbi_taxonomy(
        self, ncbi_taxonomy_input: NcbiTaxonomyCreate, **kwargs
    ) -> NcbiTaxonomy:
        return self._ncbi_taxonomys_repo.create(ncbi_taxonomy_input, **kwargs)

    def update_ncbi_taxonomy(
        self, ncbi_taxonomy_input: NcbiTaxonomyUpdate, **kwargs
    ) -> NcbiTaxonomy:
        return self._ncbi_taxonomys_repo.update(ncbi_taxonomy_input, **kwargs)

    def delete_ncbi_taxonomy(self, ncbi_taxonomy_name: str, **kwargs):
        return self._ncbi_taxonomys_repo.delete(ncbi_taxonomy_name, **kwargs)

    def get_ncbi_taxonomy(self, ncbi_taxonomy_id: str, **kwargs) -> NcbiTaxonomy:
        return self._ncbi_taxonomys_repo.get(ncbi_taxonomy_id, **kwargs)

    def get_all(self, **kwargs) -> List[NcbiTaxonomy]:
        return self._ncbi_taxonomys_repo.get_all(**kwargs)
