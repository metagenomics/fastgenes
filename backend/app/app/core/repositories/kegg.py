import abc
from typing import List, Set, Union, Dict

from sqlmodel import Session, select

from app.core.models.kegg import KeggOrthology
from app.core.schemas.entities.kegg import (
    KeggOrthologyCreate,
    KeggOrthologyUpdate,
)


class KeggOrthologysRepo(abc.ABC):
    @abc.abstractmethod
    def get(self, kegg_id: str) -> KeggOrthology:
        raise NotImplementedError

    @abc.abstractmethod
    def get_entries(self, kegg_id_list: List[str]) -> List[KeggOrthology]:
        raise NotImplementedError

    @abc.abstractmethod
    def get_all(self) -> List[KeggOrthology]:
        raise NotImplementedError

    @abc.abstractmethod
    def get_all_ids(self) -> Set[str]:
        raise NotImplementedError

    @abc.abstractmethod
    def create(self, kegg_input: KeggOrthologyCreate) -> KeggOrthology:
        raise NotImplementedError

    @abc.abstractmethod
    def create_batch(self, ncbi_taxonomy_input_list: List[KeggOrthologyCreate]) -> int:
        raise NotImplementedError

    @abc.abstractmethod
    def update(self, kegg_input: KeggOrthologyUpdate) -> KeggOrthology:
        raise NotImplementedError

    @abc.abstractmethod
    def update_batch(self, ncbi_taxonomy_input_list: List[KeggOrthologyUpdate]) -> int:
        raise NotImplementedError

    @abc.abstractmethod
    def delete(self, kegg_name: str):
        raise NotImplementedError


class SqlModelKeggOrthologysRepo(KeggOrthologysRepo):
    def get(self, kegg_id: str, session: Session) -> KeggOrthology:
        """Retrieve one KEGG from kegg_id."""
        statement = select(KeggOrthology).where(KeggOrthology.kegg_id == kegg_id)
        return session.exec(statement).one()

    def get_entries(
        self, kegg_id_list: List[str], session: Session
    ) -> List[KeggOrthology]:
        """Retrieve list of KEGG entries from kegg_id list."""
        statement = select(KeggOrthology).where(KeggOrthology.kegg_id.in_(kegg_id_list))
        return session.exec(statement).all()

    def get_all(
        self, session: Session, return_dict: bool = False
    ) -> Union[List[KeggOrthology], Dict[str, KeggOrthology]]:
        """Retrieve all KEGG entries."""
        keggs = session.exec(select(KeggOrthology)).all()
        if return_dict:
            return {kegg.kegg_id: kegg for kegg in keggs}
        return keggs

    def get_all_ids(self, session: Session) -> Set[str]:
        """Retrieve all KEGG ids."""
        kegg_ids = session.exec(select(KeggOrthology.kegg_id).distinct()).all()
        return set(kegg_ids)

    def create(
        self, kegg_input: KeggOrthologyCreate, session: Session, commit: bool = True
    ) -> KeggOrthology:
        """Create a KEGG entry."""
        kegg = KeggOrthology(
            kegg_id=kegg_input.kegg_id,
            name=kegg_input.name,
            definition=kegg_input.definition,
            ec_numbers=kegg_input.ec_numbers,
        )
        session.add(kegg)
        if commit:
            session.commit()
            session.refresh(kegg)
        return kegg

    def create_batch(
        self,
        kegg_input_list: List[KeggOrthologyCreate],
        session: Session,
    ) -> int:
        """Create KEGG entries."""
        kegg_items = [
            KeggOrthology(
                kegg_id=kegg_input.kegg_id,
                name=kegg_input.name,
                definition=kegg_input.definition,
                ec_numbers=kegg_input.ec_numbers,
            )
            for kegg_input in kegg_input_list
        ]
        session.bulk_insert_mappings(
            KeggOrthology, [item.dict() for item in kegg_items]
        )
        session.commit()
        return len(kegg_items)

    def update(
        self,
        kegg_input: KeggOrthologyUpdate,
        session: Session,
        exclude_none: bool = True,
        commit: bool = True,
        existing_kegg: KeggOrthology = None,
    ) -> KeggOrthology:
        """Update a Kegg entry."""
        kegg = (
            self.get(kegg_input.kegg_id, session)
            if existing_kegg is None
            else existing_kegg
        )
        for k, v in kegg_input.dict(exclude_none=exclude_none).items():
            setattr(kegg, k, v)
        session.add(kegg)
        if commit:
            session.commit()
            session.refresh(kegg)
        return kegg

    def update_batch(
        self,
        kegg_input_list: List[KeggOrthologyUpdate],
        session: Session,
        exclude_none: bool = True,
        existing_entry_list: List[KeggOrthology] = None,
    ) -> int:
        """Update KEGG entries."""
        kegg_ori_list = (
            self.get_entries([k.kegg_id for k in kegg_input_list], session=session)
            if existing_entry_list is None
            else existing_entry_list
        )
        for k_update, k_ori in zip(kegg_input_list, kegg_ori_list):
            for k, v in k_update.dict(exclude_none=exclude_none).items():
                setattr(k_ori, k, v)
        session.bulk_update_mappings(
            KeggOrthology, [item.dict() for item in kegg_ori_list]
        )
        session.commit()
        return len(kegg_ori_list)

    def delete(self, kegg_id: str, session: Session, commit: bool = True):
        """Delete KEGG entry from kegg_id."""
        kegg = self.get(kegg_id, session)
        session.delete(kegg)
        if commit:
            session.commit()
