import abc
from typing import List

from sqlmodel import Session, select

from app.core.models.experiment import Experiment
from app.core.schemas.entities.experiment import ExperimentCreate, ExperimentUpdate


class ExperimentsRepo(abc.ABC):
    @abc.abstractmethod
    def get(self, experiment_name: int) -> Experiment:
        raise NotImplementedError

    @abc.abstractmethod
    def get_all(self) -> List[Experiment]:
        raise NotImplementedError

    @abc.abstractmethod
    def create(self, experiment_input: ExperimentCreate) -> Experiment:
        raise NotImplementedError

    @abc.abstractmethod
    def update(self, experiment_input: ExperimentUpdate) -> Experiment:
        raise NotImplementedError

    @abc.abstractmethod
    def delete(self, experiment_name: int):
        raise NotImplementedError


class SqlModelExperimentsRepo(ExperimentsRepo):
    def get(self, experiment_id: int, session: Session) -> Experiment:
        """Retrieve one experiment from name."""
        statement = select(Experiment).where(Experiment.id == experiment_id)
        return session.exec(statement).one()

    def get_all(self, session: Session) -> List[Experiment]:
        """Retrieve all experiments."""
        experiments = session.exec(select(Experiment)).all()
        return experiments

    def create(
        self, experiment_input: ExperimentCreate, session: Session
    ) -> Experiment:
        """Create a experiment entry."""
        experiment = Experiment(
            name=experiment_input.name,
            date=experiment_input.date,
            description=experiment_input.description,
            doi=experiment_input.doi,
        )
        session.add(experiment)
        session.commit()
        session.refresh(experiment)
        return experiment

    def update(
        self,
        experiment_input: ExperimentUpdate,
        session: Session,
        exclude_none: bool = True,
    ) -> Experiment:
        """Update a experiment entry."""
        experiment = self.get(experiment_input.id, session)
        for k, v in experiment_input.dict(exclude_none=exclude_none).items():
            setattr(experiment, k, v)
        session.add(experiment)
        session.commit()
        session.refresh(experiment)
        return experiment

    def delete(self, experiment_id: int, session: Session):
        experiment = self.get(experiment_id, session)
        session.delete(experiment)
        session.commit()
