import abc
from typing import List

from sqlmodel import Session, select

from app.core.models.annotations import (
    EggnogAnnotation,
    KeggOrthologyAnnotation,
    NcbiTaxonomyAnnotation,
)
from app.core.models.gene import Gene
from app.core.schemas.entities.annotations import (
    EggnogAnnotationCreate,
    KeggOrthologyAnnotationCreate,
    NcbiTaxonomyAnnotationCreate,
)


class EggnogAnnotationsRepo(abc.ABC):
    @abc.abstractmethod
    def get(self, annotation_id: int) -> EggnogAnnotation:
        raise NotImplementedError

    @abc.abstractmethod
    def get_all(self) -> List[EggnogAnnotation]:
        raise NotImplementedError

    @abc.abstractmethod
    def create(self, annotation_input: EggnogAnnotationCreate) -> EggnogAnnotation:
        raise NotImplementedError

    @abc.abstractmethod
    def delete(self, annotation_id: int):
        raise NotImplementedError


class SqlModelEggnogAnnotationsRepo(EggnogAnnotationsRepo):
    def get(self, annotation_id: int, session: Session) -> EggnogAnnotation:
        """Retrieve one eggnog annotation from id."""
        statement = select(EggnogAnnotation).where(EggnogAnnotation.id == annotation_id)
        return session.exec(statement).one()

    def get_all(self, session: Session) -> List[EggnogAnnotation]:
        """Retrieve all eggnog annotations."""
        eggnog_annotations = session.exec(select(EggnogAnnotation)).all()
        return eggnog_annotations

    def create(
        self, annotation_input: EggnogAnnotationCreate, session: Session
    ) -> EggnogAnnotation:
        """Create a eggnog annotation."""
        genes = session.exec(
            select(Gene).where(Gene.id.in_(annotation_input.genes))
        ).all()
        eggnog_annotation = EggnogAnnotation(
            eggnog_id=annotation_input.eggnog_id,
            experiment_id=annotation_input.experiment_id,
            genes=genes,
        )
        session.add(eggnog_annotation)
        session.commit()
        session.refresh(eggnog_annotation)
        return eggnog_annotation

    def delete(self, annotation_id: int, session: Session):
        eggnog_annotation = self.get(annotation_id, session)
        session.delete(eggnog_annotation)
        session.commit()


class KeggOrthologyAnnotationsRepo(abc.ABC):
    @abc.abstractmethod
    def get(self, annotation_id: int) -> KeggOrthologyAnnotation:
        raise NotImplementedError

    @abc.abstractmethod
    def get_all(self) -> List[KeggOrthologyAnnotation]:
        raise NotImplementedError

    @abc.abstractmethod
    def create(
        self, annotation_input: KeggOrthologyAnnotationCreate
    ) -> KeggOrthologyAnnotation:
        raise NotImplementedError

    @abc.abstractmethod
    def delete(self, annotation_id: int):
        raise NotImplementedError


class SqlModelKeggAnnotationsRepo(KeggOrthologyAnnotationsRepo):
    def get(self, annotation_id: int, session: Session) -> KeggOrthologyAnnotation:
        """Retrieve one kegg annotation from id."""
        statement = select(KeggOrthologyAnnotation).where(
            KeggOrthologyAnnotation.id == annotation_id
        )
        return session.exec(statement).one()

    def get_all(self, session: Session) -> List[KeggOrthologyAnnotation]:
        """Retrieve all kegg annotations."""
        kegg_annotations = session.exec(select(KeggOrthologyAnnotation)).all()
        return kegg_annotations

    def create(
        self, annotation_input: KeggOrthologyAnnotationCreate, session: Session
    ) -> KeggOrthologyAnnotation:
        """Create a kegg orthology annotation."""
        genes = session.exec(
            select(Gene).where(Gene.id.in_(annotation_input.genes))
        ).all()
        kegg_annotation = KeggOrthologyAnnotation(
            kegg_id=annotation_input.kegg_id,
            experiment_id=annotation_input.experiment_id,
            genes=genes,
        )
        session.add(kegg_annotation)
        session.commit()
        session.refresh(kegg_annotation)
        return kegg_annotation

    def delete(self, annotation_id: int, session: Session):
        kegg_annotation = self.get(annotation_id, session)
        session.delete(kegg_annotation)
        session.commit()


class NcbiTaxonomyAnnotationsRepo(abc.ABC):
    @abc.abstractmethod
    def get(self, annotation_id: int) -> NcbiTaxonomyAnnotation:
        raise NotImplementedError

    @abc.abstractmethod
    def get_all(self) -> List[NcbiTaxonomyAnnotation]:
        raise NotImplementedError

    @abc.abstractmethod
    def create(
        self, annotation_input: NcbiTaxonomyAnnotationCreate
    ) -> NcbiTaxonomyAnnotation:
        raise NotImplementedError

    @abc.abstractmethod
    def delete(self, annotation_id: int):
        raise NotImplementedError


class SqlModelNcbiTaxonomyAnnotationsRepo(NcbiTaxonomyAnnotationsRepo):
    def get(self, annotation_id: int, session: Session) -> NcbiTaxonomyAnnotation:
        """Retrieve one NCBI taxonomy annotation from id."""
        statement = select(NcbiTaxonomyAnnotation).where(
            NcbiTaxonomyAnnotation.id == annotation_id
        )
        return session.exec(statement).one()

    def get_all(self, session: Session) -> List[NcbiTaxonomyAnnotation]:
        """Retrieve all NCBI taxonomy annotations."""
        ncbi_tax_annotations = session.exec(select(NcbiTaxonomyAnnotation)).all()
        return ncbi_tax_annotations

    def create(
        self, annotation_input: NcbiTaxonomyAnnotationCreate, session: Session
    ) -> NcbiTaxonomyAnnotation:
        """Create a NCBI taxonomy annotation."""
        genes = session.exec(
            select(Gene).where(Gene.id.in_(annotation_input.genes))
        ).all()
        ncbi_tax_annotation = NcbiTaxonomyAnnotation(
            ncbi_taxonomy_id=annotation_input.ncbi_taxonomy_id,
            experiment_id=annotation_input.experiment_id,
            genes=genes,
        )
        session.add(ncbi_tax_annotation)
        session.commit()
        session.refresh(ncbi_tax_annotation)
        return ncbi_tax_annotation

    def delete(self, annotation_id: int, session: Session):
        ncbi_tax_annotation = self.get(annotation_id, session)
        session.delete(ncbi_tax_annotation)
        session.commit()
