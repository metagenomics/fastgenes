import abc
from typing import List, Union

from sqlmodel import Session, select

from app.core.models.catalog import Catalog
from app.core.schemas.entities.catalog import CatalogCreate, CatalogUpdate


class CatalogsRepo(abc.ABC):
    @abc.abstractmethod
    def get(self, catalog_name: str) -> Catalog:
        raise NotImplementedError

    @abc.abstractmethod
    def get_all(self) -> List[Catalog]:
        raise NotImplementedError

    @abc.abstractmethod
    def create(self, catalog_input: CatalogCreate) -> Catalog:
        raise NotImplementedError

    @abc.abstractmethod
    def update(self, catalog_input: CatalogUpdate) -> Catalog:
        raise NotImplementedError

    @abc.abstractmethod
    def delete(self, catalog_name: str):
        raise NotImplementedError


class SqlModelCatalogsRepo(CatalogsRepo):
    def get(self, catalog_name: str, session: Session) -> Catalog:
        """Retrieve one catalog from name."""
        statement = select(Catalog).where(Catalog.name == catalog_name)
        return session.exec(statement).one()

    def get_all(
        self,
        session: Session,
        return_dict: bool = False,
        dict_key: str = "catalog_name",
    ) -> Union[List[Catalog], dict]:
        """Retrieve all catalogs."""
        catalogs = session.exec(select(Catalog)).all()
        if return_dict:
            return {getattr(catalog, dict_key): catalog for catalog in catalogs}
        return catalogs

    def create(self, catalog_input: CatalogCreate, session: Session) -> Catalog:
        """Create a catalog entry."""
        catalog = Catalog(name=catalog_input.name, doi=catalog_input.doi)
        session.add(catalog)
        session.commit()
        session.refresh(catalog)
        return catalog

    def update(
        self,
        catalog_input: CatalogUpdate,
        session: Session,
        exclude_none: bool = True,
    ) -> Catalog:
        """Update a catalog entry."""
        catalog = self.get(catalog_input.name, session)
        for k, v in catalog_input.dict(exclude_none=exclude_none).items():
            setattr(catalog, k, v)
        session.add(catalog)
        session.commit()
        session.refresh(catalog)
        return catalog

    def delete(self, catalog_name: str, session: Session):
        catalog = self.get(catalog_name, session)
        session.delete(catalog)
        session.commit()
