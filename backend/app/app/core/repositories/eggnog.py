import abc
from typing import List

from sqlmodel import Session, select

from app.core.models.eggnog import Eggnog
from app.core.schemas.entities.eggnog import EggnogCreate, EggnogUpdate


class EggnogsRepo(abc.ABC):
    @abc.abstractmethod
    def get(self, eggnog_name: str) -> Eggnog:
        raise NotImplementedError

    @abc.abstractmethod
    def get_all(self) -> List[Eggnog]:
        raise NotImplementedError

    @abc.abstractmethod
    def create(self, eggnog_input: EggnogCreate) -> Eggnog:
        raise NotImplementedError

    @abc.abstractmethod
    def update(self, eggnog_input: EggnogUpdate) -> Eggnog:
        raise NotImplementedError

    @abc.abstractmethod
    def delete(self, eggnog_name: str):
        raise NotImplementedError


class SqlModelEggnogsRepo(EggnogsRepo):
    def get(self, eggnog_id: str, session: Session) -> Eggnog:
        """Retrieve one eggnog from name."""
        statement = select(Eggnog).where(Eggnog.eggnog_id == eggnog_id)
        return session.exec(statement).one()

    def get_all(self, session: Session) -> List[Eggnog]:
        """Retrieve all eggnogs."""
        eggnogs = session.exec(select(Eggnog)).all()
        return eggnogs

    def create(self, eggnog_input: EggnogCreate, session: Session) -> Eggnog:
        """Create a eggnog entry."""
        eggnog = Eggnog(
            eggnog_id=eggnog_input.eggnog_id,
            name=eggnog_input.name,
            version=eggnog_input.version,
        )
        session.add(eggnog)
        session.commit()
        session.refresh(eggnog)
        return eggnog

    def update(
        self,
        eggnog_input: EggnogUpdate,
        session: Session,
        exclude_none: bool = True,
    ) -> Eggnog:
        """Update a eggnog entry."""
        eggnog = self.get(eggnog_input.eggnog_id, session)
        for k, v in eggnog_input.dict(exclude_none=exclude_none).items():
            setattr(eggnog, k, v)
        session.add(eggnog)
        session.commit()
        session.refresh(eggnog)
        return eggnog

    def delete(self, eggnog_id: str, session: Session):
        eggnog = self.get(eggnog_id, session)
        session.delete(eggnog)
        session.commit()
