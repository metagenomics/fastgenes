import abc
from typing import List, Set

from sqlmodel import Session, select

from app.core.models.ncbi_taxonomy import NcbiTaxonomy
from app.core.schemas.entities.ncbi_taxonomy import (
    NcbiTaxonomyCreate,
    NcbiTaxonomyUpdate,
)


class NcbiTaxonomysRepo(abc.ABC):
    @abc.abstractmethod
    def get(self, ncbi_taxonomy_name: int) -> NcbiTaxonomy:
        raise NotImplementedError

    @abc.abstractmethod
    def get_entries(self, tax_id_list: List[str]) -> List[NcbiTaxonomy]:
        raise NotImplementedError

    @abc.abstractmethod
    def get_all(self) -> List[NcbiTaxonomy]:
        raise NotImplementedError

    @abc.abstractmethod
    def get_all_ids(self) -> Set[str]:
        raise NotImplementedError

    @abc.abstractmethod
    def get_tax_id_to_id_mapping(self) -> dict:
        raise NotImplementedError

    @abc.abstractmethod
    def create(self, ncbi_taxonomy_input: NcbiTaxonomyCreate) -> NcbiTaxonomy:
        raise NotImplementedError

    @abc.abstractmethod
    def create_batch(self, ncbi_taxonomy_input_list: List[NcbiTaxonomyCreate]) -> int:
        raise NotImplementedError

    @abc.abstractmethod
    def update(self, ncbi_taxonomy_input: NcbiTaxonomyUpdate) -> NcbiTaxonomy:
        raise NotImplementedError

    @abc.abstractmethod
    def update_batch(
        self, ncbi_taxonomy_input_list: List[NcbiTaxonomyUpdate]
    ) -> NcbiTaxonomy:
        raise NotImplementedError

    @abc.abstractmethod
    def delete(self, ncbi_taxonomy_name: int):
        raise NotImplementedError


class SqlModelNcbiTaxonomysRepo(NcbiTaxonomysRepo):
    def get(self, tax_id: int, session: Session) -> NcbiTaxonomy:
        """Retrieve one NCBI taxonomy from tax_id."""
        statement = select(NcbiTaxonomy).where(NcbiTaxonomy.tax_id == tax_id)
        return session.exec(statement).one()

    def get_entries(
        self, tax_id_list: List[str], session: Session
    ) -> List[NcbiTaxonomy]:
        """Retrieve list of NCBI taxonomy entries from tax_id list."""
        statement = select(NcbiTaxonomy).where(NcbiTaxonomy.tax_id.in_(tax_id_list))
        return session.exec(statement).all()

    def get_all(
        self, session: Session, return_dict: bool = False
    ) -> List[NcbiTaxonomy]:
        """Retrieve all NCBI taxonomy entries."""
        ncbi_taxonomys = session.exec(select(NcbiTaxonomy)).all()
        if return_dict:
            return {tax.tax_id: tax for tax in ncbi_taxonomys}
        return ncbi_taxonomys

    def get_all_ids(self, session: Session) -> Set[str]:
        """Retrieve all NCBI taxonomy tax_ids."""
        tax_ids = session.exec(select(NcbiTaxonomy.tax_id).distinct()).all()
        return set(tax_ids)

    def get_tax_id_to_id_mapping(self, session: Session) -> dict:
        """Return mapping dict of tax_id to id of db."""
        tax_ids = session.exec(
            select(NcbiTaxonomy.tax_id, NcbiTaxonomy.id).distinct()
        ).all()
        return {t[0]: t[1] for t in tax_ids}

    def create(
        self,
        ncbi_taxonomy_input: NcbiTaxonomyCreate,
        session: Session,
        commit: bool = True,
    ) -> NcbiTaxonomy:
        """Create a NCBI taxonomy entry."""
        ncbi_taxonomy = NcbiTaxonomy(
            tax_id=ncbi_taxonomy_input.tax_id,
            rank=ncbi_taxonomy_input.rank,
            name=ncbi_taxonomy_input.name,
        )
        session.add(ncbi_taxonomy)
        if commit:
            session.commit()
            session.refresh(ncbi_taxonomy)
        return ncbi_taxonomy

    def create_batch(
        self,
        ncbi_taxonomy_input_list: List[NcbiTaxonomyCreate],
        session: Session,
    ) -> int:
        """Create NCBI taxonomy entries."""
        ncbi_taxonomy_items = [
            NcbiTaxonomy(
                tax_id=ncbi_taxonomy_input.tax_id,
                rank=ncbi_taxonomy_input.rank,
                name=ncbi_taxonomy_input.name,
            )
            for ncbi_taxonomy_input in ncbi_taxonomy_input_list
        ]
        session.bulk_insert_mappings(
            NcbiTaxonomy, [item.dict() for item in ncbi_taxonomy_items]
        )
        session.commit()
        return len(ncbi_taxonomy_items)

    def update(
        self,
        ncbi_taxonomy_input: NcbiTaxonomyUpdate,
        session: Session,
        exclude_none: bool = True,
        commit: bool = True,
        existing_entry: NcbiTaxonomy = None,
    ) -> NcbiTaxonomy:
        """Update a NCBI taxonomy entry."""
        ncbi_taxonomy = (
            self.get(ncbi_taxonomy_input.tax_id, session)
            if existing_entry is None
            else existing_entry
        )
        for k, v in ncbi_taxonomy_input.dict(exclude_none=exclude_none).items():
            setattr(ncbi_taxonomy, k, v)
        session.add(ncbi_taxonomy)
        if commit:
            session.commit()
            session.refresh(ncbi_taxonomy)
        return ncbi_taxonomy

    def update_batch(
        self,
        ncbi_taxonomy_input_list: List[NcbiTaxonomyUpdate],
        session: Session,
        exclude_none: bool = True,
        existing_entry_list: List[NcbiTaxonomy] = None,
    ) -> int:
        """Update NCBI taxonomy entries."""
        ncbi_tax_ori_list = (
            self.get_entries(
                [nt.tax_id for nt in ncbi_taxonomy_input_list], session=session
            )
            if existing_entry_list is None
            else existing_entry_list
        )
        for ncbi_tax_update, ncbi_tax_ori in zip(
            ncbi_taxonomy_input_list, ncbi_tax_ori_list
        ):
            for k, v in ncbi_tax_update.dict(exclude_none=exclude_none).items():
                setattr(ncbi_tax_ori, k, v)
        session.bulk_update_mappings(
            NcbiTaxonomy, [item.dict() for item in ncbi_tax_ori_list]
        )
        session.commit()
        return len(ncbi_tax_ori_list)

    def delete(self, tax_id: int, session: Session, commit: bool = True):
        """Delete a NCBI taxonomy entry from tax_id."""
        ncbi_taxonomy = self.get(tax_id, session)
        session.delete(ncbi_taxonomy)
        if commit:
            session.commit()
