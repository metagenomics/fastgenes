import abc
from typing import List, Set, Dict, Union

from sqlmodel import Session, select

from app.core.models.catalog import Catalog
from app.core.models.gene import Gene
from app.core.models.gene_links import GeneCatalogLink
from app.core.repositories.catalog import SqlModelCatalogsRepo
from app.core.schemas.entities.gene import GeneCreate, GeneUpdate
from app.core.schemas.tasks.db import DbOperation


class GenesRepo(abc.ABC):
    @abc.abstractmethod
    def get(self, gene_id: str) -> Gene:
        raise NotImplementedError

    @abc.abstractmethod
    def get_entries(self, gene_id_list: List[str]) -> List[Gene]:
        raise NotImplementedError

    @abc.abstractmethod
    def get_all(self) -> List[Gene]:
        raise NotImplementedError

    @abc.abstractmethod
    def get_all_ids(self) -> Set[str]:
        raise NotImplementedError

    @abc.abstractmethod
    def create(self, gene_input: GeneCreate) -> Gene:
        raise NotImplementedError

    @abc.abstractmethod
    def create_batch(self, gene_input_list: List[GeneCreate]) -> int:
        raise NotImplementedError

    @abc.abstractmethod
    def update(self, gene_input: GeneUpdate) -> Gene:
        raise NotImplementedError

    @abc.abstractmethod
    def update_batch(self, gene_input_list: List[GeneUpdate]) -> Gene:
        raise NotImplementedError

    @abc.abstractmethod
    def delete(self, gene_id: str):
        raise NotImplementedError

    @abc.abstractmethod
    def delete_all(self) -> DbOperation:
        raise NotImplementedError


class SqlModelGenesRepo(GenesRepo):
    @property
    def catalogs(self) -> Dict[str, Catalog]:
        """Mapping dict for existing catalogs."""
        if getattr(self, "_catalogs", None) is None:
            raise UserWarning("catalogs not set!")
        return self._catalogs

    @catalogs.setter
    def catalogs(self, value: str):
        self._catalogs = value

    def load_catalogs(self, session: Session):
        catalogs_repo = SqlModelCatalogsRepo()
        self.catalogs = catalogs_repo.get_all(
            session=session, return_dict=True, dict_key="id"
        )

    def _get_catalogs(self, catalog_ids: list, session: Session) -> List[Catalog]:
        if catalog_ids is None:
            return []
        if getattr(self, "_catalogs", None) is None:
            self.load_catalogs(session)
        catalogs = [catalog for k, catalog in self.catalogs.items() if k in catalog_ids]
        return catalogs

    def get(self, gene_id: str, session: Session) -> Gene:
        """Retrieve one gene from id."""
        statement = select(Gene).where(Gene.gene_id == gene_id)
        return session.exec(statement).one()

    def get_entries(self, gene_id_list: List[str], session: Session) -> List[Gene]:
        """Retrieve list of genes from gene id list."""
        statement = select(Gene).where(Gene.gene_id.in_(gene_id_list))
        return session.exec(statement).all()

    def get_all(self, session: Session, return_dict: bool = False) -> List[Gene]:
        """Retrieve all genes."""
        genes = session.exec(select(Gene)).all()
        if return_dict:
            return {gene.gene_id: gene for gene in genes}
        return genes

    def get_gene_db_ids(
        self, gene_id_list: List[str], session: Session
    ) -> Dict[str, int]:
        """Retrieve all Gene db ids from gene_ids."""
        genes = self.get_entries(gene_id_list, session)
        return {gene.gene_id: gene.id for gene in genes}

    def get_all_ids(self, session: Session) -> Set[str]:
        """Retrieve all Gene gene_ids."""
        gene_ids = session.exec(select(Gene.gene_id).distinct()).all()
        return set(gene_ids)

    def create(self, gene_input: GeneCreate, session: Session) -> Gene:
        """Create a gene entry."""
        gene = Gene(
            gene_id=gene_input.gene_id,
            sequence=gene_input.sequence,
            length=len(gene_input.sequence),
            catalogs=self._get_catalogs(gene_input.catalogs, session),
        )
        session.add(gene)
        session.commit()
        session.refresh(gene)
        return gene

    def _link_batch_to_catalog(
        self,
        gene_input_list: Union[List[GeneCreate], List[GeneUpdate]],
        session: Session,
    ):
        existing_genes = self.get_entries([g.gene_id for g in gene_input_list], session)
        gene_id_to_db_id = {gene.gene_id: gene.id for gene in existing_genes}
        gene_catalogs = {
            gene.gene_id: set([cat.id for cat in gene.catalogs])
            for gene in existing_genes
        }
        gene_catalog_link_list = []
        for gene in gene_input_list:
            if gene.catalogs is not None:
                gene_catalog_link_list += [
                    GeneCatalogLink(
                        gene_id=gene_id_to_db_id[gene.gene_id], catalog_id=cat_id
                    )
                    for cat_id in gene.catalogs
                    if cat_id not in gene_catalogs[gene.gene_id]
                ]
        if gene_catalog_link_list:
            session.bulk_insert_mappings(
                GeneCatalogLink, [item.dict() for item in gene_catalog_link_list]
            )
            session.commit()

    def create_batch(self, gene_input_list: List[GeneCreate], session: Session) -> int:
        gene_items = [
            Gene(
                gene_id=gene_input.gene_id,
                sequence=gene_input.sequence,
                length=len(gene_input.sequence),
            )
            for gene_input in gene_input_list
        ]
        session.bulk_insert_mappings(Gene, [item.dict() for item in gene_items])
        session.commit()
        self._link_batch_to_catalog(gene_input_list, session)
        return len(gene_items)

    def _perform_update(
        self,
        gene_input: GeneUpdate,
        gene_ori: Gene,
        exclude_none: bool,
    ):
        for k, v in gene_input.dict(exclude_none=exclude_none).items():
            if k == "catalogs":
                continue
            elif k == "sequence":
                if v is not None:
                    setattr(gene_ori, "length", len(v))
                setattr(gene_ori, k, v)
            else:
                setattr(gene_ori, k, v)
        return gene_ori

    def update(
        self,
        gene_input: GeneUpdate,
        session: Session,
        exclude_none: bool = True,
    ) -> Gene:
        """Update a gene entry."""
        gene_ori = self.get(gene_input.gene_id, session)
        gene = self._perform_update(gene_input, gene_ori, exclude_none)
        session.add(gene)
        session.commit()
        session.refresh(gene)
        return gene

    def update_batch(
        self,
        gene_input_list: List[GeneUpdate],
        session: Session,
        exclude_none: bool = True,
        existing_entry_list: List[Gene] = None,
    ) -> int:
        """Update NCBI taxonomy entries."""
        gene_ori_list = (
            self.get_entries([g.gene_id for g in gene_input_list], session=session)
            if existing_entry_list is None
            else existing_entry_list
        )
        updated_gene_list = []
        for gene_input, gene_ori in zip(gene_input_list, gene_ori_list):
            updated_gene_list.append(
                self._perform_update(gene_input, gene_ori, exclude_none)
            )
        session.bulk_update_mappings(Gene, [item.dict() for item in updated_gene_list])
        session.commit()
        self._link_batch_to_catalog(gene_input_list, session)
        return len(updated_gene_list)

    def delete(self, gene_id: str, session: Session):
        gene = self.get(gene_id, session)
        session.delete(gene)
        session.commit()

    def delete_all(self, session: Session) -> int:
        gene_count = session.query(Gene.id).count()
        session.query(GeneCatalogLink).delete()
        session.query(Gene).delete()
        session.commit()
        return DbOperation(name="delete-all-genes", deleted=gene_count)
