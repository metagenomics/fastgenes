from typing import List

from app.core.schemas.entities.gene import (
    GeneRead,
)
from app.core.schemas.entities.catalog import CatalogRead

from app.core.schemas.entities.annotations import (
    KeggOrthologyAnnotationRead,
    EggnogAnnotationRead,
    NcbiTaxonomyAnnotationRead,
)


class GeneReadWithAnnotations(GeneRead):
    catalogs: List[CatalogRead] = []
    eggnog_annotations: List[EggnogAnnotationRead] = []
    kegg_annotations: List[KeggOrthologyAnnotationRead] = []
    ncbi_taxonomy_annotations: List[NcbiTaxonomyAnnotationRead] = []
