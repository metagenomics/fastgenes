from typing import Optional

from app.core.models.eggnog import EggnogBase


class EggnogRead(EggnogBase):
    id: int
    pass


class EggnogCreate(EggnogBase):
    pass


class EggnogUpdate(EggnogBase):
    name: Optional[str]
    version: Optional[str]
    pass
