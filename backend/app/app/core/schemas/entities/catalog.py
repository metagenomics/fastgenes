from app.core.models.catalog import CatalogBase


class CatalogRead(CatalogBase):
    id: int

    class Config:
        schema_extra = {"example": {"name": "virgo", "doi": None, "id": 9}}


class CatalogCreate(CatalogBase):
    pass


class CatalogUpdate(CatalogBase):
    pass
