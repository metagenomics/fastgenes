from typing import Optional

from app.core.models.kegg import KeggOrthologyBase


class KeggOrthologyRead(KeggOrthologyBase):
    id: int

    class Config:
        schema_extra = {
            "example": {
                "kegg_id": "K00022",
                "name": "HADH",
                "definition": "3-hydroxyacyl-CoA dehydrogenase",
                "ec_numbers": ["1.1.1.35"],
                "id": 30,
            }
        }


class BaseKeggOrthologyPost(KeggOrthologyBase):
    class Config:
        schema_extra = {
            "example": {
                "kegg_id": "K00022",
                "name": "HADH",
                "definition": "3-hydroxyacyl-CoA dehydrogenase",
                "ec_numbers": ["1.1.1.35"],
            }
        }


class KeggOrthologyCreate(BaseKeggOrthologyPost):
    pass


class KeggOrthologyUpdate(BaseKeggOrthologyPost):
    name: Optional[str]
