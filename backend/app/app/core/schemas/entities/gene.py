from typing import Optional, List

from app.core.models.gene import GeneBase


class GeneRead(GeneBase):
    length: int
    id: int


class GeneCreate(GeneBase):
    catalogs: Optional[List[int]]


class GeneUpdate(GeneBase):
    sequence: Optional[str]
    catalogs: Optional[List[int]]
