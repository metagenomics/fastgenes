from typing import Optional

from app.core.models.ncbi_taxonomy import NcbiTaxonomyBase


class NcbiTaxonomyRead(NcbiTaxonomyBase):
    id: int
    parent_id: Optional[int]
    hierarchy: Optional[dict]

    class Config:
        schema_extra = {
            "example": {
                "tax_id": 562,
                "name": "Escherichia coli",
                "rank": "species",
                "id": 436,
                "parent_id": 435,
                "hierarchy": {
                    "species": {"tax_id": 562, "name": "Escherichia coli"},
                    "genus": {"tax_id": 561, "name": "Escherichia"},
                    "family": {"tax_id": 543, "name": "Enterobacteriaceae"},
                    "order": {"tax_id": 91347, "name": "Enterobacterales"},
                    "class": {"tax_id": 1236, "name": "Gammaproteobacteria"},
                    "phylum": {"tax_id": 1224, "name": "Proteobacteria"},
                    "superkingdom": {"tax_id": 2, "name": "Bacteria"},
                    "no_rank": {"tax_id": 131567, "name": "cellular organisms"},
                },
            }
        }


class BaseNcbiTaxonomyPost(NcbiTaxonomyBase):
    class Config:
        schema_extra = {
            "example": {
                "tax_id": 562,
                "name": "Escherichia coli",
                "rank": "species",
            }
        }


class NcbiTaxonomyCreate(BaseNcbiTaxonomyPost):
    pass


class NcbiTaxonomyUpdate(BaseNcbiTaxonomyPost):
    tax_id: int
    name: Optional[str]
    rank: Optional[str]
    parent_id: Optional[int]
    hierarchy: Optional[dict]
