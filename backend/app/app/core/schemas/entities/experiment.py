import datetime
from typing import Optional

from app.core.models.experiment import ExperimentBase


class ExperimentCreate(ExperimentBase):
    pass


class ExperimentRead(ExperimentBase):
    id: int
    pass


class ExperimentUpdate(ExperimentBase):
    id: int
    name: Optional[str]
    date: Optional[datetime.time]
    pass
