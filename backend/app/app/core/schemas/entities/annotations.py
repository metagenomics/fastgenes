from typing import List

from sqlmodel import SQLModel

from app.core.models.annotations import (
    KeggOrthologyAnnotationBase,
    EggnogAnnotationBase,
    NcbiTaxonomyAnnotationBase,
)
from app.core.schemas.entities.eggnog import EggnogRead
from app.core.schemas.entities.experiment import ExperimentRead
from app.core.schemas.entities.kegg import KeggOrthologyRead
from app.core.schemas.entities.ncbi_taxonomy import NcbiTaxonomyRead


class BaseAnnotationRead(SQLModel):
    id: int
    experiment: ExperimentRead


# ------------------ EggNOG ------------------


class EggnogAnnotationRead(BaseAnnotationRead):
    eggnog: EggnogRead


class EggnogAnnotationCreate(EggnogAnnotationBase):
    genes: List[int]
    pass


# ------------------ KEGG ------------------


class KeggOrthologyAnnotationRead(BaseAnnotationRead):
    kegg: KeggOrthologyRead


class KeggOrthologyAnnotationCreate(KeggOrthologyAnnotationBase):
    genes: List[int]


# ------------------ NCBI Taxonomy ------------------


class NcbiTaxonomyAnnotationRead(BaseAnnotationRead):
    ncbi_taxonomy: NcbiTaxonomyRead


class NcbiTaxonomyAnnotationCreate(NcbiTaxonomyAnnotationBase):
    genes: List[int]
