from enum import Enum

from pydantic import BaseModel


class TaskStatus(str, Enum):
    """https://docs.celeryproject.org/en/latest/userguide/tasks.html#built-in-states"""

    pending = "PENDING"
    started = "STARTED"
    success = "SUCCESS"
    failure = "FAILURE"
    retry = "RETRY"
    revoked = "REVOKED"


class BaseTask(BaseModel):
    task_id: str
    task_status: TaskStatus


class CreateTask(BaseTask):
    class Config:
        schema_extra = {
            "example": {
                "task_id": "bd4bdc9d-4169-4b77-b802-8c2f343591a3",
                "task_status": "PENDING",
            }
        }


class TaskResult(BaseTask):
    task_result: dict
