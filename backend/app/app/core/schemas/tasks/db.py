from typing import Optional, Union, List

from pydantic import BaseModel, Field

from .base import TaskResult


class DbOperation(BaseModel):
    created: int = Field(default=0)
    updated: int = Field(default=0)
    deleted: int = Field(default=0)
    skipped: int = Field(default=0)
    name: Optional[str]
    extra: Optional[dict]


class TaskDbOperation(TaskResult):
    task_result: List[Union[DbOperation, dict]]

    class Config:
        schema_extra = {
            "example": {
                "task_id": "d355a270-d505-4e55-86ae-8c9b34d7853b",
                "task_status": "SUCCESS",
                "task_result": [
                    {
                        "created": 0,
                        "updated": 24839,
                        "deleted": 0,
                        "skipped": 0,
                        "name": "create_or_update_KEGG_entries",
                        "extra": None,
                    },
                ],
            }
        }
