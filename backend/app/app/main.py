import logging

from fastapi import FastAPI

from app.version import __version__
from app.api.router import api_router
from app.core.config import settings
from app.dependency_injections import run_di


app = FastAPI(
    title="FastGenes",
    description="API for genes database.",
    version=__version__,
    docs_url="/docs",
    openapi_url=f"{settings.API_STR}/openapi.json",
)
logger = logging.getLogger(__name__)


app.include_router(
    api_router,
    prefix=settings.API_STR,
)


@app.on_event("startup")
def on_startup():
    run_di()
