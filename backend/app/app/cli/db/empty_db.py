"""CLI to empty DB."""
import logging

import typer

from app.core.config import settings
from app.db import reinit_db


logging.basicConfig()
logger = logging.getLogger()


def empty_all_tables():
    logging.info("Reinitializing database...")
    reinit_db()
    logging.info(f"DB[{settings.DATABASE_NAME}] is now empty!")


def empty_db(
    verbose: bool = typer.Option(False, help="Set logger to INFO level"),
    debug: bool = typer.Option(False, help="Set logger to DEBUG level"),
):
    """
    CLI interface to create small test DB.

    Args:
        verbose: Set logger to INFO level"
        debug: "Set logger to DEBUG level"
    """
    if debug:
        logger.setLevel("DEBUG")
    elif verbose:
        logger.setLevel("INFO")
    empty_all_tables()


def run():
    """Run CLI command for small test DB."""
    typer.run(empty_db)
