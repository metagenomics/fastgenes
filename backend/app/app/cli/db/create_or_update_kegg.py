"""CLI Create KEGG KO from API."""
import logging

import typer

from app.dependency_injections import run_di
from app.worker import (
    create_or_update_all_ko,
)


logging.basicConfig()
logger = logging.getLogger()


def create_db():
    run_di()
    actions_done = create_or_update_all_ko()
    for key, value in actions_done.items():
        logging.info(f"KEGG KO {key}: {value}")


def create_kegg_ko(
    verbose: bool = typer.Option(False, help="Set logger to INFO level"),
    debug: bool = typer.Option(False, help="Set logger to DEBUG level"),
):
    """
    CLI interface to create small test DB.

    Args:
        verbose: Set logger to INFO level"
        debug: "Set logger to DEBUG level"
    """
    if debug:
        logger.setLevel("DEBUG")
    elif verbose:
        logger.setLevel("INFO")
    create_db()


def run():
    """CLI Create KEGG KO from API."""
    typer.run(create_kegg_ko)
