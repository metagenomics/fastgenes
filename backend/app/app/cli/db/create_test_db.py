"""CLI to create small test DB."""
import logging

from sqlmodel import Session
from tqdm import tqdm
import typer

from app.db import engine
import app.core.models as models


logging.basicConfig()
logger = logging.getLogger()


def create_db():
    logging.info("Creating test db...")
    with Session(engine) as session:
        # Catalogs
        catalog_virgo = models.Catalog(name="Virgo")
        catalog_igc = models.Catalog(name="IGC")
        # Experiments
        experiment_blast = models.Experiment(name="exp A", date="2020-10-10")
        experiment_kraken = models.Experiment(name="exp B", date="2020-10-15")
        # Annotation entities
        kegg_a = models.KeggOrthology(kegg_id="K00001", name="First KO")
        kegg_b = models.KeggOrthology(kegg_id="K00002", name="Second KO")
        ncbi_tax_ecoli = models.NcbiTaxonomy(
            tax_id=562, name="Escherichia coli", rank="species"
        )
        ncbi_tax_lacto = models.NcbiTaxonomy(
            tax_id=1578, name="Lactobacillus", rank="genus"
        )
        # Genes
        gene_1 = models.Gene(
            gene_id="TG001",
            sequence="ctatcgatcga",
            length=11,
            catalogs=[catalog_virgo],
        )
        gene_2 = models.Gene(
            gene_id="TG002",
            sequence="ctaaaatcga",
            length=10,
            catalogs=[catalog_virgo],
        )
        gene_3 = models.Gene(
            gene_id="TG003",
            sequence="ctattcga",
            length=8,
            catalogs=[catalog_igc, catalog_virgo],
        )
        gene_4 = models.Gene(
            gene_id="TG004",
            sequence="ctatcgatctctcga",
            length=15,
            catalogs=[catalog_igc],
        )

        # Annotations
        annotations = []
        annotations.append(
            models.KeggOrthologyAnnotation(
                kegg=kegg_a, experiment=experiment_blast, genes=[gene_1, gene_4]
            )
        )
        annotations.append(
            models.KeggOrthologyAnnotation(
                kegg=kegg_b, experiment=experiment_blast, genes=[gene_2, gene_4]
            )
        )
        annotations.append(
            models.NcbiTaxonomyAnnotation(
                ncbi_taxonomy=ncbi_tax_ecoli,
                experiment=experiment_kraken,
                genes=[gene_1, gene_2],
            )
        )
        annotations.append(
            models.NcbiTaxonomyAnnotation(
                ncbi_taxonomy=ncbi_tax_lacto,
                experiment=experiment_kraken,
                genes=[gene_3],
            )
        )
        # Adding to session
        logging.info("Creating annotations...")
        for annotation in tqdm(annotations):
            session.add(annotation)
        session.commit()


def test_db(
    verbose: bool = typer.Option(False, help="Set logger to INFO level"),
    debug: bool = typer.Option(False, help="Set logger to DEBUG level"),
):
    """
    CLI interface to create small test DB.

    Args:
        verbose: Set logger to INFO level"
        debug: "Set logger to DEBUG level"
    """
    if debug:
        logger.setLevel("DEBUG")
    elif verbose:
        logger.setLevel("INFO")
    create_db()


def run():
    """Run CLI command for small test DB."""
    typer.run(test_db)
