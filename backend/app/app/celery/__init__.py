from celery import Celery

from app.core.config import settings

celery_app = Celery(
    __name__,
    broker=settings.CELERY_BROKER_URL,
    include=[
        "app.celery.tasks.kegg",
        "app.celery.tasks.ncbi_taxonomy",
        "app.celery.tasks.gene",
        "app.celery.tasks.annotation",
    ],
)
celery_app.conf.task_track_started = settings.CELERY_TASK_TRACK_STARTED
celery_app.conf.result_backend = settings.CELERY_RESULT_BACKEND
