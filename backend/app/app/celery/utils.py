from celery import current_app


def is_running_task(task_name: str) -> bool:
    inspect = current_app.control.inspect()
    for tasks in inspect.active().values():
        for task in tasks:
            if task["name"] == task_name:
                return True
    return False


def get_task_id(task_name: str) -> str:
    inspect = current_app.control.inspect()
    for tasks in inspect.active().values():
        for task in tasks:
            if task["name"] == task_name:
                return task["id"]
    return ""
