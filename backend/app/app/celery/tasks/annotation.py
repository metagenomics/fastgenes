import logging

import pandas as pd
from sqlmodel import Session

from app.celery import celery_app
from app.db import engine
from app.core.use_cases.db_creation.annotation import (
    CreateGeneAnnotationsUseCase,
)

logger = logging.getLogger(__name__)

CREATE_ANNOTATIONS_TASK_NAME = "create_gene_annotations"


@celery_app.task(name=CREATE_ANNOTATIONS_TASK_NAME)
def create_gene_annotations(annotation_url: str, experiment_id: int) -> dict:
    with Session(engine) as session:
        df = pd.read_csv(annotation_url, sep=",")
        # Create or update entries
        use_case = CreateGeneAnnotationsUseCase(df, experiment_id)
        results = use_case.create_annotations(session=session)
        return [result.dict() for result in results]
