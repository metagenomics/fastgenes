import logging
import os
import requests
import shutil
import tempfile

from sqlmodel import Session

from app.celery import celery_app
from app.db import engine
from app.core.use_cases.db_creation.gene import (
    CreateOrUpdateGenesUseCase,
)

from app.core.repositories.catalog import SqlModelCatalogsRepo
from app.core.repositories.gene import SqlModelGenesRepo

logger = logging.getLogger(__name__)

CREATE_GENES_TASK_NAME = "create_or_update_genes"


def _download_fasta(fasta_url: str, tmp_dir: str) -> None:
    # Download
    logger.info(f"Downloading from {fasta_url} to {tmp_dir}...")
    r = requests.get(fasta_url)
    # Writing tmp tar.gz
    tmp_fasta_path = os.path.join(tmp_dir, "genes.fasta")
    logger.info(f"Writing file to {tmp_fasta_path}...")
    with open(tmp_fasta_path, "wb") as file:
        file.write(r.content)


@celery_app.task(name=CREATE_GENES_TASK_NAME)
def create_or_update_genes(fasta_url: str, catalog_name: str) -> dict:
    with Session(engine) as session:
        # Download genes at fasta format
        tmp_dir = tempfile.mkdtemp()
        try:
            _download_fasta(fasta_url, tmp_dir)
            # Create or update entries
            use_case = CreateOrUpdateGenesUseCase(
                repo=SqlModelGenesRepo(),
                fasta_path=os.path.join(tmp_dir, "genes.fasta"),
                catalog_name=catalog_name,
                catalog_repo=SqlModelCatalogsRepo(),
            )
            results = use_case.create_or_update_all_entries(session=session)
        # Remove tmp directory
        finally:
            shutil.rmtree(tmp_dir)
        return [result.dict() for result in results]
