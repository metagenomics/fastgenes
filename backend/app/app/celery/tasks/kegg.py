from sqlmodel import Session

from app.celery import celery_app
from app.db import engine
from app.core.use_cases.db_creation.kegg import (
    CreateOrUpdateKeggOrthologyEntriesUseCase,
)
from app.core.repositories.kegg import SqlModelKeggOrthologysRepo

CREATE_KEGG_TASK_NAME = "create_or_update_all_ko"


@celery_app.task(name=CREATE_KEGG_TASK_NAME)
def create_or_update_all_ko() -> dict:
    with Session(engine) as session:
        use_case = CreateOrUpdateKeggOrthologyEntriesUseCase(
            repo=SqlModelKeggOrthologysRepo()
        )
        results = use_case.create_or_update_all_entries(session=session)
        return [result.dict() for result in results]
