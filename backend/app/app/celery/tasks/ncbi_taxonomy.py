import logging
import os
import requests
import shutil
import tarfile
import tempfile

from sqlmodel import Session

from app.celery import celery_app
from app.db import engine
from app.core.use_cases.db_creation.ncbi_taxonomy import (
    CreateOrUpdateNcbiTaxonomyEntriesUseCase,
)
from app.core.use_cases.ncbi_taxonomy.hierarchy import (
    GenerateNcbiTaxonomyHierarchyUseCase,
)
from app.core.repositories.ncbi_taxonomy import SqlModelNcbiTaxonomysRepo

logger = logging.getLogger(__name__)

CREATE_NCBI_TAX_TASK_NAME = "create_or_update_all_ncbi_tax"
GENERATE_HIERARCHY_TASK_NAME = "generate_ncbi_tax_hierarchy"


def _download_ncbi_tax_dmp(dump_url: str, tmp_dir: str) -> None:
    # Download
    logger.info(f"Downloading from {dump_url} to {tmp_dir}...")
    r = requests.get(dump_url)
    # Writing tmp tar.gz
    tmp_tar_path = os.path.join(tmp_dir, "taxdump.tar.gz")
    logger.info(f"Writing file to {tmp_tar_path}...")
    with open(tmp_tar_path, "wb") as file:
        file.write(r.content)
    # Extracting tmp tar.gz
    logger.info(f"Extracting {tmp_tar_path} at {tmp_dir}...")
    with tarfile.open(tmp_tar_path, "r:gz") as tar:
        tar.extractall(tmp_dir)


@celery_app.task(name=CREATE_NCBI_TAX_TASK_NAME)
def create_or_update_all_ncbi_tax(ncbi_taxdump_tar_url: str, link_only=False) -> dict:
    with Session(engine) as session:
        # Download taxonomy dumps in tmp directory
        tmp_dir = tempfile.mkdtemp()
        try:
            _download_ncbi_tax_dmp(ncbi_taxdump_tar_url, tmp_dir)
            # Create or update entries
            use_case = CreateOrUpdateNcbiTaxonomyEntriesUseCase(
                repo=SqlModelNcbiTaxonomysRepo(),
                names_path=os.path.join(tmp_dir, "names.dmp"),
                nodes_path=os.path.join(tmp_dir, "nodes.dmp"),
            )
            results = use_case.create_or_update_all_entries(
                link_only=link_only, session=session
            )
        # Remove tmp directory
        finally:
            shutil.rmtree(tmp_dir)
        return [result.dict() for result in results]


@celery_app.task(name=GENERATE_HIERARCHY_TASK_NAME)
def generate_ncbi_tax_hierarchy() -> dict:
    with Session(engine) as session:
        use_case = GenerateNcbiTaxonomyHierarchyUseCase(
            ncbi_taxonomy_repo=SqlModelNcbiTaxonomysRepo(),
        )
        results = use_case.generate_all_ncbi_taxonomy_hierarchy(session=session)
    return results.dict()
