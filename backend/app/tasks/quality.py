from invoke import task


@task
def black_format(c):
    """Run black format."""
    c.run("black app/")


@task
def black_check(c):
    """Run black check."""
    c.run("black app/ --check")


@task
def lint(c):
    """Run flake8."""
    c.run("flake8 app/")


@task(black_format, lint)
def all(c):
    """Run all quality tests."""
    pass
