#!/bin/bash

alembic upgrade head
uvicorn --reload --host 0.0.0.0 --port 8888 app.main:app
