# 🚀 FastGenes

From experiences acquired developing https://gitlab.pasteur.fr/metagenomics/metagenedb, create a light version of it with FastAPI as a backend and SQLModel as ORM.

* [FastAPI](https://fastapi.tiangolo.com/)
* [SQLModel](https://sqlmodel.tiangolo.com/)

## 💻 Run the project for development

To run the application you need:

* `Docker` : [Install instructions](https://docs.docker.com/install/)
* `Docker Compose` : [Install instructions](https://docs.docker.com/compose/install/)

From the root directory of the repository:

```bash
docker-compose build
docker-compose up
```

> 💬 *If you run the application for the first time, you will need to create the database depending on the name of your `DATABASE_NAME` env variable (`fastgenes` being the default value). [[More information below](#-create-your-db-on-postgresql)]*.

Then you can visit the [app](http://localhost) and the [documentation](http://localhost/docs) of the project.

### 📄 `.env`

> 💬 *This file allows user to deal with environment variable.*

Example of the different env variable is available:

* `backend/app/app/.env.sample`

If you remove the `.sample` extension to the file, the `.env` is read by the backend application.

The full list of Environment variables used by the application can be found at `app/app/core/config.py`.

-------

# 📝 Development & Contributions

## 💻 CI rules locally

First, make sure that you have the backend installed locally. From `backend/app` directory:

```bash
poetry install
```

### 💎 Quality

black and flake8 are applied on this project and you can run all using:

```bash
poetry run inv quality.all
```

### 🚦 Tests

You can run unit tests using:

```bash
poetry run inv tests.unit
poetry run inv tests.cov # with coverage
```

## 💾 DB Management

### 📖 Create your DB on postgresql

When you first run the application, you need to create the database based on the name your are giving (by default, fastgenes)

> 💬 *This is necessary since the name of the database is different from the default one.*

Find the ID of your running postgresql container:

```bash
(app-FUpT0C5z-py3.8) khillion@Kenzos-Mac-mini:~/Bioinfo/repos/pasteur-gitlab/fastgenes$ docker ps
CONTAINER ID   IMAGE                        COMMAND                  CREATED          STATUS          PORTS                    NAMES
428f880b2e9f   fastgenes_backend            "/app/scripts/start_…"   59 minutes ago   Up 19 minutes   0.0.0.0:8888->8888/tcp   fastgenes-backend
1504b29dc6d3   bitnami/postgresql:12.10.0   "/opt/bitnami/script…"   59 minutes ago   Up 40 minutes   0.0.0.0:5433->5432/tcp   postgresql
```

`1504b29dc6d3` is what we need, we can now connect to it:

```bash
docker exec -it 1504b29dc6d3 psql --user postgres
CREATE DATABASE fastgenes WITH OWNER postgres;
exit
```

> 💬 *You can create as many databases as you want and use the `.env` file in `backend/app/app` directory.*

### 📝 Populate test DB

You can create a test database using the API directly and some test files available on the repository.

> 💬 *You can use directly the [documentation page](http://localhost/docs) to run the different commands.*

Title | route | input path
----- | ----- | ----------
Create KEGG database | `/api/admin_db/create_kegg_ko` | no input file necessary, process is quick for all KEGG KO.
NCBI taxonomy | `/api/admin_db/create_ncbi_taxonomy` | `https://gitlab.pasteur.fr/metagenomics/fastgenes/-/raw/25-gene-annotations/data_test/ncbi_taxonomy/taxdump.tar.gz?inline=false`
NCBI taxonomy hierarchy | `/api/admin_db/generate_ncbi_taxonomy_hierarchy` | No file necessary, based on existing db
Genes | `/api/admin_db/create_genes` | `https://gitlab.pasteur.fr/metagenomics/fastgenes/-/raw/25-gene-annotations/data_test/genes/genes.fa?inline=false`

#### 💀 Deprecated way

From `backend/app` directory:

```bash
poetry run create_test_db
```

### 📂 Empty DB

```bash
poetry run empty_db
```
